## ROOT
setenv ROOTSYS /projects/cepc/root/test/root
setenv PATH ${ROOTSYS}/bin:${PATH}
setenv LD_LIBRARY_PATH ${ROOTSYS}/lib:${LD_LIBRARY_PATH}

## g++
setenv PATH /usr/local/bin:$PATH

## other libs
setenv LD_LIBRARY_PATH /usr/lib64/libstdc++.so.6
setenv LD_LIBRARY_PATH /projects/lhcb/sw/lcg/releases/gcc/4.9.1/x86_64-slc6/lib64/


## LHAPDF
setenv PATH /projects/cepc/LHAPDF_621/bin:$PATH
setenv LD_LIBRARY_PATH /projects/cepc/LHAPDF_621/lib:$LD_LIBRARY_PATH
setenv PYTHONPATH /projects/cepc/LHAPDF_621/lib64/python2.6/site-packages:$PYTHONPATH




********* PDF uncertainty analysis *********
*                                          *
*      Central value    808507.325 fb      *
*                                          *
*        Absolute PDF uncertainties        *
*      Symmetric +/-     36031.517 fb      *
*      +ve direction     36384.375 fb      *
*      -ve direction     39513.015 fb      *
*                                          *
*        Relative PDF uncertainties        *
*      Symmetric +/-          4.46 %       *
*      +ve direction          4.50 %       *
*      -ve direction          4.89 %       *
*                                          *
********************************************

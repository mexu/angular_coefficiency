\contentsline {section}{\numberline {1}Overview}{6}{section.1}
\contentsline {section}{\numberline {2}Installation}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}OpenMP and MPI}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Installation as a library}{8}{subsection.2.2}
\contentsline {section}{\numberline {3}Input parameters}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}Parton distributions}{10}{subsection.3.1}
\contentsline {section}{\numberline {4}Benchmark results at NNLO }{12}{section.4}
\contentsline {section}{\numberline {5}Runtime options}{13}{section.5}
\contentsline {section}{\numberline {6}Nuclear collisions}{28}{section.6}
\contentsline {section}{\numberline {7}Output}{29}{section.7}
\contentsline {subsection}{\numberline {7.1}Histograms}{30}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Other output modes}{32}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}Simple n-tuple output}{32}{subsubsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.2}n-tuples using FROOT}{33}{subsubsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.3}Unweighted events}{33}{subsubsection.7.2.3}
\contentsline {section}{\numberline {8}Other user options}{34}{section.8}
\contentsline {section}{\numberline {9}Notes on specific processes}{35}{section.9}
\contentsline {subsection}{\numberline {9.1}$W$-boson production, processes 1,6}{35}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}$W+$\nobreakspace {}jet production, processes 11,16}{36}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}$W+b$ production, processes 12,17}{36}{subsection.9.3}
\contentsline {subsection}{\numberline {9.4}$W+c$ production, processes 13,18}{36}{subsection.9.4}
\contentsline {subsection}{\numberline {9.5}$W+c$ production ($m_c=0$), processes 14,19}{36}{subsection.9.5}
\contentsline {subsection}{\numberline {9.6}$W+b{\mathaccent "7016\relax b}$ production, processes 20,25}{37}{subsection.9.6}
\contentsline {subsection}{\numberline {9.7}$W+b{\mathaccent "7016\relax b}$ production ($m_b=0$), processes 21,26}{37}{subsection.9.7}
\contentsline {subsection}{\numberline {9.8}$W+2$\nobreakspace {}jets production, processes 22,27}{37}{subsection.9.8}
\contentsline {subsection}{\numberline {9.9}$W+3$\nobreakspace {}jets production, processes 23,28}{38}{subsection.9.9}
\contentsline {subsection}{\numberline {9.10}$W+b{\mathaccent "7016\relax b}+$\nobreakspace {}jet production ($m_b=0$), processes 24,29}{38}{subsection.9.10}
\contentsline {subsection}{\numberline {9.11}$Z$-boson production, processes 31--33}{38}{subsection.9.11}
\contentsline {subsection}{\numberline {9.12}$Z$-boson production decaying to jets, processes 34--35}{38}{subsection.9.12}
\contentsline {subsection}{\numberline {9.13}$t \mathaccent "7016\relax {t}$ production mediated by $Z/\gamma ^*$-boson exchange, process 36}{39}{subsection.9.13}
\contentsline {subsection}{\numberline {9.14}$Z+$\nobreakspace {}jet production, processes 41--43}{39}{subsection.9.14}
\contentsline {subsection}{\numberline {9.15}$Z+2$\nobreakspace {}jets production, processes 44, 46}{39}{subsection.9.15}
\contentsline {subsection}{\numberline {9.16}$Z+3$\nobreakspace {}jets production, processes 45, 47}{39}{subsection.9.16}
\contentsline {subsection}{\numberline {9.17}$Z+b{\mathaccent "7016\relax b}$ production, process 50}{40}{subsection.9.17}
\contentsline {subsection}{\numberline {9.18}$Z+b{\mathaccent "7016\relax b}$ production ($m_b=0$), processes 51--53}{40}{subsection.9.18}
\contentsline {subsection}{\numberline {9.19}$Z+b{\mathaccent "7016\relax b}+$\nobreakspace {}jet production ($m_b=0$), process 54}{40}{subsection.9.19}
\contentsline {subsection}{\numberline {9.20}$Z+c{\mathaccent "7016\relax c}$ production ($m_c=0$), process 56}{40}{subsection.9.20}
\contentsline {subsection}{\numberline {9.21}Di-boson production, processes 61--89}{41}{subsection.9.21}
\contentsline {subsubsection}{\numberline {9.21.1}$WW$ production, processes 61-64, 69}{41}{subsubsection.9.21.1}
\contentsline {subsubsection}{\numberline {9.21.2}$WW$+jet production, process 66}{42}{subsubsection.9.21.2}
\contentsline {subsubsection}{\numberline {9.21.3}$WZ$ production, processes 71--80}{42}{subsubsection.9.21.3}
\contentsline {subsubsection}{\numberline {9.21.4}$ZZ$ production, processes 81--84, 86--90}{42}{subsubsection.9.21.4}
\contentsline {subsubsection}{\numberline {9.21.5}$ZZ$+jet production, process 85}{43}{subsubsection.9.21.5}
\contentsline {subsubsection}{\numberline {9.21.6}Anomalous couplings}{43}{subsubsection.9.21.6}
\contentsline {subsection}{\numberline {9.22}$WH$ production, processes 91-94, 96-99}{44}{subsection.9.22}
\contentsline {subsection}{\numberline {9.23}$ZH$ production, processes 101--109}{45}{subsection.9.23}
\contentsline {subsection}{\numberline {9.24}Higgs production, processes 111--121}{45}{subsection.9.24}
\contentsline {subsection}{\numberline {9.25}$H \to W^+W^-$ production, processes 123-126}{46}{subsection.9.25}
\contentsline {subsection}{\numberline {9.26}$H \to ZZ \to e^- e^+ \mu ^- \mu ^+$ production, processes 128-133}{47}{subsection.9.26}
\contentsline {subsection}{\numberline {9.27}$H+b$ production, processes 136--138}{48}{subsection.9.27}
\contentsline {subsection}{\numberline {9.28}$t\mathaccent "7016\relax {t}$ production with 2 semi-leptonic decays, processes 141--145}{49}{subsection.9.28}
\contentsline {subsection}{\numberline {9.29}$t\mathaccent "7016\relax {t}$ production with decay and a gluon, process 143}{50}{subsection.9.29}
\contentsline {subsection}{\numberline {9.30}$t\mathaccent "7016\relax {t}$ production with one hadronic decay, processes 146--151}{50}{subsection.9.30}
\contentsline {subsection}{\numberline {9.31}$Q\overline {Q}$ production, processes 157--159}{51}{subsection.9.31}
\contentsline {subsection}{\numberline {9.32}$t{\mathaccent "7016\relax t}+$\nobreakspace {}jet production, process 160}{51}{subsection.9.32}
\contentsline {subsection}{\numberline {9.33}Single top production, processes 161--177}{51}{subsection.9.33}
\contentsline {subsection}{\numberline {9.34}$Wt$ production, processes 180--187}{52}{subsection.9.34}
\contentsline {subsection}{\numberline {9.35}$H+$\nobreakspace {}jet production, processes 201--210}{53}{subsection.9.35}
\contentsline {subsection}{\numberline {9.36}Higgs production via WBF, processes 211--217}{54}{subsection.9.36}
\contentsline {subsection}{\numberline {9.37}$\tau ^+\tau ^-$ production, process 221}{54}{subsection.9.37}
\contentsline {subsection}{\numberline {9.38}$e^-e^+ \nu _{\mu } \mathaccent "7016\relax \nu _{\mu } $-pair production via WBF, processes 222}{54}{subsection.9.38}
\contentsline {subsection}{\numberline {9.39}$\nu _e e^+ \mu ^- \mu ^+$-pair production via WBF, processes 223,2231}{55}{subsection.9.39}
\contentsline {subsection}{\numberline {9.40}$e^- \mathaccent "7016\relax \nu _{e} \nu _{\mu } \mu ^+$-pair production via WBF, processes 224,2241}{55}{subsection.9.40}
\contentsline {subsection}{\numberline {9.41}$e^- \mathaccent "7016\relax \nu _{e} \mu ^- \mu ^+$-pair production via WBF, processes 225,2251}{55}{subsection.9.41}
\contentsline {subsection}{\numberline {9.42}$e^- e^+ \mathaccent "7016\relax \nu _{e} \nu _{e}$-pair production via WBF, processes 226}{55}{subsection.9.42}
\contentsline {subsection}{\numberline {9.43}$\nu _{e} e^+ \nu _{\mu } \mu ^+ $-pair production via WBF, processes 228,2281}{56}{subsection.9.43}
\contentsline {subsection}{\numberline {9.44}$e^- \mathaccent "7016\relax {\nu }_{e} \mu ^- \mathaccent "7016\relax {\nu }_{\mu } $-pair production via WBF, processes 229,2291}{56}{subsection.9.44}
\contentsline {subsection}{\numberline {9.45}$t$-channel single top with an explicit $b$-quark, processes 231--240}{56}{subsection.9.45}
\contentsline {subsection}{\numberline {9.46}$W^+W^++$jets production, processes 251,252}{57}{subsection.9.46}
\contentsline {subsection}{\numberline {9.47}$Z+Q$ production, processes 261--267}{57}{subsection.9.47}
\contentsline {subsection}{\numberline {9.48}$H + 2$\nobreakspace {}jet production, processes 270--274}{58}{subsection.9.48}
\contentsline {subsection}{\numberline {9.49}$H + 3$\nobreakspace {}jet production, processes 275-278}{58}{subsection.9.49}
\contentsline {subsection}{\numberline {9.50}Direct $\gamma $ production, processes 280--282}{58}{subsection.9.50}
\contentsline {subsection}{\numberline {9.51}Direct $\gamma $ + heavy flavour production, processes 283--284}{59}{subsection.9.51}
\contentsline {subsection}{\numberline {9.52}$\gamma \gamma $ production, processes 285-286}{59}{subsection.9.52}
\contentsline {subsection}{\numberline {9.53}$\gamma \gamma \gamma $ production, process 287}{60}{subsection.9.53}
\contentsline {subsection}{\numberline {9.54}$\gamma \gamma \gamma \gamma $ production, process 289}{60}{subsection.9.54}
\contentsline {subsection}{\numberline {9.55}$W\gamma $ production, processes 290-297}{61}{subsection.9.55}
\contentsline {subsubsection}{\numberline {9.55.1}Anomalous $WW\gamma $ couplings}{61}{subsubsection.9.55.1}
\contentsline {subsection}{\numberline {9.56}$Z\gamma $, production, processes 300, 305}{62}{subsection.9.56}
\contentsline {subsubsection}{\numberline {9.56.1}Anomalous $ZZ\gamma $ and $Z\gamma \gamma $ couplings}{62}{subsubsection.9.56.1}
\contentsline {subsection}{\numberline {9.57}$Z\gamma \gamma $ production processes, 301, 306}{63}{subsection.9.57}
\contentsline {subsection}{\numberline {9.58}$Z\gamma j$, production, processes 302, 307}{63}{subsection.9.58}
\contentsline {subsection}{\numberline {9.59}$Z\gamma \gamma j$ and $Z\gamma j j $, 303, 304, 308 and 309}{64}{subsection.9.59}
\contentsline {subsection}{\numberline {9.60}$W+Q+$\nobreakspace {}jet production processes 311--326}{64}{subsection.9.60}
\contentsline {subsection}{\numberline {9.61}$W+c+$\nobreakspace {}jet production, processes 331, 336}{65}{subsection.9.61}
\contentsline {subsection}{\numberline {9.62}$Z+Q+$jet production, processes 341--357}{65}{subsection.9.62}
\contentsline {subsection}{\numberline {9.63}$c \overline s \to W^+$, processes 361--363}{66}{subsection.9.63}
\contentsline {subsection}{\numberline {9.64}$W\gamma \gamma $ production, processes 370-371}{66}{subsection.9.64}
\contentsline {subsection}{\numberline {9.65}$W+Q$ production in the 4FNS, processes 401--408}{66}{subsection.9.65}
\contentsline {subsection}{\numberline {9.66}$W+Q$ production in the 5FNS, processes 411, 416}{67}{subsection.9.66}
\contentsline {subsection}{\numberline {9.67}$W+Q$ production in the combined 4FNS/5FNS, processes 421, 426}{67}{subsection.9.67}
\contentsline {subsection}{\numberline {9.68}$W+b{\mathaccent "7016\relax b}+$\nobreakspace {}jet production, processes 431,436}{67}{subsection.9.68}
\contentsline {subsection}{\numberline {9.69}$W+t{\mathaccent "7016\relax t}$ processes 500--516}{68}{subsection.9.69}
\contentsline {subsection}{\numberline {9.70}$Zt{\mathaccent "7016\relax t}$ production, processes 529-533}{68}{subsection.9.70}
\contentsline {subsection}{\numberline {9.71}$Ht$ and $H\mathaccent "7016\relax {t}$ production, processes 540--557}{69}{subsection.9.71}
\contentsline {subsection}{\numberline {9.72}$Zt$ and $Z\mathaccent "7016\relax {t}$ production, processes 560--569}{69}{subsection.9.72}
\contentsline {subsection}{\numberline {9.73}$HH$ production, processes 601--602}{70}{subsection.9.73}
\contentsline {subsection}{\numberline {9.74}$Ht{\mathaccent "7016\relax t}$ production, processes 640--660}{70}{subsection.9.74}
\contentsline {subsection}{\numberline {9.75}Dark Matter Processes Mono-jet and Mono-photon 800-848}{71}{subsection.9.75}
\contentsline {section}{\numberline {A}{\tt MCFM }references}{75}{appendix.A}
\contentsline {section}{\numberline {B}Processes included in MCFM}{79}{appendix.B}
\contentsline {section}{\numberline {C}Historical PDF sets}{90}{appendix.C}
\contentsline {section}{\numberline {D}Version 8.0 changelog}{92}{appendix.D}
\contentsline {section}{\numberline {E}Version 7.0 changelog}{92}{appendix.E}
\contentsline {section}{\numberline {F}Version 6.7 changelog}{92}{appendix.F}
\contentsline {section}{\numberline {G}Versions 6.6 and 6.5 changelog}{93}{appendix.G}
\contentsline {section}{\numberline {H}Version 6.4 changelog}{94}{appendix.H}
\contentsline {subsection}{\numberline {H.1}Code changes}{94}{subsection.H.1}
\contentsline {section}{\numberline {I}Version 6.3 changelog}{94}{appendix.I}
\contentsline {subsection}{\numberline {I.1}Code changes}{95}{subsection.I.1}
\contentsline {section}{\numberline {J}Version 6.2 changelog}{95}{appendix.J}
\contentsline {subsection}{\numberline {J.1}Input file changes}{96}{subsection.J.1}
\contentsline {subsection}{\numberline {J.2}Code changes}{96}{subsection.J.2}
\contentsline {section}{\numberline {K}Version 6.1 changelog}{97}{appendix.K}
\contentsline {subsection}{\numberline {K.1}Input file changes}{97}{subsection.K.1}
\contentsline {subsection}{\numberline {K.2}Code changes}{98}{subsection.K.2}

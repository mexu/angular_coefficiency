#ifndef _ALGOZ_
#define _ALGOZ_

#include <iostream>
#include <TLorentzVector.h>
#include <TROOT.h>
#include <TMath.h>
#include <vector>
#include <TH1.h>
#include "/projects/lhcb/users/mexu/Z2MuMu/Function/const_number.hpp"
using namespace const_num;

namespace algoz {
  double Efficiency_Error(double a, double b);
  double Error_Type3(double a,double b , double c,  double a_err, double b_err, double c_err);
  void Diff_2D(TH2D* hist_den, TH2D* hist_num, TH2D* hist);
  string ChargeName(int _type);
  string Eff_VarName(int _type);
  string VarName(int _type);
  double Collins_Soper(TLorentzVector mum, TLorentzVector mup);
  double Collins_Soper_Phi(TLorentzVector mum, TLorentzVector mup);
  double AFB_Err(double NF, double NB, double NF_err, double NB_err);
  void Make_AFB(TH1D* histF, TH1D* histB, TH1D* hist_afb);
  double Ratio_Error(double a,double b, double a_err, double b_err);
  void Make_Ratio(TH1D* hist_nume, TH1D* hist_deno, TH1D* hist);
  void Make_Sub(TH1D* hist_up, TH1D* hist_down, TH1D* hist);
//  void PrintFitResultHist(TH1D* hist,double ymin, double ymax, string _Type,int index);
//  void Compare_Result(double Pythia_A0[nbins_fit_Z_pt],double Res_FullNoFSR_A0[nbins_fit_Z_pt] );
}

inline double Efficiency_Error(double a, double b){
  return TMath::Sqrt((a+1.0)*fabs(b-a+1.0)/(b+2.0)/(b+2.0)/(b+3.0));
}

inline double algoz::Error_Type3(double a,double b , double c,  double a_err, double b_err, double c_err){ //(a*b)/c

  double part_1 = (a_err*a_err*b*b)/(c*c);
  double part_2 = (b_err*b_err*a*a)/(c*c);
  double part_3 = (a*a*b*b*c_err*c_err)/(c*c*c*c);
  return TMath::Sqrt(part_1 + part_2+part_3);
}

inline void algoz::Diff_2D(TH2D* hist_den, TH2D* hist_num, TH2D* hist){
  for(int i = 1; i < hist_den->GetNbinsX()+1; i++){
    for(int j = 1; j < hist_den->GetNbinsY()+1; j++){
      hist->SetBinContent(i, j, 0.);
      hist->SetBinError(i, j, 0.);

      double n_deno = hist_den->GetBinContent(i, j);
      double n_nume = hist_num->GetBinContent(i, j);
      double e_deno = hist_den->GetBinError(i, j);
      double e_nume = hist_num->GetBinError(i, j);

      double ratio = 0.;
      double error = 0.;

      double diff = n_nume-n_deno;
      error = sqrt(e_deno*e_deno + e_nume*e_nume);
      hist->SetBinContent(i, j,diff);
      hist->SetBinError(i, j, error);
    }
  }
}


inline string algoz::Eff_VarName(int _type){
  string name_tmp = "";
  if(_type == 0) name_tmp = "ETA";
  else if(_type == 1) name_tmp = "PT";
  else if(_type == 2) name_tmp = "PHI";
  else if(_type == 3) name_tmp = "SPD";
  else cout<<"Wrong input independence variable, please check !!!"<<endl;
  return name_tmp;
}

inline string algoz::ChargeName(int _type){
  string name_tmp = "";
  if(_type == 0) name_tmp = "Mup";
  else if(_type == 1) name_tmp = "Mum";
  else if(_type == 2) name_tmp = "Z";
  else cout<<"Wrong input efficiency type, please check !!!"<<endl;
  return name_tmp;
}

inline string algoz::VarName(int _type){
  string name_tmp = "";
  if(_type == 0) name_tmp = "Eta";
  else if(_type == 1) name_tmp = "PT";
  else if(_type == 2) name_tmp = "CST";
  else if(_type == 3) name_tmp = "Phi";
  else cout<<"Wrong input Var variable, please check !!!"<<endl;
  return name_tmp;
}


inline double algoz::Collins_Soper(TLorentzVector mum, TLorentzVector mup){
  double dimass12 = (mum + mup).M();
  double diPx = (mum.Px() + mup.Px());
  double diPy = (mum.Py() + mup.Py());
  double dimassQT = sqrt( diPx*diPx + diPy*diPy );
  double P1plus = ( mum.E() + mum.Pz() );
  double P1mins = ( mum.E() - mum.Pz() );
  double P2plus = ( mup.E() + mup.Pz() );
  double P2mins = ( mup.E() - mup.Pz() );
  double collins = ( P1plus*P2mins - P1mins*P2plus ) /
    ( dimass12 * sqrt(dimass12*dimass12 + dimassQT*dimassQT) );

  return collins;
}

inline double algoz::Collins_Soper_Phi(TLorentzVector mum, TLorentzVector mup){

  TLorentzVector boostedMu = mum ;
  TLorentzVector Z = (mum+mup);
  TVector3 CSAxis, xAxis, yAxis;
  boostedMu.Boost(-Z.BoostVector());
  double ProtonMass = 938.272; // MeV
  double BeamEnergy = 6500000; // MeV
  double sign  = fabs(Z.Z())/Z.Z();
  TLorentzVector p1, p2;
  p1.SetPxPyPzE(0, 0, sign*BeamEnergy,
                TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)); // quark
  p2.SetPxPyPzE(0, 0, -1*sign*BeamEnergy,
                TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)); // antiquark

  p1.Boost(-Z.BoostVector());
  p2.Boost(-Z.BoostVector());
  CSAxis = (p1.Vect().Unit()-p2.Vect().Unit()).Unit();
  yAxis = (p1.Vect().Unit()).Cross((p2.Vect().Unit()));
  yAxis = yAxis.Unit();
  xAxis = yAxis.Cross(CSAxis);

  xAxis = xAxis.Unit();
  double phi = atan2((boostedMu.Vect()*yAxis),(boostedMu.Vect()*xAxis));
  return phi; 

}


// error
inline double algoz::AFB_Err(double NF, double NB, double NF_err, double NB_err) {
  return 2*sqrt(NB*NB*NF_err*NF_err + NF*NF*NB_err*NB_err)/pow(NF+NB, 2);
}

// make AFB
inline void algoz::Make_AFB(TH1D* histF, TH1D* histB, TH1D* hist_afb){
  for(int i = 1; i < histF->GetNbinsX()+1; i++){
    double nf = histF->GetBinContent(i);
    double nb = histB->GetBinContent(i);
    double ef = histF->GetBinError(i);
    double eb = histB->GetBinError(i);

    double afb = 0.;
    double err = 0.;
    
    if((nf+nb) > 0){
      afb = (nf - nb)/(nf + nb);
      cout<<afb<<endl;
      err = 2*sqrt(nb*nb*ef*ef + nf*nf*eb*eb)/pow(nf+nb, 2);
    }
    hist_afb->SetBinContent(i, afb);
    hist_afb->SetBinError(i, err);
  }
}

inline double algoz::Ratio_Error(double a,double b, double a_err, double b_err){
  double part_1 = (a_err*a_err)/(b*b);
  double part_2 = (a*a*b_err*b_err)/(b*b*b*b);
  return TMath::Sqrt(part_1 + part_2);
}

inline void algoz::Make_Ratio(TH1D* hist_nume, TH1D* hist_deno, TH1D* hist){

  for(int i = 1; i < hist_deno->GetNbinsX()+1; i++){
      hist->SetBinContent(i, 0.);
      hist->SetBinError(i,0.);

      double n_deno = hist_deno->GetBinContent(i);
      double n_nume = hist_nume->GetBinContent(i);
      double e_deno = hist_deno->GetBinError(i);
      double e_nume = hist_nume->GetBinError(i);

      double ratio = 0.;
      double error = 0.;
      if(n_deno > 0){
        ratio = n_nume/n_deno;
        //cout<<ratio<<endl;
        error = Ratio_Error(n_nume, n_deno, e_nume, e_deno);
      }
     
      hist->SetBinContent(i,ratio);
     // hist->SetBinError(i, error);
  }
}


inline void algoz::Make_Sub(TH1D* hist_up, TH1D* hist_down, TH1D* hist){

  for(int i = 1; i < hist_up->GetNbinsX()+1; i++){

      hist->SetBinContent(i, 0.);
      hist->SetBinError(i, 0.);

      double n_up = hist_up->GetBinContent(i);
      double n_down = hist_down->GetBinContent(i);
      double e_up = hist_up->GetBinError(i);
      double e_down = hist_down->GetBinError(i);
      double diff = n_up-n_down;
      cout<<diff<<endl;
      double error = TMath::Sqrt(e_up*e_up+e_down*e_down);

      hist->SetBinContent(i, diff);
      hist->SetBinError(i, error);
  }
}
/*
inline void algoz::PrintFitResultHist(TH1D* hist,double ymin, double ymax, string _Type,int index){

  TCanvas *Can_Tmp = new TCanvas("Can_Tmp","",700,500);
  hist->SetXTitle("p_{T}(Z)");
  hist->SetYTitle(Form("A_{%0.0d}",index));
  hist->GetYaxis()->SetTitleOffset(1.1);
  hist->SetLineColor(4);
  hist->SetMarkerStyle(7);
  hist->SetMarkerSize(1);
  hist->SetMarkerColor(2);
  hist->SetAxisRange(ymin, ymax, "Y");
  hist->Draw();

  for(int i = 1; i < nbins_fit_Z_pt; i++){
    TLine* line = new TLine(bins_fit_Z_pt[i], ymin, bins_fit_Z_pt[i], ymax);
    line->SetLineColor(3);
    line->SetLineWidth(2);
    line->SetLineStyle(2);
    line->Draw("same");
  }

  Can_Tmp->Print(Form("./plots/Results_%s/Fit_A%0.0d.pdf",_Type.c_str(),index));
  delete Can_Tmp;

}
*/
/*
inline void algoz::Compare_Result(double Pythia_A[nbins_fit_Z_pt],double Res_FullNoFSR_A[nbins_fit_Z_pt],int value,double ymin,double ymax){

  TH1D *Hist_Pythia =new TH1D("Hist_Pythia","",nbins_fit_Z_pt,bins_fit_Z_pt);
  TH1D *Hist_Res_FullNoFSR =new TH1D("Hist_Full_NoFSR","",nbins_fit_Z_pt,bins_fit_Z_pt);
  for(int index=0;index<nbins_fit_Z_pt;index++){

    Hist_Pythia->SetBinContent(index+1,Pythia_A[index]);
    Hist_Res_FullNoFSR->SetBinContent(index+1,Res_FullNoFSR_A[index]);
}
  TCanvas* TCan = new TCanvas("TCan", "TCan", 700, 500);
  TLegend* Leg = new TLegend(0.2,0.6,0.4,0.8);
  //TLegend* Leg = new TLegend(pars[0], pars[1], pars[2], pars[3]);
  Leg->SetLineColor(0);
  Leg->SetFillColor(0);
  Hist_Pythia->SetMarkerColor(2);
  Hist_Pythia->SetLineColor(2);
  Hist_Res_FullNoFSR->SetMarkerColor(4);
  Hist_Res_FullNoFSR->SetLineColor(4);
  Leg->AddEntry(Hist_Pythia,"Pythia", "p");
  Leg->AddEntry(Hist_Res_FullNoFSR,"ResBos_NoFSR", "p");

  Hist_Pythia->SetTitle("");
  Hist_Pythia->SetXTitle("Z_{p_{T}}");
  Hist_Pythia->SetYTitle(Form("A_{%d}",value));
  Hist_Pythia->SetAxisRange(ymin, ymax, "Y");;
  Hist_Pythia->GetYaxis()->CenterTitle(false);
  Hist_Pythia->GetXaxis()->SetTitleOffset(1.1);
  Hist_Pythia->Draw("P");
  Hist_Res_FullNoFSR->Draw("same&&P");
  Leg->Draw("same");
  TCan->Print(Form("./plots/Compare/Result_A%d.pdf",value));
  delete TCan; delete Leg; 
  delete Hist_Pythia; delete Hist_Res_FullNoFSR;
}
 
void Make_Hist_Comparison(vector<TH1D*> hist, vector<string> name,
                                           string xtitle, string ytitle,
                                           string output, double pars[6], int nhist,string histname,bool norma){

  if(norma){
    for(int i = 0; i < nhist; i++){

      double n = hist[i]->Integral();
      hist[i] -> Scale(1.0/n);
  }
}
  string format[4] = {".C",".eps",".pdf",".png"};

  int _hist_colors[10]   = {2, 4, 33, 1, 45, 6, 7, 8, 9, 1};
  int _marker_styles[10] = {24, 25, 26, 27, 20, 30, 22, 22, 20, 21};
  int _line_styles[10] = {1, 1, 1, 1, 1, 2, 3, 3, 3, 4};
  int _fill_styles[10] = {3004,3009, 3007, 1, 1, 2, 3, 3, 3, 4};

  TCanvas* c1 = new TCanvas("c1", "c1", 700, 500);
  TLegend* leg = new TLegend(pars[0], pars[1], pars[2], pars[3]);
  leg->SetLineColor(0);
  leg->SetFillColor(0);

  for(int i = 0; i < nhist; i++){
    hist[i]->SetFillColor(_hist_colors[i]);
    hist[i]->SetLineColor(_hist_colors[i]);
    hist[i]->SetFillStyle(_fill_styles[i]);

    leg->AddEntry(hist[i], name[i].c_str(), "f");
  }

  hist[0]->SetTitle("");
  hist[0]->SetXTitle(xtitle.c_str());
  hist[0]->SetYTitle(ytitle.c_str());
  hist[0]->SetAxisRange(pars[4], pars[5], "Y");;
  hist[0]->GetYaxis()->CenterTitle(false);
  hist[0]->GetXaxis()->SetTitleOffset(1.1);
  hist[0]->Draw("hist same");
  for(int i = 1; i < nhist; i++){
    hist[i]->Draw("hist same");
  }

  TLatex *myTex = new TLatex(0.18,0.850,"LHCb Data");
  myTex->SetTextFont(132);
  myTex->SetTextSize(0.05251055);
  myTex->SetLineWidth(2);
  myTex->SetNDC();
  myTex->Draw("same");
  leg->Draw("same");
  for(int i=0;i<4;i++){
    c1->Print(Form("%s/%s%s",output.c_str(),histname.c_str(),format[i].c_str()));
   }

  delete c1;
  delete leg;
}

*/


/*
void Make_Hist_Comparison(vector<TH1D*> hist, vector<string> name,string xtitle,string ytitle, double pars[4],bool _norm){

  if(_norm){
    for(int i = 0; i < name.size(); i++){
      hist[i]->Scale(1.0/hist[i]->Integral());
    }
    TH1D* Hist_Chi2 = (TH1D*)(hist[0]->Clone());
    for(int ibinx=1 ; ibinx<hist[0]->GetNbinsX()+1; ibinx++) {
      Hist_Chi2->SetBinContent(ibinx,0);
    }
    Make_Ratio(hist[0],hist[1],Hist_Chi2)
  }
  TLegend* leg = new TLegend(pars[0], pars[1], pars[2], pars[3]);
  leg->SetLineColor(0);
  leg->SetFillColor(0);

  for(int i = 0; i < name.size(); i++){
    hist[i]->SetMarkerColor(_hist_colors[i]);
    hist[i]->SetLineColor(_hist_colors[i]);
    leg->AddEntry(hist[i], name[i].c_str(), "l");
  }
  hist[0]->GetXaxis()->SetTitleOffset(1.1);
  hist[0]->GetXaxis()->SetTitleOffset(0.85);
  hist[0]->GetYaxis()->SetRangeUser(87,90);
  hist[0]->Draw("p");

  for(int i = 1; i < name.size(); i++){
    hist[i]->Draw("p&&same");
  }
  leg->Draw("same");
}
*/
/*
void Draw_Dependence(vector<TLorentzVector> _mum_save, vector<TLorentzVector> _mup_save){

   TH1D* Hist_Dep_Mup_Eta_PT  = new TH1D("Hist_Dep_Mup_Eta_PT", "Hist_Dep_Mup_Eta_PT",nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mup_Eta_Phi = new TH1D("Hist_Dep_Mup_Eta_Phi","Hist_Dep_Mup_Eta_Phi", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mup_Eta_CST = new TH1D("Hist_Dep_Mup_Eta_CST","Hist_Dep_Mup_Eta_CST", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mup_Eta_Mass = new TH1D("Hist_Dep_Mup_Eta_Mass","Hist_Dep_Mup_Eta_Mass", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mup_PT_Mass = new TH1D("Hist_Dep_Mup_PT_Mass","Hist_Dep_Mup_PT_Mass", nbins_pt,bins_pt);
   TH1D* Hist_Dep_Mup_Phi_Mass = new TH1D("Hist_Dep_Mup_Phi_Mass","Hist_Dep_Mup_Phi_Mass", nbins_phi,bins_phi);

   TH1D* Hist_Dep_Mum_Eta_PT  = new TH1D("Hist_Dep_Mum_Eta_PT", "Hist_Dep_Mum_Eta_PT",nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mum_Eta_Phi = new TH1D("Hist_Dep_Mum_Eta_Phi","Hist_Dep_Mum_Eta_Phi", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mum_Eta_CST = new TH1D("Hist_Dep_Mum_Eta_CST","Hist_Dep_Mum_Eta_CST", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mum_Eta_Mass = new TH1D("Hist_Dep_Mum_Eta_Mass","Hist_Dep_Mum_Eta_Mass", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mum_PT_Mass = new TH1D("Hist_Dep_Mum_PT_Mass","Hist_Dep_Mum_PT_Mass", nbins_pt,bins_pt);
   TH1D* Hist_Dep_Mum_Phi_Mass = new TH1D("Hist_Dep_Mum_Phi_Mass","Hist_Dep_Mum_Phi_Mass", nbins_phi,bins_phi);
  
   TH1D* Hist_Dep_CST_Mass = new TH1D("Hist_Dep_CST_Mass","Hist_Dep_CST_Mass", nbins_cs,bins_cs);
   TH1D* Hist_Dep_Phi_Mass = new TH1D("Hist_Dep_Phi_Mass","Hist_Dep_Phi_Mass", nbins_cs,bins_cs);

   vector<TH1D*> Vec_Hist_Mup_Eta_PT;   vector<TH1D*> Vec_Hist_Mum_Eta_PT;
   vector<TH1D*> Vec_Hist_Mup_Eta_Phi;  vector<TH1D*> Vec_Hist_Mum_Eta_Phi;
   vector<TH1D*> Vec_Hist_Mup_Eta_CST;  vector<TH1D*> Vec_Hist_Mum_Eta_CST;
   vector<TH1D*> Vec_Hist_Mup_Eta_Mass; vector<TH1D*> Vec_Hist_Mum_Eta_Mass;
   vector<TH1D*> Vec_Hist_Mup_PT_Mass;  vector<TH1D*> Vec_Hist_Mum_PT_Mass;
   vector<TH1D*> Vec_Hist_Mup_Phi_Mass;  vector<TH1D*> Vec_Hist_Mum_Phi_Mass;
   
   vector<TH1D*> Vec_Hist_CST_Mass; vector<TH1D*> Vec_Hist_Phi_Mass;

   Vec_Hist_Mup_Eta_PT.clear();    Vec_Hist_Mum_Eta_PT.clear();
   Vec_Hist_Mup_Eta_Phi.clear();   Vec_Hist_Mum_Eta_Phi.clear();
   Vec_Hist_Mup_Eta_CST.clear();   Vec_Hist_Mum_Eta_CST.clear();
   Vec_Hist_Mup_Eta_Mass.clear();   Vec_Hist_Mum_Eta_Mass.clear();
   Vec_Hist_Mup_PT_Mass.clear();   Vec_Hist_Mum_PT_Mass.clear();
   Vec_Hist_Mup_Phi_Mass.clear();   Vec_Hist_Mum_Phi_Mass.clear();
 
   Vec_Hist_CST_Mass.clear();   Vec_Hist_CST_Mass.clear();
   Vec_Hist_Phi_Mass.clear();   Vec_Hist_Phi_Mass.clear();

   for(int i =0; i<nbins_eta;i++){

     TH1D* Hist_Tmp_Mup_Eta_PT  = new TH1D(Form("Hist_Tmp_Mup_Eta_PTBin%d",i),Form("Hist_Tmp_Mup_Eta_PTBin%d",i),50,0.0,120.0);
     TH1D* Hist_Tmp_Mup_Eta_Phi = new TH1D(Form("Hist_Tmp_Mup_Eta_PhiBin%d",i),Form("Hist_Tmp_Mup_Eta_PhiBin%d",i),50,-3.14,3.14);
     TH1D* Hist_Tmp_Mup_Eta_CST = new TH1D(Form("Hist_Tmp_Mup_Eta_CSTBin%d",i),Form("Hist_Tmp_Mup_Eta_CSTBin%d",i),50,-1.0,1.0);
     TH1D* Hist_Tmp_Mup_Eta_Mass = new TH1D(Form("Hist_Tmp_Mup_Eta_MassBin%d",i),Form("Hist_Tmp_Mup_Eta_MassBin%d",i),50,50,150);
     TH1D* Hist_Tmp_Mup_PT_Mass = new TH1D(Form("Hist_Tmp_Mup_PT_MassBin%d",i),Form("Hist_Tmp_Mup_PT_MassBin%d",i),50,50,150);
     TH1D* Hist_Tmp_Mup_Phi_Mass = new TH1D(Form("Hist_Tmp_Mup_Phi_MassBin%d",i),Form("Hist_Tmp_Mup_Phi_MassBin%d",i),50,50,150);
   
     TH1D* Hist_Tmp_Mum_Eta_PT  = new TH1D(Form("Hist_Tmp_Mum_Eta_PTBin%d",i),Form("Hist_Tmp_Mum_Eta_PTBin%d",i),50,0.0,120.0);
     TH1D* Hist_Tmp_Mum_Eta_Phi = new TH1D(Form("Hist_Tmp_Mum_Eta_PhiBin%d",i),Form("Hist_Tmp_Mum_Eta_PhiBin%d",i),50,-3.14,3.14);
     TH1D* Hist_Tmp_Mum_Eta_CST = new TH1D(Form("Hist_Tmp_Mum_Eta_CSTBin%d",i),Form("Hist_Tmp_Mum_Eta_CSTBin%d",i),50,-1.0,1.0);
     TH1D* Hist_Tmp_Mum_Eta_Mass = new TH1D(Form("Hist_Tmp_Mum_Eta_MassBin%d",i),Form("Hist_Tmp_Mum_Eta_MassBin%d",i),50,50,150);
     TH1D* Hist_Tmp_Mum_PT_Mass = new TH1D(Form("Hist_Tmp_Mum_PT_MassBin%d",i),Form("Hist_Tmp_Mum_PT_MassBin%d",i),50,50,150);
     TH1D* Hist_Tmp_Mum_Phi_Mass = new TH1D(Form("Hist_Tmp_Mum_Phi_MassBin%d",i),Form("Hist_Tmp_Mum_Phi_MassBin%d",i),50,50,150);
 
     TH1D* Hist_Tmp_CST_Mass = new TH1D(Form("Hist_Tmp_CST_MassBin%d",i),Form("Hist_Tmp_CST_MassBin%d",i),50,50,150);
     TH1D* Hist_Tmp_Phi_Mass = new TH1D(Form("Hist_Tmp_Phi_MassBin%d",i),Form("Hist_Tmp_Phi_MassBin%d",i),50,50,150);

     //Vec_Hist_Mup_Eta_PT.push_back(Hist_Tmp_Mup_Eta_PT);
     //Vec_Hist_Mup_Eta_Phi.push_back(Hist_Tmp_Mup_Eta_Phi);
     //Vec_Hist_Mup_Eta_CST.push_back(Hist_Tmp_Mup_Eta_CST);
     //Vec_Hist_Mup_Eta_Mass.push_back(Hist_Tmp_Mup_Eta_Mass);
     //Vec_Hist_Mup_PT_Mass.push_back(Hist_Tmp_Mup_PT_Mass);
     //Vec_Hist_Mup_Phi_Mass.push_back(Hist_Tmp_Mup_Phi_Mass);

     //Vec_Hist_Mum_Eta_PT.push_back(Hist_Tmp_Mum_Eta_PT);
     //Vec_Hist_Mum_Eta_Phi.push_back(Hist_Tmp_Mum_Eta_Phi);
     //Vec_Hist_Mum_Eta_CST.push_back(Hist_Tmp_Mum_Eta_CST);
     //Vec_Hist_Mum_Eta_Mass.push_back(Hist_Tmp_Mum_Eta_Mass);
     //Vec_Hist_Mum_PT_Mass.push_back(Hist_Tmp_Mum_PT_Mass);
     //Vec_Hist_Mum_Phi_Mass.push_back(Hist_Tmp_Mum_Phi_Mass);

     //Vec_Hist_CST_Mass.push_back(Hist_Tmp_CST_Mass);
     //Vec_Hist_Phi_Mass.push_back(Hist_Tmp_Phi_Mass);
  }

  for(int i=0; i<_mup_save.size();i++){
    double mup_Pt     = _mup_save[i].Pt();
    double mum_Pt     = _mum_save[i].Pt();
    double mup_Eta    = _mup_save[i].Eta();
    double mum_Eta    = _mum_save[i].Eta(); 
    double Zmas      = (_mup_save[i]+_mum_save[i]).M();
    double collins    = Collins_Soper(_mum_save[i],_mup_save[i]);
    double phi        = Collins_Soper_Phi(_mum_save[i], _mup_save[i]);
    for(int j=0;j<nbins_pt;j++){
      if(mup_Eta>bins_eta[j]&&mup_Eta<bins_eta[j+1]){
        Vec_Hist_Mup_Eta_PT[j]->Fill(mup_Pt);
        Vec_Hist_Mup_Eta_Phi[j]->Fill(_mup_save[i].Phi()); 
        Vec_Hist_Mup_Eta_CST[j]->Fill(collins);
        Vec_Hist_Mup_Eta_Mass[j]->Fill(Zmas);
       }

      if(mum_Eta>bins_eta[j]&&mum_Eta<bins_eta[j+1]){
        Vec_Hist_Mum_Eta_PT[j]->Fill(mum_Pt);
        Vec_Hist_Mum_Eta_Phi[j]->Fill(_mum_save[i].Phi());
        Vec_Hist_Mum_Eta_CST[j]->Fill(collins);
        Vec_Hist_Mum_Eta_Mass[j]->Fill(Zmas);
      }

    if(mup_Pt>bins_pt[j]&&mup_Pt<bins_pt[j+1]) Vec_Hist_Mup_PT_Mass[j]->Fill(Zmas);
    if(mum_Pt>bins_pt[j]&&mum_Pt<bins_pt[j+1]) Vec_Hist_Mum_PT_Mass[j]->Fill(Zmas);

    if(_mup_save[i].Phi()>bins_phi[j]&&_mup_save[i].Phi()<bins_phi[j+1]) Vec_Hist_Mup_Phi_Mass[j]->Fill(Zmas);
    if(_mum_save[i].Phi()>bins_phi[j]&&_mum_save[i].Phi()<bins_phi[j+1]) Vec_Hist_Mum_Phi_Mass[j]->Fill(Zmas);

    if(collins>bins_cs[j]&&collins<bins_cs[j+1]) Vec_Hist_CST_Mass[j]->Fill(Zmas);
    if(phi>bins_cs[j]&&phi<bins_cs[j+1])         Vec_Hist_Phi_Mass[j]->Fill(Zmas);
     }
  }

   for(int j=0;j<nbins_pt;j++){
     Hist_Dep_Mup_Eta_PT->SetBinContent(j+1, Vec_Hist_Mup_Eta_PT[j]->GetMean());
     Hist_Dep_Mup_Eta_Phi->SetBinContent(j+1, Vec_Hist_Mup_Eta_Phi[j]->GetMean());
     Hist_Dep_Mup_Eta_CST->SetBinContent(j+1, Vec_Hist_Mup_Eta_CST[j]->GetMean());
     Hist_Dep_Mup_Eta_Mass->SetBinContent(j+1, Vec_Hist_Mup_Eta_Mass[j]->GetMean());
     Hist_Dep_Mup_PT_Mass->SetBinContent(j+1, Vec_Hist_Mup_PT_Mass[j]->GetMean());
     Hist_Dep_Mup_Phi_Mass->SetBinContent(j+1, Vec_Hist_Mup_Phi_Mass[j]->GetMean());
  
     Hist_Dep_Mum_Eta_PT->SetBinContent(j+1, Vec_Hist_Mum_Eta_PT[j]->GetMean());
     Hist_Dep_Mum_Eta_Phi->SetBinContent(j+1, Vec_Hist_Mum_Eta_Phi[j]->GetMean());
     Hist_Dep_Mum_Eta_CST->SetBinContent(j+1, Vec_Hist_Mum_Eta_CST[j]->GetMean());
     Hist_Dep_Mum_Eta_Mass->SetBinContent(j+1, Vec_Hist_Mum_Eta_Mass[j]->GetMean());
     Hist_Dep_Mum_PT_Mass->SetBinContent(j+1, Vec_Hist_Mum_PT_Mass[j]->GetMean());
     Hist_Dep_Mum_Phi_Mass->SetBinContent(j+1, Vec_Hist_Mum_Phi_Mass[j]->GetMean());
 
     Hist_Dep_CST_Mass->SetBinContent(j+1, Vec_Hist_CST_Mass[j]->GetMean());
     Hist_Dep_Phi_Mass->SetBinContent(j+1, Vec_Hist_Phi_Mass[j]->GetMean());

  }
}
*/
#endif

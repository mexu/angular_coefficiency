#ifndef _CONST_NUMBER_
#define _CONST_NUMBER_
#include <iostream>
using namespace std;
namespace const_num{

  const int nbins_afb_al = 17;
  const double bins_afb_al[nbins_afb_al+1] = { 60., 70., 80., 84., 86., 88., 89., 90., 90.5, 91., 91.5, 92., 93., 94., 96, 100., 120., 160. } ;
//  const int nbin_afb_al = 14;
//  const double bin_afb_al[nbin_afb_al+1] = { 80., 84., 86., 88., 89., 90., 90.5, 91., 91.5, 92., 93., 94., 96, 100., 120.} ;
  const int nbins_eta = 12;
  const double bins_eta[nbins_eta+1] = {1.8,2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5,5.0};
  const int nbins_phi = 10;
  const double bins_phi[nbins_phi+1] = {-3.14159, -2.51327, -1.88496, -1.25664, -0.628319, 0, 0.628319, 1.25664, 1.88496, 2.51327, 3.14159};
  const int nbins_cs = 10;
  const double bins_cs[nbins_cs+1] = {-1.0,-0.75,-0.55,-0.35,-0.05,0.05,0.2,0.35,0.55,0.75,1.0};
  const int nbins_pt = 12;
  const double bins_pt[nbins_pt+1] = {10,15,20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70};
  const int nbins_spd = 20;
  const double bins_spd[nbins_spd+1] = {0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000};


  const int _hist_colors[10]   = {2, 4, 6, 1, 45, 6, 7, 8, 9, 1};
  const int _marker_styles[10] = {24, 25, 26, 27, 20, 30, 22, 22, 20, 21};
  const int _line_styles[10] = {1, 1, 1, 1, 1, 2, 3, 3, 3, 4};
  const int _fill_styles[10] = {3004,3009, 3007, 1, 1, 2, 3, 3, 3, 4};

  const int n_format = 1;
  const string format[4] = {".pdf",".eps",".C",".png"};

  const int nbins_fit_Z_pt = 6;
  const double bins_fit_Z_pt[nbins_fit_Z_pt+1] = {0.0,10.0,20.0,35.0,55.0,80,100.0};

  const string LFN_gauss_gene  = "/eos/lhcb/user/m/mexu/Z2MuMu/Gauss_Gene/";
  //const string LFN_mc = "/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/BackUp";
  const string LFN_mc = "/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC";
  const string LFN_data = " /projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data";
  const string LFN = " /projects/lhcb/users/mexu/Z2MuMu/DVTuples";
  const string LFN_Eff = "/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency";


  const double par_A0[2] = {-0.6,0.2};
  const double par_A1[2] = {-0.2,0.2};
  const double par_A2[2] = {0.0,0.7};
  const double par_A3[2] = {-0.1,0.1};
  const double par_A4[2] = {-0.1,0.1};
  const double par_A5[2] = {-0.1,0.1};
  const double par_A6[2] = {-0.1,0.1};

  const int pt_bin = 60;  const double pt_min = 0.0;   const double pt_max = 80;
  const int cs_bin = 60;  const double cs_min = -0.8;  const double cs_max = 0.8;
  const int phi_bin =60;  const double phi_min = -2.8; const double phi_max = 2.8;
  const int eta_bin = 60; const double eta_min = -5.0;  const double eta_max = 5.0;
  const int nbins_mass = 60; const double mass_min = 60; const double mass_max =120;

//***************************************************
// Readin tuples
//***************************************************
//
  const double E_Lumi = 0.015;
}
#endif

#ifndef _ALGOWZ_
#define _ALGOWZ_

#include <iostream>
#include <TLorentzVector.h>
#include <TROOT.h>
#include <TMath.h>
#include <vector>
#include <TH1.h>
const int nbins_eta = 10;
const double bins_eta[nbins_eta+1] = {2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.25, 4.5};
const int nbins_phi = 10;
const double bins_phi[nbins_phi+1] = {-3.14159, -2.51327, -1.88496, -1.25664, -0.628319, 0, 0.628319, 1.25664, 1.88496, 2.51327, 3.14159};
const int nbins_cs = 10;
const double bins_cs[nbins_cs+1] = {-1.0,-0.75,-0.55,-0.35,-0.05,0.05,0.2,0.35,0.55,0.75,1.0};
const int nbins_pt = 10;
const double bins_pt[nbins_pt+1] = {10, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70};

void Draw_Dependence(vector<TLorentzVector> _mum_save, vector<TLorentzVector> _mup_save){

   TH1D* Hist_Dep_Mup_Eta_PT  = new TH1D("Hist_Dep_Mup_Eta_PT", "Hist_Dep_Mup_Eta_PT",nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mup_Eta_Phi = new TH1D("Hist_Dep_Mup_Eta_Phi","Hist_Dep_Mup_Eta_Phi", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mup_Eta_CST = new TH1D("Hist_Dep_Mup_Eta_CST","Hist_Dep_Mup_Eta_CST", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mup_Eta_Mass = new TH1D("Hist_Dep_Mup_Eta_Mass","Hist_Dep_Mup_Eta_Mass", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mup_PT_Mass = new TH1D("Hist_Dep_Mup_PT_Mass","Hist_Dep_Mup_PT_Mass", nbins_pt,bins_pt);
   TH1D* Hist_Dep_Mup_Phi_Mass = new TH1D("Hist_Dep_Mup_Phi_Mass","Hist_Dep_Mup_Phi_Mass", nbins_phi,bins_phi);

   TH1D* Hist_Dep_Mum_Eta_PT  = new TH1D("Hist_Dep_Mum_Eta_PT", "Hist_Dep_Mum_Eta_PT",nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mum_Eta_Phi = new TH1D("Hist_Dep_Mum_Eta_Phi","Hist_Dep_Mum_Eta_Phi", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mum_Eta_CST = new TH1D("Hist_Dep_Mum_Eta_CST","Hist_Dep_Mum_Eta_CST", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mum_Eta_Mass = new TH1D("Hist_Dep_Mum_Eta_Mass","Hist_Dep_Mum_Eta_Mass", nbins_eta,bins_eta);
   TH1D* Hist_Dep_Mum_PT_Mass = new TH1D("Hist_Dep_Mum_PT_Mass","Hist_Dep_Mum_PT_Mass", nbins_pt,bins_pt);
   TH1D* Hist_Dep_Mum_Phi_Mass = new TH1D("Hist_Dep_Mum_Phi_Mass","Hist_Dep_Mum_Phi_Mass", nbins_phi,bins_phi);
  
   TH1D* Hist_Dep_CST_Mass = new TH1D("Hist_Dep_CST_Mass","Hist_Dep_CST_Mass", nbins_cs,bins_cs);
   TH1D* Hist_Dep_Phi_Mass = new TH1D("Hist_Dep_Phi_Mass","Hist_Dep_Phi_Mass", nbins_cs,bins_cs);

   vector<TH1D*> Vec_Hist_Mup_Eta_PT;   vector<TH1D*> Vec_Hist_Mum_Eta_PT;
   vector<TH1D*> Vec_Hist_Mup_Eta_Phi;  vector<TH1D*> Vec_Hist_Mum_Eta_Phi;
   vector<TH1D*> Vec_Hist_Mup_Eta_CST;  vector<TH1D*> Vec_Hist_Mum_Eta_CST;
   vector<TH1D*> Vec_Hist_Mup_Eta_Mass; vector<TH1D*> Vec_Hist_Mum_Eta_Mass;
   vector<TH1D*> Vec_Hist_Mup_PT_Mass;  vector<TH1D*> Vec_Hist_Mum_PT_Mass;
   vector<TH1D*> Vec_Hist_Mup_Phi_Mass;  vector<TH1D*> Vec_Hist_Mum_Phi_Mass;
   
   vector<TH1D*> Vec_Hist_CST_Mass; vector<TH1D*> Vec_Hist_Phi_Mass;

   Vec_Hist_Mup_Eta_PT.clear();    Vec_Hist_Mum_Eta_PT.clear();
   Vec_Hist_Mup_Eta_Phi.clear();   Vec_Hist_Mum_Eta_Phi.clear();
   Vec_Hist_Mup_Eta_CST.clear();   Vec_Hist_Mum_Eta_CST.clear();
   Vec_Hist_Mup_Eta_Mass.clear();   Vec_Hist_Mum_Eta_Mass.clear();
   Vec_Hist_Mup_PT_Mass.clear();   Vec_Hist_Mum_PT_Mass.clear();
   Vec_Hist_Mup_Phi_Mass.clear();   Vec_Hist_Mum_Phi_Mass.clear();
 
   Vec_Hist_CST_Mass.clear();   Vec_Hist_CST_Mass.clear();
   Vec_Hist_Phi_Mass.clear();   Vec_Hist_Phi_Mass.clear();

   for(int i =0; i<nbins_eta;i++){

     TH1D* Hist_Tmp_Mup_Eta_PT  = new TH1D(Form("Hist_Tmp_Mup_Eta_PTBin%d",i),Form("Hist_Tmp_Mup_Eta_PTBin%d",i),50,0.0,120.0);
     TH1D* Hist_Tmp_Mup_Eta_Phi = new TH1D(Form("Hist_Tmp_Mup_Eta_PhiBin%d",i),Form("Hist_Tmp_Mup_Eta_PhiBin%d",i),50,-3.14,3.14);
     TH1D* Hist_Tmp_Mup_Eta_CST = new TH1D(Form("Hist_Tmp_Mup_Eta_CSTBin%d",i),Form("Hist_Tmp_Mup_Eta_CSTBin%d",i),50,-1.0,1.0);
     TH1D* Hist_Tmp_Mup_Eta_Mass = new TH1D(Form("Hist_Tmp_Mup_Eta_MassBin%d",i),Form("Hist_Tmp_Mup_Eta_MassBin%d",i),50,50,150);
     TH1D* Hist_Tmp_Mup_PT_Mass = new TH1D(Form("Hist_Tmp_Mup_PT_MassBin%d",i),Form("Hist_Tmp_Mup_PT_MassBin%d",i),50,50,150);
     TH1D* Hist_Tmp_Mup_Phi_Mass = new TH1D(Form("Hist_Tmp_Mup_Phi_MassBin%d",i),Form("Hist_Tmp_Mup_Phi_MassBin%d",i),50,50,150);
   
     TH1D* Hist_Tmp_Mum_Eta_PT  = new TH1D(Form("Hist_Tmp_Mum_Eta_PTBin%d",i),Form("Hist_Tmp_Mum_Eta_PTBin%d",i),50,0.0,120.0);
     TH1D* Hist_Tmp_Mum_Eta_Phi = new TH1D(Form("Hist_Tmp_Mum_Eta_PhiBin%d",i),Form("Hist_Tmp_Mum_Eta_PhiBin%d",i),50,-3.14,3.14);
     TH1D* Hist_Tmp_Mum_Eta_CST = new TH1D(Form("Hist_Tmp_Mum_Eta_CSTBin%d",i),Form("Hist_Tmp_Mum_Eta_CSTBin%d",i),50,-1.0,1.0);
     TH1D* Hist_Tmp_Mum_Eta_Mass = new TH1D(Form("Hist_Tmp_Mum_Eta_MassBin%d",i),Form("Hist_Tmp_Mum_Eta_MassBin%d",i),50,50,150);
     TH1D* Hist_Tmp_Mum_PT_Mass = new TH1D(Form("Hist_Tmp_Mum_PT_MassBin%d",i),Form("Hist_Tmp_Mum_PT_MassBin%d",i),50,50,150);
     TH1D* Hist_Tmp_Mum_Phi_Mass = new TH1D(Form("Hist_Tmp_Mum_Phi_MassBin%d",i),Form("Hist_Tmp_Mum_Phi_MassBin%d",i),50,50,150);
 
     TH1D* Hist_Tmp_CST_Mass = new TH1D(Form("Hist_Tmp_CST_MassBin%d",i),Form("Hist_Tmp_CST_MassBin%d",i),50,50,150);
     TH1D* Hist_Tmp_Phi_Mass = new TH1D(Form("Hist_Tmp_Phi_MassBin%d",i),Form("Hist_Tmp_Phi_MassBin%d",i),50,50,150);

     Vec_Hist_Mup_Eta_PT.push_back(Hist_Tmp_Mup_Eta_PT);
     Vec_Hist_Mup_Eta_Phi.push_back(Hist_Tmp_Mup_Eta_Phi);
     Vec_Hist_Mup_Eta_CST.push_back(Hist_Tmp_Mup_Eta_CST);
     Vec_Hist_Mup_Eta_Mass.push_back(Hist_Tmp_Mup_Eta_Mass);
     Vec_Hist_Mup_PT_Mass.push_back(Hist_Tmp_Mup_PT_Mass);
     Vec_Hist_Mup_Phi_Mass.push_back(Hist_Tmp_Mup_Phi_Mass);

     Vec_Hist_Mum_Eta_PT.push_back(Hist_Tmp_Mum_Eta_PT);
     Vec_Hist_Mum_Eta_Phi.push_back(Hist_Tmp_Mum_Eta_Phi);
     Vec_Hist_Mum_Eta_CST.push_back(Hist_Tmp_Mum_Eta_CST);
     Vec_Hist_Mum_Eta_Mass.push_back(Hist_Tmp_Mum_Eta_Mass);
     Vec_Hist_Mum_PT_Mass.push_back(Hist_Tmp_Mum_PT_Mass);
     Vec_Hist_Mum_Phi_Mass.push_back(Hist_Tmp_Mum_Phi_Mass);

     Vec_Hist_CST_Mass.push_back(Hist_Tmp_CST_Mass);
     Vec_Hist_Phi_Mass.push_back(Hist_Tmp_Phi_Mass);
  }

  for(int i=0; i<_mup_save.size();i++){
//    double mup_Pt     = _mup_save[i].Pt();
  //  double mum_Pt     = _mum_save[i].Pt();
  //  double mup_Eta    = _mup_save[i].Eta();
  //  double mum_Eta    = _mum_save[i].Eta(); 
  //  double Zmas      = (_mup_save[i]+_mum_save[i]).M();
  //  double collins    = Collins_Soper(_mum_save[i],_mup_save[i]);
  //  double phi        = Collins_Soper_Phi(_mum_save[i], _mup_save[i]);
  //  for(int j=0;j<nbins_pt;j++){
  //    if(mup_Eta>bins_eta[j]&&mup_Eta<bins_eta[j+1]){
  //      Vec_Hist_Mup_Eta_PT[j]->Fill(mup_Pt);
  //      Vec_Hist_Mup_Eta_Phi[j]->Fill(_mup_save[i].Phi()); 
  //      Vec_Hist_Mup_Eta_CST[j]->Fill(collins);
  //      Vec_Hist_Mup_Eta_Mass[j]->Fill(Zmas);
  //     }

  //    if(mum_Eta>bins_eta[j]&&mum_Eta<bins_eta[j+1]){
  //      Vec_Hist_Mum_Eta_PT[j]->Fill(mum_Pt);
  //      Vec_Hist_Mum_Eta_Phi[j]->Fill(_mum_save[i].Phi());
  //      Vec_Hist_Mum_Eta_CST[j]->Fill(collins);
  //      Vec_Hist_Mum_Eta_Mass[j]->Fill(Zmas);
  //    }

  //  if(mup_Pt>bins_pt[j]&&mup_Pt<bins_pt[j+1]) Vec_Hist_Mup_PT_Mass[j]->Fill(Zmas);
  //  if(mum_Pt>bins_pt[j]&&mum_Pt<bins_pt[j+1]) Vec_Hist_Mum_PT_Mass[j]->Fill(Zmas);

  //  if(_mup_save[i].Phi()>bins_phi[j]&&_mup_save[i].Phi()<bins_phi[j+1]) Vec_Hist_Mup_Phi_Mass[j]->Fill(Zmas);
  //  if(_mum_save[i].Phi()>bins_phi[j]&&_mum_save[i].Phi()<bins_phi[j+1]) Vec_Hist_Mum_Phi_Mass[j]->Fill(Zmas);

  //  if(collins>bins_cs[j]&&collins<bins_cs[j+1]) Vec_Hist_CST_Mass[j]->Fill(Zmas);
  //  if(phi>bins_cs[j]&&phi<bins_cs[j+1])         Vec_Hist_Phi_Mass[j]->Fill(Zmas);
  //   }
  }
/*
   for(int j=0;j<nbins_pt;j++){
     Hist_Dep_Mup_Eta_PT->SetBinContent(j+1, Vec_Hist_Mup_Eta_PT[j]->GetMean());
     Hist_Dep_Mup_Eta_Phi->SetBinContent(j+1, Vec_Hist_Mup_Eta_Phi[j]->GetMean());
     Hist_Dep_Mup_Eta_CST->SetBinContent(j+1, Vec_Hist_Mup_Eta_CST[j]->GetMean());
     Hist_Dep_Mup_Eta_Mass->SetBinContent(j+1, Vec_Hist_Mup_Eta_Mass[j]->GetMean());
     Hist_Dep_Mup_PT_Mass->SetBinContent(j+1, Vec_Hist_Mup_PT_Mass[j]->GetMean());
     Hist_Dep_Mup_Phi_Mass->SetBinContent(j+1, Vec_Hist_Mup_Phi_Mass[j]->GetMean());
  
     Hist_Dep_Mum_Eta_PT->SetBinContent(j+1, Vec_Hist_Mum_Eta_PT[j]->GetMean());
     Hist_Dep_Mum_Eta_Phi->SetBinContent(j+1, Vec_Hist_Mum_Eta_Phi[j]->GetMean());
     Hist_Dep_Mum_Eta_CST->SetBinContent(j+1, Vec_Hist_Mum_Eta_CST[j]->GetMean());
     Hist_Dep_Mum_Eta_Mass->SetBinContent(j+1, Vec_Hist_Mum_Eta_Mass[j]->GetMean());
     Hist_Dep_Mum_PT_Mass->SetBinContent(j+1, Vec_Hist_Mum_PT_Mass[j]->GetMean());
     Hist_Dep_Mum_Phi_Mass->SetBinContent(j+1, Vec_Hist_Mum_Phi_Mass[j]->GetMean());
 
     Hist_Dep_CST_Mass->SetBinContent(j+1, Vec_Hist_CST_Mass[j]->GetMean());
     Hist_Dep_Phi_Mass->SetBinContent(j+1, Vec_Hist_Phi_Mass[j]->GetMean());

  }
*/
}
#endif

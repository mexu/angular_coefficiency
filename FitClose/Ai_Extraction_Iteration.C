#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooArgList.h"
#include "RooAddPdf.h"
#include "RooEffProd.h"
#include "inputs.hpp"

#include "Fit_Fun.h"
#pragma cling load("Fit_Fun_cxx.so")

//
// calcualte the acc for each reweighted MC sample
//
void Cal_Acc_iteration(string _Year, int _Step, int study_type, int default_Ai){
  cout<<" Processing Cal_Acc_iteration() ..."<<endl;
  // name for test type
  char testtype[200];
  sprintf(testtype, "Type_%d_Ai_%d", study_type, default_Ai);

  //Read in Root File
  double z_Y, z_PT, Align_CS, Align_PHI, CS_TRUE, Phi_TRUE;
  double gb_weights, ai_weight; 

  TChain *Ch;
  if(study_type %2 == 1) // fix me
    Ch = new TChain("AnaTreeA1");
  else
    Ch = new TChain("AnaTreeB1");

  if(_Step == 0)
    Ch->Add(Form("./outputs/MC_%s.root", _Year.c_str()));
  else 
    Ch->Add(Form("./outputs/MCsample_step_%d_%s_%s.root", _Step-1, _Year.c_str(), testtype));

  // set root branch 
  Ch->SetBranchAddress("z_Y",         &z_Y);
  Ch->SetBranchAddress("z_PT",        &z_PT);
  Ch->SetBranchAddress("Align_CS",    &Align_CS);
  Ch->SetBranchAddress("Align_PHI",   &Align_PHI);
  Ch->SetBranchAddress("CS_TRUE",     &CS_TRUE);
  Ch->SetBranchAddress("Phi_TRUE",    &Phi_TRUE);
  //if(_Step>0)
    //Ch->SetBranchAddress("ai_weight", &ai_weight);

  // read GB-weight
  TChain* Ch_G;
  if(_Step > 0){
    Ch_G = new TChain("DecayTree");
    Ch_G->Add(Form("./outputs/gbreweight_step_%d_%s_%s.root", _Step, _Year.c_str(), testtype));
    Ch_G->SetBranchAddress("gb_weights", &gb_weights);
    Ch_G->SetBranchAddress("ai_weight",  &ai_weight);
  }

  //Store event information
  vector<double> Vec_PT;    Vec_PT.clear();
  vector<double> Vec_Y;     Vec_Y.clear();
  vector<double> Vec_Phi;   Vec_Phi.clear();
  vector<double> Vec_CS;    Vec_CS.clear();
  vector<double> Vec_PhiG;  Vec_PhiG.clear();
  vector<double> Vec_CSG;   Vec_CSG.clear();
  vector<double> Vec_GB_W;  Vec_GB_W.clear();
  vector<double> Vec_Ai_W;  Vec_Ai_W.clear();

  // loop events
  for(int i = 0; i < Ch->GetEntries(); i++){
    Ch->GetEntry(i);
    Vec_PT.push_back(z_PT);
    Vec_Y.push_back(z_Y);
    Vec_PhiG.push_back(Phi_TRUE);
    Vec_CSG.push_back(CS_TRUE);
    Vec_Phi.push_back(Align_PHI);
    Vec_CS.push_back(Align_CS);
  }

  // loop GB-weight
  if(_Step > 0){
    for(int i = 0; i < Ch_G->GetEntries(); i++){
      Ch_G->GetEntry(i);
      Vec_GB_W.push_back(gb_weights);
      Vec_Ai_W.push_back(ai_weight);
    }
  }

  // read predicted or measured Ai
  TFile* File_Result;
  if(_Step == 0){ 
  /*
    if(default_Ai)
      File_Result = new TFile("outputs/PYTHIA_Predictions.root");
    else{
      if(study_type %2 == 1)
        File_Result = new TFile("outputs/Generator_2016_Type_110_measured.root"); // measured input Ai for Sample-A; fix me
      else
        File_Result = new TFile("outputs/Generator_2016_Type_111_measured.root"); // measured input Ai for Sample-B
    }
  */
    File_Result = new TFile("outputs/PYTHIA_Predictions.root");
  }
  // read results from previous step: _Step-1
  else 
    File_Result = new TFile(Form("outputs/Result_PT_step_%d_%s_%s.root", _Step-1, _Year.c_str(), testtype));

  TH1D* Hist_F_A0 = (TH1D*)File_Result->Get("Hist_FA0");
  TH1D* Hist_F_A1 = (TH1D*)File_Result->Get("Hist_FA1");
  TH1D* Hist_F_A2 = (TH1D*)File_Result->Get("Hist_FA2");
  TH1D* Hist_F_A3 = (TH1D*)File_Result->Get("Hist_FA3");
  TH1D* Hist_F_A4 = (TH1D*)File_Result->Get("Hist_FA4");

  // create a new root file to save scale factors information
  TFile *_file = new TFile(Form("./outputs/Acc_step_%d_%s_%s.root", _Step, _Year.c_str(), testtype),"recreate");
  TH1D *Hist_S1 = new TH1D("Hist_S1", "", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_S2 = new TH1D("Hist_S2", "", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_S3 = new TH1D("Hist_S3", "", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_S4 = new TH1D("Hist_S4", "", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_S5 = new TH1D("Hist_S5", "", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_S6 = new TH1D("Hist_S6", "", nbins_fit_Z_pt, bins_fit_Z_pt);

  //Check Fitting Result
  double Test1[nbins_fit_Z_pt], Test2[nbins_fit_Z_pt], Test3[nbins_fit_Z_pt], Test4[nbins_fit_Z_pt], Test5[nbins_fit_Z_pt], Test6[nbins_fit_Z_pt];
  //Fitting Process 
  for(int i = 0; i < nbins_fit_Z_pt; i++){
   Test1[i] = Test2[i] = Test3[i] = Test4[i] = Test5[i] = Test6[i] = 0;
  }

  for(int j = 0; j < Vec_PT.size(); j++){
    if(Vec_PT[j] < 0 || Vec_PT[j] > 100) continue;
    int zpt_bin = Get_ZPT_Bin(Vec_PT[j]);

    double P1 =  1.0+Vec_CS[j]*Vec_CS[j];
    double P2 =  0.5*(Hist_F_A0->GetBinContent(zpt_bin))*(1-3*Vec_CS[j]*Vec_CS[j]);
    double P3 =  (Hist_F_A1->GetBinContent(zpt_bin))*2*Vec_CS[j]*(sqrt(1-Vec_CS[j]*Vec_CS[j]))*cos(Vec_Phi[j]);
    double P4 =  0.5*(Hist_F_A2->GetBinContent(zpt_bin))*(1-Vec_CS[j]*Vec_CS[j])*cos(2.*Vec_Phi[j]);
    double P5 =  (Hist_F_A3->GetBinContent(zpt_bin))*(sqrt(1-Vec_CS[j]*Vec_CS[j]))*cos(Vec_Phi[j]);
    double P6 =  (Hist_F_A4->GetBinContent(zpt_bin))*Vec_CS[j];

    double MCGen = (P1+P2+P3+P4+P5+P6)*1.0;
    if(_Step>0){
      MCGen = MCGen/(Vec_GB_W[j]);
    }

    Test1[zpt_bin-1] +=  (1+ Vec_CS[j]*Vec_CS[j])*1.0/MCGen;
    Test2[zpt_bin-1] +=  0.5*(1-3*Vec_CS[j]*Vec_CS[j])*1.0/MCGen;
    Test3[zpt_bin-1] +=  2*Vec_CS[j]*(sqrt(1-Vec_CS[j]*Vec_CS[j]))*cos(Vec_Phi[j])*1.0/MCGen;
    Test4[zpt_bin-1] +=  0.5*(1-Vec_CS[j]*Vec_CS[j])*cos(2.*Vec_Phi[j])*1.0/MCGen;
    Test5[zpt_bin-1] +=  (sqrt(1-Vec_CS[j]*Vec_CS[j]))*cos(Vec_Phi[j])*1.0/MCGen;
    Test6[zpt_bin-1] +=  Vec_CS[j]*1.0/MCGen;
  }

  for(int i = 0; i < nbins_fit_Z_pt; i++){
   Hist_S1->SetBinContent(i+1,Test1[i]*1.0/Test1[i]);
   Hist_S2->SetBinContent(i+1,Test2[i]*1.0/Test1[i]);
   Hist_S3->SetBinContent(i+1,Test3[i]*1.0/Test1[i]);
   Hist_S4->SetBinContent(i+1,Test4[i]*1.0/Test1[i]);
   Hist_S5->SetBinContent(i+1,Test5[i]*1.0/Test1[i]);
   Hist_S6->SetBinContent(i+1,Test6[i]*1.0/Test1[i]);
   //cout<<"  --> Normalisation factors: "<<i<<"  "<<Test1[i]*1.0/Test1[i]<<"   "<<Test2[i]*1.0/Test1[i]<<"   "<<Test3[i]*1.0/Test1[i]<<"    "<<Test4[i]*1.0/Test1[i]<<"    "<<Test5[i]*1.0/Test1[i]<<"    "<<Test6[i]*1.0/Test1[i]<<endl;
  }

  _file->Write();
  _file->Close();
}

//
// extract Ai with updated MC sample
//
void Extract_Result_PT_iteration(string _Year, int _Step, int study_type, int default_Ai){
  cout<<" Processing Extract_Result_PT_iteration() ..."<<endl;
  // name for test type
  char testtype[200];
  sprintf(testtype, "Type_%d_Ai_%d", study_type, default_Ai);

  //Read in Acc Information: previous step, from Cal_Acc_iteration.C 
  string Acc_Result = Form("./outputs/Acc_step_%d_%s_%s.root", _Step, _Year.c_str(), testtype);
  TFile* File_Acc_Full = new TFile(Acc_Result.c_str());
  TH1D* Hist_F_S1 = (TH1D*)File_Acc_Full->Get("Hist_S1");
  TH1D* Hist_F_S2 = (TH1D*)File_Acc_Full->Get("Hist_S2");
  TH1D* Hist_F_S3 = (TH1D*)File_Acc_Full->Get("Hist_S3");
  TH1D* Hist_F_S4 = (TH1D*)File_Acc_Full->Get("Hist_S4");
  TH1D* Hist_F_S5 = (TH1D*)File_Acc_Full->Get("Hist_S5");
  TH1D* Hist_F_S6 = (TH1D*)File_Acc_Full->Get("Hist_S6");

  //Read in Data Tuple: (1.5 * Ai) or (0.0 * Ai) as input
  string tree_name = GetTreeName(study_type);
  TChain *Ch = new TChain(tree_name.c_str());
  
  if(default_Ai)
    Ch->Add(Form("outputs/MC_%s.root", _Year.c_str()));
  else
    Ch->Add(Form("./outputs/MC_%s_Measured.root", _Year.c_str())); // fix me

  double z_PT, z_Y, ai_weight, Align_PHI, Align_CS;
  double mup_PT, mup_ETA, mum_PT, mum_ETA; 
  Ch->SetBranchAddress("z_PT",      &z_PT);
  Ch->SetBranchAddress("z_Y",       &z_Y);
  Ch->SetBranchAddress("Align_CS",  &Align_CS);
  Ch->SetBranchAddress("Align_PHI", &Align_PHI);
  Ch->SetBranchAddress("ai_weight", &ai_weight);

  //Store event information
  vector<double> Vec_PT;  Vec_PT.clear();
  vector<double> Vec_Y;   Vec_Y.clear();
  vector<double> Vec_Phi; Vec_Phi.clear();
  vector<double> Vec_CS;  Vec_CS.clear();
  vector<double> Vec_W;   Vec_W.clear();

  for(int i = 0; i < Ch->GetEntries(); i++){
    Ch->GetEntry(i);
    Vec_PT.push_back(z_PT);
    Vec_Y.push_back(z_Y);
    Vec_Phi.push_back(Align_PHI);
    Vec_CS.push_back(Align_CS);
    Vec_W.push_back(ai_weight);
  }

  double corre[nbins_fit_Z_pt+1] = {0.};

  // Prepare output directory
  string _Output = Form("Result_PT_step_%d_%s_%s", _Step, _Year.c_str(), testtype);
  TFile *_file = new TFile(Form("./outputs/%s.root", _Output.c_str()), "recreate");
  TH1D *Hist_FA0  = new TH1D("Hist_FA0",  "", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_FA1  = new TH1D("Hist_FA1",  "", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_FA2  = new TH1D("Hist_FA2",  "", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_FA3  = new TH1D("Hist_FA3",  "", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_FA4  = new TH1D("Hist_FA4",  "", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_FA02 = new TH1D("Hist_FA02", "", nbins_fit_Z_pt, bins_fit_Z_pt);

  //Fitting Process 
  for(int i = 0; i < nbins_fit_Z_pt; i++){
    RooRealVar *_CS     = new RooRealVar("_CS",   "", cs_fit_min,  cs_fit_max);
    RooRealVar *_Phi    = new RooRealVar("_Phi",  "", phi_fit_min, phi_fit_max);
    RooRealVar *_Weight = new RooRealVar("swits", "swits", -3.00, 3.000);
    RooArgList *_Vars   = new RooArgList(*_CS, *_Phi, *_Weight);
    RooDataSet *_Data   = new RooDataSet("_Data", "weighted data", RooArgSet(*_Vars));

    for(int j = 0; j < Vec_PT.size(); j++){
      if((1-Vec_CS[j]*Vec_CS[j])>0){
        if(Vec_PT[j]<bins_fit_Z_pt[i]) continue;
	      if(Vec_PT[j]>bins_fit_Z_pt[i+1]) continue;
  
	      *_CS     = Vec_CS[j];
	      *_Phi    = Vec_Phi[j];
	      *_Weight = Vec_W[j];
	      _Data->add(RooArgList(*_CS,*_Phi,*_Weight));
      }
    }

    RooRealVar *A0 = new RooRealVar("A0", "A0", 0.001, -1.0, 1.0);
    RooRealVar *A1 = new RooRealVar("A1", "A1", -0.02, -1.0, 1.0);
    RooRealVar *A2 = new RooRealVar("A2", "A2", 0.100, -1.0, 1.0);
    RooRealVar *A3 = new RooRealVar("A3", "A3", 0.027, -1.0, 1.0);
    RooRealVar *A4 = new RooRealVar("A4", "A4", 0.020, -1.0, 1.0);

    cout<<"=================================================="<<endl;
    cout<<" Scale factors: "<<endl;
    cout<<Hist_F_S1->GetBinContent(i+1)<<endl;
    cout<<Hist_F_S2->GetBinContent(i+1)<<endl;
    cout<<Hist_F_S3->GetBinContent(i+1)<<endl;
    cout<<Hist_F_S4->GetBinContent(i+1)<<endl;
    cout<<Hist_F_S5->GetBinContent(i+1)<<endl;
    cout<<Hist_F_S6->GetBinContent(i+1)<<endl;
    cout<<"=================================================="<<endl;

    RooRealVar *S1 = new RooRealVar("S1", "S1", Hist_F_S1->GetBinContent(i+1));
    RooRealVar *S2 = new RooRealVar("S2", "S2", Hist_F_S2->GetBinContent(i+1));
    RooRealVar *S3 = new RooRealVar("S3", "S3", Hist_F_S3->GetBinContent(i+1));
    RooRealVar *S4 = new RooRealVar("S4", "S4", Hist_F_S4->GetBinContent(i+1));
    RooRealVar *S5 = new RooRealVar("S5", "S5", Hist_F_S5->GetBinContent(i+1));
    RooRealVar *S6 = new RooRealVar("S6", "S6", Hist_F_S6->GetBinContent(i+1));

    RooDataSet *_Wdata = new RooDataSet(_Data->GetName(), _Data->GetTitle(), _Data, *_Data->get(), 0, _Weight->GetName());
    RooAbsPdf *_Model  = new Fit_Fun("model_CS", "model_CS", *_CS, *_Phi, *A0, *A1, *A2, *A3, *A4, *S1, *S2, *S3, *S4, *S5, *S6);

    RooFitResult *fitr = _Model->fitTo(*_Wdata, NumCPU(10), Save(), SumW2Error(true));
  
    Hist_FA0->SetBinContent(i+1,  A0->getVal());    Hist_FA0->SetBinError(i+1, A0->getError());
    Hist_FA1->SetBinContent(i+1,  A1->getVal());    Hist_FA1->SetBinError(i+1, A1->getError());
    Hist_FA2->SetBinContent(i+1,  A2->getVal());    Hist_FA2->SetBinError(i+1, A2->getError());
    Hist_FA3->SetBinContent(i+1,  A3->getVal());    Hist_FA3->SetBinError(i+1, A3->getError());
    Hist_FA4->SetBinContent(i+1,  A4->getVal());    Hist_FA4->SetBinError(i+1, A4->getError());
    Hist_FA02->SetBinContent(i+1, (A0->getVal())-(A2->getVal()));   
    Hist_FA02->SetBinError(i+1, sqrt(A0->getError()*A0->getError()+A2->getError()*A2->getError()));
   
    cout<<"=============================================================="<<endl;
    cout<<"     Fitted results; Bin Index "<<i                               <<endl;
    cout<<"=============================================================="<<endl;
    cout<<A0->getVal()<<endl;
    cout<<A1->getVal()<<endl;
    cout<<A2->getVal()<<endl;
    cout<<A3->getVal()<<endl;
    cout<<A4->getVal()<<endl;

    TMatrixDSym cormat = fitr->correlationMatrix();
    cormat.Print();
    double tmp = cormat(0,2);
    corre[i] = tmp;

    delete _CS;
    delete _Phi;
    delete _Weight;
    delete _Vars;
    delete _Data;
    delete A0;
    delete A1;
    delete A2;
    delete A3;
    delete A4;
    delete S1;
    delete S2;
    delete S3;
    delete S4;
    delete S5;
    delete S6;
    delete _Wdata;
    delete _Model;
    delete fitr;
  }
  ofstream out(Form("outputs/Corre_step_%d_%s_%s.hpp", _Step, _Year.c_str(), testtype));
  out<<"double corr_"<<_Year.c_str()<<"["<<nbins_fit_Z_pt<<"]={";
  for(int i = 0; i < nbins_fit_Z_pt; i++){
    if(i == nbins_fit_Z_pt-1)  out<<corre[i]<<"};";
    else   out<<corre[i]<<",";
  }
  out.close();

  //Hist_FA0->Write();
  //Hist_FA1->Write();
  //Hist_FA2->Write();
  //Hist_FA3->Write();
  //Hist_FA4->Write();
  //Hist_FA02->Write();

  _file->Write();
  _file->Close();
}

//
// prepare a new MC root file for next step
//
void Prepare_GBReweight(string _Year, int _Step, int study_type, int default_Ai){
  cout<<" Processing Prepare_GBReweight() ..."<<endl;
  // name for test type
  char testtype[200];
  sprintf(testtype, "Type_%d_Ai_%d", study_type, default_Ai);

  // input prediction
  TFile* File_original;
  File_original = new TFile("outputs/PYTHIA_Predictions.root"); // fix me, for 121, this should use sample-A
  /*
      if(default_Ai)
      File_original = new TFile("outputs/PYTHIA_Predictions.root");
    else{
      if(study_type %2 == 1)
        File_original = new TFile("outputs/Generator_2016_Type_110_measured.root"); // measured input Ai for Sample-A; fix me
      else
        File_original = new TFile("outputs/Generator_2016_Type_111_measured.root"); // measured input Ai for Sample-B
    }
  */
  TH1D* Hist_O_A0 = (TH1D*)File_original->Get("Hist_FA0");
  TH1D* Hist_O_A1 = (TH1D*)File_original->Get("Hist_FA1");
  TH1D* Hist_O_A2 = (TH1D*)File_original->Get("Hist_FA2");
  TH1D* Hist_O_A3 = (TH1D*)File_original->Get("Hist_FA3");
  TH1D* Hist_O_A4 = (TH1D*)File_original->Get("Hist_FA4");

  // input measured results
  TFile* File_Result = new TFile(Form("outputs/Result_PT_step_%d_%s_%s.root", _Step, _Year.c_str(), testtype));

  TH1D* Hist_F_A0 = (TH1D*)File_Result->Get("Hist_FA0");
  TH1D* Hist_F_A1 = (TH1D*)File_Result->Get("Hist_FA1");
  TH1D* Hist_F_A2 = (TH1D*)File_Result->Get("Hist_FA2");
  TH1D* Hist_F_A3 = (TH1D*)File_Result->Get("Hist_FA3");
  TH1D* Hist_F_A4 = (TH1D*)File_Result->Get("Hist_FA4");

  // Read in Root File: with Ai as input
  double z_Y, z_PT, z_P, CS_TRUE, Phi_TRUE, Align_CS, Align_PHI;
  double ai_weight; 

  // read original MC events, which will be used for create a new root file for acc study
  TChain *Ch_old;
  if(study_type %2 == 1) 
    Ch_old = new TChain("AnaTreeA1");
  else
    Ch_old = new TChain("AnaTreeB1");
  Ch_old->Add(Form("./outputs/MC_%s.root", _Year.c_str()));
  Ch_old->SetBranchAddress("z_Y",         &z_Y);
  Ch_old->SetBranchAddress("z_P",         &z_P);
  Ch_old->SetBranchAddress("z_PT",        &z_PT);
  Ch_old->SetBranchAddress("Align_CS",    &Align_CS);
  Ch_old->SetBranchAddress("Align_PHI",   &Align_PHI);
  Ch_old->SetBranchAddress("CS_TRUE",     &CS_TRUE);
  Ch_old->SetBranchAddress("Phi_TRUE",    &Phi_TRUE);

  // create a new root file
  TFile* file_new = new TFile(Form("./outputs/MCsample_step_%d_%s_%s.root", _Step, _Year.c_str(), testtype), "recreate");
  TTree *Ch;
  if(study_type %2 == 1)  
    Ch = new TTree("AnaTreeA1", "AnaTreeA1");
  else
    Ch = new TTree("AnaTreeB1", "AnaTreeB1");
  Ch->Branch("z_Y",       &z_Y,       "z_Y/D");
  Ch->Branch("z_P",       &z_P,       "z_P/D");
  Ch->Branch("z_PT",      &z_PT,      "z_PT/D");
  Ch->Branch("Align_CS",  &Align_CS,  "Align_CS/D");
  Ch->Branch("Align_PHI", &Align_PHI, "Align_PHI/D");
  Ch->Branch("CS_TRUE",   &CS_TRUE,   "CS_TRUE/D");
  Ch->Branch("Phi_TRUE",  &Phi_TRUE,  "Phi_TRUE/D");
  Ch->Branch("ai_weight", &ai_weight, "ai_weight/D");

  // loop MC events
  for(int i = 0; i < Ch_old->GetEntries(); i++){
    Ch_old->GetEntry(i);
  
    int zpt_bin = Get_ZPT_Bin(z_PT);

    double wt_original = 
      Get_Ai_Wt(CS_TRUE, Phi_TRUE, Hist_O_A0->GetBinContent(zpt_bin), 
                Hist_O_A1->GetBinContent(zpt_bin), Hist_O_A2->GetBinContent(zpt_bin), 
                Hist_O_A3->GetBinContent(zpt_bin), Hist_O_A4->GetBinContent(zpt_bin));
    double wt_new      = 
      Get_Ai_Wt(CS_TRUE, Phi_TRUE, Hist_F_A0->GetBinContent(zpt_bin), 
                Hist_F_A1->GetBinContent(zpt_bin), Hist_F_A2->GetBinContent(zpt_bin), 
                Hist_F_A3->GetBinContent(zpt_bin), Hist_F_A4->GetBinContent(zpt_bin));

    ai_weight = wt_new/wt_original;

    Ch->Fill();
  }

  file_new->Write();
  file_new->Close();
}

//
// comparison measured Ai
//
void Compare_Measured_Ai(string _Year, int _Step, int study_type, int default_Ai){
  cout<<" Processing Compare_Measured_Ai() ..."<<endl;
  // name for test type
  char testtype[200];
  sprintf(testtype, "Type_%d_Ai_%d", study_type, default_Ai);

  double factor = GetReweightFactor(study_type);

  // Readin File
  TFile* File_Gene;

  if(default_Ai) 
    File_Gene = new TFile("outputs/PYTHIA_Predictions.root");
  else{
    if(study_type %2 == 1)
      File_Gene = new TFile("outputs/Generator_2016_Type_111_measured.root"); // measured input Ai in Sample-B, fix me
    else
      File_Gene = new TFile("outputs/Generator_2016_Type_110_measured.root"); // measured input Ai in Sample-A    
  }

  TFile* File_Measured = new TFile(Form("./outputs/Result_PT_step_%d_%s_%s.root", _Step, _Year.c_str(), testtype));

  // Init Vector to store histogram 
  TH1D* Vec_Hist_Gene[n_Ai+1]; 
  TH1D* Vec_Hist_GeneM[n_Ai+1]; 
  TH1D* Vec_Hist_Measured[n_Ai+1];

  // predicted and measured Ai
  for(int m = 0; m < n_Ai; m++){ //A0,A1,A2,A3,A4
    Vec_Hist_Gene[m]     = (TH1D*)File_Gene->Get(Form("Hist_FA%d", m));
    Vec_Hist_Measured[m] = (TH1D*)File_Measured->Get(Form("Hist_FA%d", m));
  } 

  // modified Ai
  for(int i = 0; i < n_Ai; i++){
    Vec_Hist_GeneM[i] = new TH1D(Form("Hist_FMA%d", i), "", nbins_fit_Z_pt, bins_fit_Z_pt);
    for(int nbin = 0; nbin < nbins_fit_Z_pt; nbin++){
      double value = Vec_Hist_Gene[i]->GetBinContent(nbin+1);
      double error = Vec_Hist_Gene[i]->GetBinError(nbin+1);
      Vec_Hist_GeneM[i]->SetBinContent(nbin+1, value*factor);
      Vec_Hist_GeneM[i]->SetBinError(nbin+1, error);
    }
  }

  //A0-A2
  Vec_Hist_Gene[n_Ai]     = new TH1D("Hist_Gene_FA02",    "Hist_Gene_FA02",    nbins_fit_Z_pt, bins_fit_Z_pt);
  Vec_Hist_GeneM[n_Ai]    = new TH1D("Hist_GeneM_FA02",   "Hist_GeneM_FA02",   nbins_fit_Z_pt, bins_fit_Z_pt);
  Vec_Hist_Measured[n_Ai] = new TH1D("Hist_Measured_A02", "Hist_Measured_A02", nbins_fit_Z_pt, bins_fit_Z_pt);
 
  Make_Sub(Vec_Hist_Gene[0],     Vec_Hist_Gene[2],     Vec_Hist_Gene[n_Ai]);
  Make_Sub(Vec_Hist_GeneM[0],    Vec_Hist_GeneM[2],    Vec_Hist_GeneM[n_Ai]);
  Make_Sub(Vec_Hist_Measured[0], Vec_Hist_Measured[2], Vec_Hist_Measured[n_Ai]);

  //A4
  //A4Scale(Vec_Hist_Gene[n_Ai-1],Vec_Hist_Gene[n_Ai-1]);
  //A4Scale(Vec_Hist_GeneM[n_Ai-1],Vec_Hist_GeneM[n_Ai-1]);
  //A4Scale(Vec_Hist_Measured[n_Ai-1],Vec_Hist_Measured[n_Ai-1]);

  double pars_a0[6] = {0.2,  0.68, 0.5,  0.88, -0.1, 1.0};
  double pars_a1[6] = {0.2,  0.66, 0.5,  0.86, -0.1, 1.0};
  double pars_a2[6] = {0.25, 0.68, 0.55, 0.88, -0.1, 1.0};
  double pars_a3[6] = {0.2,  0.66, 0.5,  0.86, -0.1, 0.25};
  double pars_a4[6] = {0.2,  0.68, 0.5,  0.88, -0.1, 0.5};
  //double pars_a4[6] = {0.2,0.2,0.5,0.4,0.85,1.1};
  double pars_a02[6] = {0.7,0.7,0.9,0.9,-0.1,0.4};
  Vec_Hist_GeneM[0]->SetMarkerStyle(25);
  Vec_Hist_GeneM[1]->SetMarkerStyle(25);
  Vec_Hist_GeneM[2]->SetMarkerStyle(25);
  Vec_Hist_GeneM[3]->SetMarkerStyle(25);
  Vec_Hist_GeneM[4]->SetMarkerStyle(25);
  Vec_Hist_GeneM[5]->SetMarkerStyle(25);
  Vec_Hist_Measured[0]->SetMarkerStyle(24);
  Vec_Hist_Measured[1]->SetMarkerStyle(24);
  Vec_Hist_Measured[2]->SetMarkerStyle(24);
  Vec_Hist_Measured[3]->SetMarkerStyle(24);
  Vec_Hist_Measured[4]->SetMarkerStyle(24);
  Vec_Hist_Measured[5]->SetMarkerStyle(24);
  Hist_Three(Vec_Hist_Gene[0], Vec_Hist_GeneM[0], Vec_Hist_Measured[0], pars_a0,  "p_{T} (Z)", "A_{0}",
      "Default", Form("Modified(A_{i}#times %.1f)", factor), "Measured", Form("./plots/Com_A0_step_%d_%s", _Step, testtype), 0);
  Hist_Three(Vec_Hist_Gene[1], Vec_Hist_GeneM[1], Vec_Hist_Measured[1], pars_a1,  "p_{T} (Z)", "A_{1}",
      "Default", Form("Modified(A_{i}#times %.1f)", factor), "Measured", Form("./plots/Com_A1_step_%d_%s", _Step, testtype), 0);
  Hist_Three(Vec_Hist_Gene[2], Vec_Hist_GeneM[2], Vec_Hist_Measured[2], pars_a2,  "p_{T} (Z)", "A_{2}",
      "Default", Form("Modified(A_{i}#times %.1f)", factor), "Measured", Form("./plots/Com_A2_step_%d_%s", _Step, testtype), 0);
  Hist_Three(Vec_Hist_Gene[3], Vec_Hist_GeneM[3], Vec_Hist_Measured[3], pars_a3,  "p_{T} (Z)", "A_{3}",
      "Default", Form("Modified(A_{i}#times %.1f)", factor), "Measured", Form("./plots/Com_A3_step_%d_%s", _Step, testtype), 0);
  Hist_Three(Vec_Hist_Gene[4], Vec_Hist_GeneM[4], Vec_Hist_Measured[4], pars_a4,  "p_{T} (Z)", "A_{4}",
      "Default", Form("Modified(A_{i}#times %.1f)", factor), "Measured", Form("./plots/Com_A4_step_%d_%s", _Step, testtype), 0);
  Hist_Three(Vec_Hist_Gene[5], Vec_Hist_GeneM[5], Vec_Hist_Measured[5], pars_a02, "p_{T} (Z)", "A_{0}-A_{2}",
      "Default", Form("Modified(A_{i}#times %.1f)", factor), "Measured", Form("./plots/Com_A02_step_%d_%s", _Step, testtype), 0);
  cout<<endl<<endl;
}


//
// main function
//
void Ai_Extraction_Iteration(string _Year, int _Step, int study_type, int default_Ai){
  gROOT->ProcessLine(".x /home/mlxu/work/angular_v2/Function/lhcbStyle.C");

  // calcualte the acceptance at first 
  Cal_Acc_iteration(_Year, _Step, study_type, default_Ai);

  // extract Ai with current pars in MC
  Extract_Result_PT_iteration(_Year, _Step, study_type, default_Ai);

  // prepare a new root file for next step
  Prepare_GBReweight(_Year, _Step, study_type, default_Ai);

  // make comparison plots for this step
  Compare_Measured_Ai(_Year, _Step, study_type, default_Ai);

  cout<<" ===> Finish Ai_Extraction_Iteration(), for Year = "<<_Year<<" ; _Step = "<<_Step
      <<" ; study_type = "<<study_type<<"  ; default_Ai = "<<default_Ai<<endl<<endl;
}

#include "../Function/const_number.hpp"
#include "../Function/AlgoZ.hpp"
#include <algorithm>
using namespace const_num;
using namespace RooFit;
using namespace algoz;

const int n_Ai = 5;
const double mass_min_closure = 75.; 
const double mass_max_closure = 105.;

//
// get ZPT bin index
//
int Get_ZPT_Bin(double ZPT_TRUE){
  int nbin_pt = 0;

  if(ZPT_TRUE > 0  && ZPT_TRUE < 10)   nbin_pt = 1; 
  if(ZPT_TRUE > 10 && ZPT_TRUE < 20)   nbin_pt = 2; 
  if(ZPT_TRUE > 20 && ZPT_TRUE < 35)   nbin_pt = 3; 
  if(ZPT_TRUE > 35 && ZPT_TRUE < 55)   nbin_pt = 4; 
  if(ZPT_TRUE > 55 && ZPT_TRUE < 80)   nbin_pt = 5; 
  if(ZPT_TRUE > 80 && ZPT_TRUE < 100)  nbin_pt = 6; 
  
  return nbin_pt;
}

//
// get a weight for a given set of Ai
//
double Get_Ai_Wt(double CS_TRUE, double Phi_TRUE, double A0, double A1, double A2, double A3, double A4){
  double Ai_wt  = 1. + CS_TRUE*CS_TRUE
    + 0.5*A0*(1.-3.*CS_TRUE*CS_TRUE)
    + A1*2.*CS_TRUE*(sqrt(1.-CS_TRUE*CS_TRUE))*cos(Phi_TRUE)
    + 0.5*A2*(1.-CS_TRUE*CS_TRUE)*cos(2.*Phi_TRUE)
    + A3*(sqrt(1.-CS_TRUE*CS_TRUE))*cos(Phi_TRUE)
    + A4*CS_TRUE;

  return Ai_wt; 
}

//
// calcualte Ai from a 2D histogram
//
void Calculate_Ai(TH2D* hist, TH1D* hist_1D){
  for(int i = 0; i < hist->GetNbinsX(); i++){
    char name[200];
    sprintf(name, "hist_projection_x%d", i+1);
    TH1D* tmp_hist = hist->ProjectionY(name, i+1, i+1);

    double mean_value = tmp_hist->GetMean();
    double mean_error = tmp_hist->GetMeanError();
    
    hist_1D->SetBinContent(i+1, mean_value);
    hist_1D->SetBinError(i+1, mean_error);

    delete tmp_hist;
  }
}

//
// study efficiency
//
void Efficiency_1D(TH1D* hist_den, TH1D* hist_num, TH1D* hist){
  for(int i = 0; i < hist_den->GetNbinsX()+1; i++){
    hist->SetBinContent(i, 0.);
    hist->SetBinError(i, 0.);

    double n_deno = hist_den->GetBinContent(i);
    double n_nume = hist_num->GetBinContent(i);

    double ratio = 0.;
    double error = 0.;
    if(n_deno > 0){
      ratio = n_nume/n_deno;
      error = TMath::Sqrt((n_nume+1.0)*fabs(n_deno-n_nume+1.0)/(n_deno+2.0)/(n_deno+2.0)/(n_deno+3.0));
    }
    hist->SetBinContent(i, ratio);
    hist->SetBinError(i, error);
  }
}

//
// get tree name for MC sample
//
string GetTreeName(int type){
  string testtype = "";
  // use Sample-B as MC sample, and Sample-A as pseudo data 
  if(type %2 == 0){
    if(type /100 == 1)      testtype = "AnaTreeA1";
    else if(type /100 == 2) testtype = "AnaTreeA2";
    else if(type /100 == 3) testtype = "AnaTreeA3";
    else if(type /100 == 4) testtype = "AnaTreeA4";
    else{
      cout<<" Wrong input type for GetTreeName(), please check!"<<endl;
      exit(0);
    } 
  }
  else{
    if((type-101) /10 == 1)      testtype = "AnaTreeB1";
    else if((type-101) /10 == 2) testtype = "AnaTreeB2";
    else if((type-101) /10 == 3) testtype = "AnaTreeB3";
    else if((type-101) /10 == 4) testtype = "AnaTreeB4";
    else{
      cout<<" Wrong input type for GetTreeName(), please check!"<<endl;
      exit(0);
    } 
  }

  return testtype;
}

//
// get reweight factor
//
double GetReweightFactor(int type){
  double testtype = -1.;

  int n1 = type/100;
  int n2 = (type - 100*n1)/10;
  int n3 = (type - 100*n1)%10;

  int n_max = max(max(n1, n2), n3);

  if(n_max == 1) testtype = 1.0;
  else if(n_max == 2) testtype = 0.;
  else if(n_max == 3) testtype = 1.5;
  else testtype = 1.0;

  return testtype;
}

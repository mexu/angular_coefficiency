## ********************************************************************** ##
## ********************************************************************** ##
## before any running, please set up env for GB-reweight!!
#source setup.sh

## ********************************************************************** ##
## ********************************************************************** ##
## create output directories 
mkdir -p outputs plots

## number of iterations
nIteration=9

## use default Ai as inputs or use measured Ai as inputs
Use_Default=1

## ********************************************************************** ##
## ********************************************************************** ##
## first step: generator level study 
#root -q -b -l Generator_Study.C

## second step: prepare MC samples
#root -q -b -l Init_Samples.C\(${Use_Default}\) 
#root -q -b -l Make_Gene_Comparison_Ai.C
#
### ********************************************************************** ##
### ********************************************************************** ##
### Types for data and MC samples (Sample-A, Sample-B): 
###      1: default; 
###      2: Ai*0.0; 
###      3: Ai*1.5; 
###      4: PT reweight
### Therefore, we have 8 combinations, calculated as 
###    ( Sample-A*100 + 10*Sample-B + bool(Use-A-as-input) ), 
###  which will be used in this test
###     111: Sample-A (1) used as MC sample, Sample-B (1) as pseudo data 
###     121: Sample-A (1) used as MC sample, Sample-B (2) as pseudo data 
###     410: Sample-A (4) used as pseudo data, Sample-B (1) as MC sample 
###     140: Sample-A (1) used as MC sample, Sample-B (4) as pseudo data 
#
### run the iteration method without any correction on sub-samples
#for _type in 110 111 121 210 131 310 141 410 ; do
#  for istep in $(seq 0 ${nIteration}) ; do
#    ## will not perform GB reweight for the first step
#    if [[ ${istep} -ne 0 ]] ; then
#      python gbreweight.py ${istep} ${_type} ${Use_Default}
#    fi
#
#    ## determine Ai in data
#    root -q -b -l Ai_Extraction_Iteration.C\(\"2016\"\,${istep}\,${_type}\,${Use_Default}\)
#  done
#  root -q -b -l Draw_Iteration.C\(\"2016\"\,${nIteration}\,${_type}\,${Use_Default}\)
#
#done
#### save the fitted results into a new root file
#cp outputs/Result_PT_step_${nIteration}_2016_Type_111_Ai_1.root outputs/Generator_2016_Type_111_measured.root
#cp outputs/Result_PT_step_${nIteration}_2016_Type_110_Ai_1.root outputs/Generator_2016_Type_110_measured.root
#
### ********************************************************************** ##
### ********************************************************************** ##
### the sub-sample is corrected for the input Ai's
###   although we split the MC sample into two independ two sub-samples
###   however, with limited statistics, the default Ai's are not same as the 
###   generator level input Ai's (estimated from 75 M PYTHIA events)
Use_Default=0
root -q -b -l Init_Samples.C\(${Use_Default}\) 

for _type in 110 111 121 210 131 310 141 410 ; do
  for istep in $(seq 0 ${nIteration}) ; do
    ## will not perform GB reweight for the first step
    if [[ ${istep} -ne 0 ]] ; then
      python gbreweight.py ${istep} ${_type} ${Use_Default}
    fi

    ## determine Ai in data
    root -q -b -l Ai_Extraction_Iteration.C\(\"2016\"\,${istep}\,${_type}\,${Use_Default}\)
  done

  root -q -b -l Draw_Iteration.C\(\"2016\"\,${nIteration}\,${_type}\,${Use_Default}\)
done

## ********************************************************************** ##
## ********************************************************************** ##
## make comparison plots
root -q -b -l Uncertainty_From_Iteration.C\(\"2016\"\,${nIteration}\,${Use_Default}\)
root -q -b -l Draw_ZPt.C\(\"2016\"\,141\,${Use_Default}\)
root -q -b -l Draw_ZPt.C\(\"2016\"\,410\,${Use_Default}\)

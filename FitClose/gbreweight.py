import root_numpy,sys
import numpy as np
import pandas
from hep_ml import reweight
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
from matplotlib import rcParams, style
style.use('seaborn-muted')
rcParams.update({'font.size': 12})
rcParams['figure.figsize'] = 18, 6
rcParams['xtick.labelsize'] = 16
rcParams['ytick.labelsize'] = 16

## step information
_step = sys.argv[1]
_type = sys.argv[2]
_ai   = sys.argv[3]
_previous = int(_step) - 1

columns = [
"z_P",
"z_PT"
]

## _type: 1: Ai*0; 0: Ai*1.5 Type_%d_Ai_%d
#if _type == "1" :
mc_file_name = './outputs/MCsample_step_'+str(_previous)+'_2016_Type_'+str(_type)+'_Ai_'+str(_ai)+'.root'
if int(_type) % 2 == 1:
   mc_tree_name   = 'AnaTreeA1'
   data_tree_name = 'AnaTreeB'
else:
   mc_tree_name = 'AnaTreeB1'
   data_tree_name = 'AnaTreeA'

if int(_ai) == 1:
   data_file_name = './outputs/MC_2016.root'
else:
   data_file_name = './outputs/MC_2016_Measured.root'

## get input data type
n1 = int(int(_type) / 100)
n2 = int((int(_type) - n1*100)/10)
n3 = int((int(_type) - n1*100)%10)
n_max = int(max(n1, n2, n3))
data_tree_name += (str) (n_max)

original_all = root_numpy.root2array(mc_file_name,   treename=mc_tree_name,   branches=columns+['ai_weight'], selection="")
target_all   = root_numpy.root2array(data_file_name, treename=data_tree_name, branches=columns+['ai_weight'], selection="")

original_all = pandas.DataFrame(original_all)
target_all   = pandas.DataFrame(target_all)

original      = original_all[columns]
original_sw   = original_all["ai_weight"]
target        = target_all  [columns]
target_sw     = target_all  ["ai_weight"]

reweighter = reweight.GBReweighter(n_estimators=50, learning_rate=0.1, max_depth=3, min_samples_leaf=1000, gb_args={'subsample': 0.6})
reweighter.fit(original, target, original_weight=original_sw, target_weight=target_sw)
gb_weights = reweighter.predict_weights(original, original_sw)

from hep_ml.metrics_utils import ks_2samp_weighted
hist_settings = {'bins': 40, 'density': True, 'alpha': 0.8, 'histtype': 'step', 'lw': 2}

def draw_distributions(original_weights, gb_weights, name, i):
     fig, ax = plt.subplots( nrows=2, ncols=2, figsize = (15,15))
    #fig, ax = plt.subplots( nrows=1, ncols=2, figsize = (15,15))
    #fig, ax = plt.subplots( nrows=2, ncols=2, figsize = (15,15))
     for id, column in enumerate(columns, 0):
        print(id)
        i = int(id / 2)
        j = int(id % 2)
        xlim = np.percentile(np.hstack([target_all[column]]), [0.01, 99.9])
        data = original_all[column]
        ax[i,j].hist(data, weights=original_weights, range=xlim,  label='MC', **hist_settings)
        ax[i,j].hist(target_all[column], weights=target_sw, range=xlim, label='Data Weights', **hist_settings)
        ax[i,j].hist(data, weights=gb_weights, range=xlim,  label='MC weighted', **hist_settings)
        ax[i,j].set_title(column)
        legend = ax[i,j].legend(loc='upper right')
        print ('KS std', column, ' = ', ks_2samp_weighted(data, target_all[column],
                                         weights1=original_weights, weights2=target_sw))
        print ('KS GB ', column, ' = ', ks_2samp_weighted(data, target_all[column],
                                         weights1=gb_weights, weights2=target_sw))
        fig.savefig(name)

# pay attention, actually we have very few data
len(original), len(target)

draw_distributions(original_sw, gb_weights, "./plots/gbreweight_step_"+_step+"_2016_Type_"+str(_type)+"_Ai_"+str(_ai)+".pdf", 3)

# Add weights to tuple
original_all['gb_weights'] = gb_weights
original_all_array = original_all.to_records(False)
weighted_tuple = root_numpy.array2root(original_all_array, "./outputs/gbreweight_step_"+_step+"_2016_Type_"+str(_type)+"_Ai_"+str(_ai)+".root", mode = "recreate", treename = "DecayTree")

{
//=========Macro generated from canvas: c2/c2
//=========  (Mon Feb 17 15:28:11 2020) by ROOT version5.34/30
   TCanvas *c2 = new TCanvas("c2", "c2",0,0,700,500);
   gStyle->SetOptStat(0);
   c2->Range(1.246914,-2.151898,5.197531,73.79747);
   c2->SetFillColor(0);
   c2->SetBorderMode(0);
   c2->SetBorderSize(2);
   c2->SetTickx(1);
   c2->SetTicky(1);
   c2->SetLeftMargin(0.14);
   c2->SetRightMargin(0.05);
   c2->SetTopMargin(0.05);
   c2->SetBottomMargin(0.16);
   c2->SetFrameLineWidth(2);
   c2->SetFrameBorderMode(0);
   c2->SetFrameLineWidth(2);
   c2->SetFrameBorderMode(0);
   Double_t xAxis6[13] = {1.8, 2, 2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4, 4.25, 4.5, 5}; 
   Double_t yAxis6[13] = {10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70}; 
   
   TH2D *Hist_2D_Eff_ALL_ETA_PT_2016 = new TH2D("Hist_2D_Eff_ALL_ETA_PT_2016","Hist_2D_Eff_ALL_ETA_PT_2016",12, xAxis6,12, yAxis6);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(15,0.6649485);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(16,0.7826087);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(17,0.8);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(18,0.8181818);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(19,0.7916667);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(20,0.8787879);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(21,0.8857143);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(22,0.8928571);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(23,0.9468085);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(24,0.9044118);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(25,0.8477157);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(26,0.1773879);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(29,0.8793566);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(30,0.960961);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(31,0.9546279);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(32,0.9436997);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(33,0.928);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(34,0.9396226);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(35,0.9702128);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(36,0.973913);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(37,0.9837838);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(38,0.9740035);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(39,0.8810127);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(40,0.201788);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(43,0.9028614);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(44,0.9834302);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(45,0.9886978);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(46,0.984507);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(47,0.9785638);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(48,0.9754204);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(49,0.9846879);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(50,0.9860558);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(51,0.9825361);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(52,0.9862486);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(53,0.8839669);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(54,0.2417396);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(57,0.9244341);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(58,0.9867146);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(59,0.9912335);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(60,0.9862475);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(61,0.9854015);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(62,0.9816327);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(63,0.989899);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(64,0.9872782);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(65,0.9866731);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(66,0.9873072);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(67,0.8912363);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(68,0.2811913);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(71,0.9263352);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(72,0.9910397);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(73,0.9917386);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(74,0.9889647);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(75,0.989673);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(76,0.9895288);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(77,0.9906068);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(78,0.989066);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(79,0.9881449);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(80,0.987984);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(81,0.9084493);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(82,0.3047118);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(85,0.9283435);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(86,0.9907977);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(87,0.9924006);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(88,0.9894589);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(89,0.9875405);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(90,0.9889593);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(91,0.9909086);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(92,0.9881441);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(93,0.9889266);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(94,0.9867453);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(95,0.904695);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(96,0.3729329);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(99,0.9363417);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(100,0.9917531);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(101,0.993315);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(102,0.9893076);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(103,0.9872949);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(104,0.9880617);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(105,0.9905202);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(106,0.9898434);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(107,0.9879366);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(108,0.9876219);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(109,0.9112932);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(110,0.4466699);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(113,0.9325891);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(114,0.9914073);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(115,0.9922441);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(116,0.9894682);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(117,0.988356);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(118,0.9881735);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(119,0.9899199);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(120,0.9886407);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(121,0.9864462);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(122,0.9873401);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(123,0.9179747);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(124,0.4782067);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(127,0.9304813);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(128,0.99094);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(129,0.9919885);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(130,0.9918228);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(131,0.9889233);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(132,0.9880107);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(133,0.9911978);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(134,0.990219);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(135,0.9877037);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(136,0.9895698);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(137,0.9148398);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(138,0.5170843);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(141,0.9358025);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(142,0.9891022);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(143,0.9942927);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(144,0.9935065);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(145,0.9903475);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(146,0.9898089);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(147,0.9885216);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(148,0.9832176);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(149,0.9871465);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(150,0.9851852);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(151,0.8885714);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(152,0.453125);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(155,0.9246753);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(156,0.9945115);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(157,0.9921569);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(158,0.9908925);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(159,0.9856373);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(160,0.9915174);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(161,0.9895942);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(162,0.9903714);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(163,0.9980354);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(164,0.9930796);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(165,0.8983051);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(166,0.5142857);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(169,0.9450549);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(170,0.9935345);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(171,0.9960861);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(172,0.9944238);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(173,0.9945355);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(174,0.9847328);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(175,0.9933921);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(176,0.9937695);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(177,0.9911504);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(178,1);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(179,0.9183673);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinContent(180,0.6);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(15,0.03367088);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(16,0.02713054);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(17,0.03800158);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(18,0.04733905);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(19,0.05800608);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(20,0.05832118);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(21,0.05545829);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(22,0.04241116);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(23,0.02457762);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(24,0.02560875);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(25,0.02564292);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(26,0.01686289);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(29,0.0119443);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(30,0.006187622);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(31,0.008993153);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(32,0.01211919);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(33,0.01659946);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(34,0.01491474);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(35,0.01170647);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(36,0.01120661);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(37,0.007035551);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(38,0.006806465);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(39,0.01153762);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(40,0.008279268);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(43,0.005751099);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(44,0.002193561);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(45,0.002390201);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(46,0.003344034);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(47,0.004843209);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(48,0.005691882);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(49,0.004357901);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(50,0.003819177);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(51,0.003678822);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(52,0.002780811);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(53,0.00582563);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(54,0.005086954);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(57,0.003530719);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(58,0.001199284);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(59,0.001165615);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(60,0.001839939);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(61,0.002207184);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(62,0.002739413);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(63,0.002047301);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(64,0.002075304);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(65,0.001799829);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(66,0.001427226);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(67,0.003556241);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(68,0.004357258);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(71,0.002895826);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(72,0.000782161);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(73,0.0007806894);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(74,0.001014245);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(75,0.001127175);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(76,0.001291213);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(77,0.001186638);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(78,0.001127136);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(79,0.001057954);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(80,0.001031468);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(81,0.002896537);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(82,0.004540999);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(85,0.002537286);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(86,0.000677005);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(87,0.0006180192);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(88,0.0007394614);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(89,0.0008234723);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(90,0.0007984996);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(91,0.0007131063);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(92,0.0008249896);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(93,0.0008394774);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(94,0.001003692);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(95,0.002941744);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(96,0.005561116);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(99,0.002226558);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(100,0.0005693798);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(101,0.0004884688);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(102,0.0005916885);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(103,0.0006256965);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(104,0.0006047297);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(105,0.000561771);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(106,0.0006390067);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(107,0.0007939536);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(108,0.0009603327);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(109,0.003122391);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(110,0.007720064);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(113,0.00300152);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(114,0.0007178333);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(115,0.0006265311);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(116,0.0007014294);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(117,0.0007291115);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(118,0.0007556137);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(119,0.0007392283);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(120,0.0008487774);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(121,0.001070658);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(122,0.001290051);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(123,0.004369534);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(124,0.01245317);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(127,0.005379077);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(128,0.001314511);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(129,0.001139137);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(130,0.001107543);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(131,0.001279803);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(132,0.001375309);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(133,0.001238451);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(134,0.00144965);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(135,0.001883529);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(136,0.002158796);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(137,0.008124706);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(138,0.02376881);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(141,0.008662785);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(142,0.002416995);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(143,0.001572443);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(144,0.001613448);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(145,0.001956946);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(146,0.002109613);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(147,0.002324477);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(148,0.003137549);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(149,0.003396778);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(150,0.004401936);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(151,0.01689185);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(152,0.04349869);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(155,0.01357588);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(156,0.002672609);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(157,0.002921038);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(158,0.002998637);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(159,0.00366465);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(160,0.002959444);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(161,0.003422552);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(162,0.003855881);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(163,0.002759421);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(164,0.005911156);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(165,0.02825463);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(166,0.08108108);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(169,0.01743074);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(170,0.004268808);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(171,0.003363154);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(172,0.003686549);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(173,0.003613288);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(174,0.005649051);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(175,0.004361906);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(176,0.005329182);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(177,0.007530075);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(178,0.007873768);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(179,0.04123748);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetBinError(180,0.1367354);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetEntries(288);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetStats(0);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContour(20);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(0,0.1773879);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(1,0.2185185);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(2,0.2596491);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(3,0.3007797);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(4,0.3419103);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(5,0.3830409);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(6,0.4241715);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(7,0.4653021);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(8,0.5064327);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(9,0.5475634);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(10,0.588694);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(11,0.6298246);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(12,0.6709552);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(13,0.7120858);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(14,0.7532164);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(15,0.794347);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(16,0.8354776);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(17,0.8766082);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(18,0.9177388);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetContourLevel(19,0.9588694);
   
   TPaletteAxis *palette = new TPaletteAxis(5.019753,10,5.197531,70,Hist_2D_Eff_ALL_ETA_PT_2016);
palette->SetLabelColor(1);
palette->SetLabelFont(132);
palette->SetLabelOffset(0.005);
palette->SetLabelSize(0.04);
palette->SetTitleOffset(1.2);
palette->SetTitleSize(0.048);
   palette->SetFillColor(100);
   palette->SetFillStyle(1001);
   palette->SetLineWidth(2);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetListOfFunctions()->Add(palette,"br");
   Hist_2D_Eff_ALL_ETA_PT_2016->SetLineWidth(2);
   Hist_2D_Eff_ALL_ETA_PT_2016->SetMarkerStyle(20);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetXaxis()->SetNdivisions(505);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetXaxis()->SetLabelFont(132);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetXaxis()->SetLabelOffset(0.01);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetXaxis()->SetTitleSize(0.048);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetXaxis()->SetTitleOffset(0.95);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetXaxis()->SetTitleFont(132);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetYaxis()->SetLabelFont(132);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetYaxis()->SetLabelOffset(0.01);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetYaxis()->SetTitleSize(0.048);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetYaxis()->SetTitleOffset(0.95);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetYaxis()->SetTitleFont(132);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetZaxis()->SetLabelFont(132);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetZaxis()->SetTitleSize(0.048);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetZaxis()->SetTitleOffset(1.2);
   Hist_2D_Eff_ALL_ETA_PT_2016->GetZaxis()->SetTitleFont(132);
   Hist_2D_Eff_ALL_ETA_PT_2016->Draw("COLZ");
   
   TPaveText *pt = new TPaveText(0.12,0.95,0.95,1,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(1);
   pt->SetLineWidth(2);
   TText *text = pt->AddText("Hist_2D_Eff_ALL_ETA_PT_2016");
   pt->Draw();
   c2->Modified();
   c2->cd();
   c2->SetSelected(c2);
}

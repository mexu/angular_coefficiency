import ROOT
from ROOT import TLorentzVector
from ROOT import TVector3
from ROOT import TFile,TTree
import math
from array import array

def Collins_Soper(mum, mup):
  dimass12 = (mum + mup).M()
  diPx = (mum.Px() + mup.Px())
  diPy = (mum.Py() + mup.Py())
  dimassQT = math.sqrt( diPx*diPx + diPy*diPy )
  P1plus = ( mum.E() + mum.Pz() )
  P1mins = ( mum.E() - mum.Pz() )
  P2plus = ( mup.E() + mup.Pz() )
  P2mins = ( mup.E() - mup.Pz() )
  collins = ( P1plus*P2mins - P1mins*P2plus ) /( dimass12 * math.sqrt(dimass12*dimass12 + dimassQT*dimassQT) )
  return collins

def Collins_Soper_Phi(mum, mup):
  
  boostedMu = mum 
  Z = (mum+mup);
  boostedMu.Boost(-Z.BoostVector());
  ProtonMass = 938.272; # MeV
  BeamEnergy = 6500000; # MeV
  sign  = math.fabs(Z.Z())/Z.Z();
  p1 ,p2 = TLorentzVector(), TLorentzVector()
  p1.SetPxPyPzE(0, 0, sign*BeamEnergy,math.sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)) #quark
  p2.SetPxPyPzE(0, 0, -1*sign*BeamEnergy,math.sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)) # antiquark
  p1.Boost(-Z.BoostVector());
  p2.Boost(-Z.BoostVector());
  CSAxis, xAxis, yAxis = TVector3(),TVector3(),TVector3()
  CSAxis = (p1.Vect().Unit()-p2.Vect().Unit()).Unit();
  yAxis = (p1.Vect().Unit()).Cross((p2.Vect().Unit()));
  yAxis = yAxis.Unit();
  xAxis = yAxis.Cross(CSAxis);
  xAxis = xAxis.Unit();
  phi = math.atan2((boostedMu.Vect()*yAxis),(boostedMu.Vect()*xAxis));
  return phi

def copygausstree(fin,fout):

  fin = ROOT.TFile(fin)
  tree= fin.Get("MCDecayTreeTuple/MCDecayTree")
  f=ROOT.TFile(fout,"recreate")
  pid  = "&&abs(muplus_MC_MOTHER_ID)==23&&abs(muminus_MC_MOTHER_ID)==23"
  tall=tree.CopyTree("1"+pid)

  CS = array('d',[0])
  Phi = array('d',[0])
  Mup_ETA = array('d',[0])
  Mum_ETA = array('d',[0])
  ZBoson_PT1 = array('d',[0])
  ZBoson_PT2 = array('d',[0])
  ZBoson_M = array('d',[0])
  ZBoson_ETA = array('d',[0])
  Mup_PT = array('d',[0])
  Mum_PT = array('d',[0])
  swits = array('d',[0])

  br_CS = tall.Branch('CS',CS,'CS/D')
  br_Phi = tall.Branch('Phi',Phi,'Phi/D')
  br_Mup_ETA = tall.Branch('Mup_ETA',Mup_ETA,'Mup_ETA/D')
  br_Mum_ETA = tall.Branch('Mum_ETA',Mum_ETA,'Mum_ETA/D')
  br_ZBoson_PT1 = tall.Branch('ZBoson_PT1',ZBoson_PT1,'ZBoson_PT1/D')
  br_ZBoson_PT2 = tall.Branch('ZBoson_PT2',ZBoson_PT2,'ZBoson_PT2/D')
  br_ZBoson_M = tall.Branch('ZBoson_M',ZBoson_M,'ZBoson_M/D')
  br_ZBoson_ETA = tall.Branch('ZBoson_ETA',ZBoson_ETA,'ZBoson_ETA/D')
  br_Mup_PT = tall.Branch('Mup_PT',Mup_PT,'Mup_PT/D')
  br_Mum_PT = tall.Branch('Mum_PT',Mum_PT,'Mum_PT/D')
  br_swits = tall.Branch('swits',swits,'swits/D')

  for event in tall:
    vec_muplus = TLorentzVector()
    vec_muminus = TLorentzVector()
    vec_muplus.SetPxPyPzE(event.muplus_TRUEP_X,event.muplus_TRUEP_Y,event.muplus_TRUEP_Z,event.muplus_TRUEP_E)
    vec_muminus.SetPxPyPzE(event.muminus_TRUEP_X,event.muminus_TRUEP_Y,event.muminus_TRUEP_Z,event.muminus_TRUEP_E)
    ZBoson_PT1[0]  = (vec_muplus+vec_muminus).Pt()/1000.
    ZBoson_PT2[0] = event.Z0_TRUEPT/1000.
    Mup_PT[0] = event.muplus_TRUEPT/1000.
    Mum_PT[0] = event.muminus_TRUEPT/1000.
    Mup_ETA[0] = vec_muplus.Eta()
    Mum_ETA[0] = vec_muminus.Eta()
    ZBoson_ETA[0] = (vec_muplus+vec_muminus).Rapidity();
    ZBoson_M[0] = (vec_muplus+vec_muminus).M()/1000.
    swits[0] = 1.0
    CS[0] = Collins_Soper(vec_muminus, vec_muplus)
    Phi[0] = Collins_Soper_Phi(vec_muminus,vec_muplus)

    br_CS.Fill()
    br_Phi.Fill()
    br_ZBoson_PT1.Fill()
    br_ZBoson_PT2.Fill()
    br_ZBoson_M.Fill()
    br_ZBoson_ETA.Fill()
    br_Mup_PT.Fill()
    br_Mum_PT.Fill()
    br_Mup_ETA.Fill()
    br_Mum_ETA.Fill()
    br_swits.Fill()

  tall.SetName("AnaTree")
  f.Write()
  f.Close()

copygausstree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Gauss/Z2MuMu.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Gauss/Z2MuMu_Pre.root")

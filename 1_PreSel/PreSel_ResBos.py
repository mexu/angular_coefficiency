import ROOT
from ROOT import TLorentzVector
from ROOT import TVector3
from ROOT import TFile,TTree
import math
from array import array

def Sel_Acc(fin,fout):

  fin  = ROOT.TFile(fin)
  tree = fin.Get("AnaTree")
  f=ROOT.TFile(fout,"recreate")

  eta_lhcb  =  "&&Mup_ETA>1.8&&Mup_ETA<5.0&&Mum_ETA>1.8&&Mum_ETA<5.0"
  pt_lhcb   = "&&Mup_PT>10&&Mum_PT>10"
  tall_lhcb = tree.CopyTree("1"+eta_lhcb+pt_lhcb)
  tall_lhcb.SetName("AnaTreeLHCbAcc")

  eta_atlas  = "&&Mup_ETA>-2.4&&Mup_ETA<2.4&&Mum_ETA>-2.4&&Mum_ETA<2.4"
  pt_atlas   = "&&Mup_PT>20&&Mum_PT>20"
  tall_atlas = tree.CopyTree("1"+eta_atlas+pt_atlas)
  tall_atlas.SetName("AnaTreeATLAsAcc")

  f.Write()
  f.Close()

#Sel_Acc("/projects/lhcb/users/mexu/Z2MuMu/ResBos/resbos_cp_11222016/tupleMaker/tupleReader/outputs/ResBos/Tuple_ZmumuAFB_NoFSR_Full.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/ResBos/ResBos_NoFSR_Acc.root")
Sel_Acc("/projects/lhcb/users/mexu/Z2MuMu/ResBos/resbos_cp_11222016/tupleMaker/tupleReader/outputs/ResBos/Tuple_ZmumuAFB_FSR_Full.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/ResBos/ResBos_FSR_Acc.root")

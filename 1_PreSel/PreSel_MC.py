import ROOT
from ROOT import TLorentzVector
from ROOT import TVector3
from ROOT import TFile,TTree
import math
from array import array

def Collins_Soper(mum, mup):
  dimass12 = (mum + mup).M()
  diPx = (mum.Px() + mup.Px())
  diPy = (mum.Py() + mup.Py())
  dimassQT = math.sqrt( diPx*diPx + diPy*diPy )
  P1plus = ( mum.E() + mum.Pz() )
  P1mins = ( mum.E() - mum.Pz() )
  P2plus = ( mup.E() + mup.Pz() )
  P2mins = ( mup.E() - mup.Pz() )
  collins = ( P1plus*P2mins - P1mins*P2plus ) /( dimass12 * math.sqrt(dimass12*dimass12 + dimassQT*dimassQT) )
  return collins

def Collins_Soper_Phi(mum, mup):
  
  boostedMu = mum 
  Z = (mum+mup);
  boostedMu.Boost(-Z.BoostVector());
  ProtonMass = 938.272; # MeV
  BeamEnergy = 6500000; # MeV
  sign  = math.fabs(Z.Z())/Z.Z();
  p1 ,p2 = TLorentzVector(), TLorentzVector()
  p1.SetPxPyPzE(0, 0, sign*BeamEnergy,math.sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)) #quark
  p2.SetPxPyPzE(0, 0, -1*sign*BeamEnergy,math.sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)) # antiquark
  p1.Boost(-Z.BoostVector());
  p2.Boost(-Z.BoostVector());
  CSAxis, xAxis, yAxis = TVector3(),TVector3(),TVector3()
  CSAxis = (p1.Vect().Unit()-p2.Vect().Unit()).Unit();
  yAxis = (p1.Vect().Unit()).Cross((p2.Vect().Unit()));
  yAxis = yAxis.Unit();
  xAxis = yAxis.Cross(CSAxis);
  xAxis = xAxis.Unit();
  phi = math.atan2((boostedMu.Vect()*yAxis),(boostedMu.Vect()*xAxis));
  return phi


def copymcttree(fin,fout):

  fin  = ROOT.TFile(fin)
  tree = fin.Get("MCTZ/MCDecayTree")
  f=ROOT.TFile(fout,"recreate")

  pid  = "&&(abs(mup_TRUEID)==13)&&(abs(mum_TRUEID)==13)&&(abs(mum_MC_MOTHER_ID)==23)&&(abs(mup_MC_MOTHER_ID)==23)"
  tall =tree.CopyTree("1"+pid)

  CS  = array('d',[0])
  Phi = array('d',[0])
  ZBoson_PT = array('d',[0])
  Mup_PT = array('d',[0])
  Mum_PT = array('d',[0])
  swits = array('d',[0])
  Z_Mass = array('d',[0])
  Mup_ETA = array('d',[0])
  Mum_ETA = array('d',[0])

  br_CS = tall.Branch('CS',CS,'CS/D')
  br_Phi = tall.Branch('Phi',Phi,'Phi/D')
  br_Mup_ETA = tall.Branch('Mup_ETA',Mup_ETA,'Mup_ETA/D')
  br_Mum_ETA = tall.Branch('Mum_ETA',Mum_ETA,'Mum_ETA/D')
  br_ZBoson_PT = tall.Branch('ZBoson_PT',ZBoson_PT,'ZBoson_PT/D')
  br_Mup_PT = tall.Branch('Mup_PT',Mup_PT,'Mup_PT/D')
  br_Mum_PT = tall.Branch('Mum_PT',Mum_PT,'Mum_PT/D')
  br_swits = tall.Branch('swits',swits,'swits/D')
  br_Z_Mass = tall.Branch('Z_Mass',Z_Mass,'Z_Mass/D')

  for event in tall:

    vec_mup = TLorentzVector()
    vec_mum = TLorentzVector()
    vec_mup.SetPxPyPzE(event.mup_TRUEP_X,event.mup_TRUEP_Y,event.mup_TRUEP_Z,event.mup_TRUEP_E)
    vec_mum.SetPxPyPzE(event.mum_TRUEP_X,event.mum_TRUEP_Y,event.mum_TRUEP_Z,event.mum_TRUEP_E)
    ZBoson_PT[0] = event.Z_TRUEPT/1000.
    Mup_PT[0] = event.mup_TRUEPT/1000.
    Mum_PT[0] = event.mum_TRUEPT/1000.
    Mup_ETA[0] = vec_mup.Eta()
    Mum_ETA[0] = vec_mum.Eta()
    swits[0] = 1.0
    Z_Mass[0] = (vec_mup+vec_mum).M()
    CS[0] = Collins_Soper(vec_mum, vec_mup)
    Phi[0] = Collins_Soper_Phi(vec_mum,vec_mup)

    br_CS.Fill()
    br_Phi.Fill()
    br_ZBoson_PT.Fill()
    br_Mup_PT.Fill()
    br_Mum_PT.Fill()
    br_Mup_ETA.Fill()
    br_Mum_ETA.Fill()
    br_swits.Fill()
    br_Z_Mass.Fill()



  tall.SetName("AnaTree")
  f.Write()
  f.Close()



def copymctree(fin,fout,bkg):

  fin = ROOT.TFile(fin)
  tree= fin.Get("Z2MUMU/DecayTree")
  f=ROOT.TFile(fout,"recreate")

  etas     = "&&mup_ETA>2.0&&mup_ETA<4.5"
  etas    += "&&mum_ETA>2.0&&mum_ETA<4.5"
  trk      = "&&sqrt(mup_PERR2)/mup_P<0.1&&sqrt(mum_PERR2)/mum_P<0.1"
  nmuon    = "&&mup_MuonNShared==0 && mum_MuonNShared==0"
  pid      = "&&mup_isMuon ==1 && mum_isMuon ==1";
  pt       = "&&mup_PT>10000";
  pt      += "&&mum_PT>10000";
  iso      = "&&(mup_PT*1.00/(mup_0.50_cc_vPT+mup_PT))>0.85&&(mum_PT*1.00/(mum_0.50_cc_vPT+mum_PT))>0.85"
  ver_cuts = "&&(ZBoson_ENDVERTEX_CHI2/ZBoson_ENDVERTEX_NDOF)< 9" 

  if(bkg):
    pid  += "&&(abs(mup_TRUEID)==13)&&(abs(mum_TRUEID)==13)"
  else:
    pid  += "&&(abs(mup_TRUEID)==13)&&(abs(mum_TRUEID)==13)&&abs(mum_MC_MOTHER_ID)==23&&abs(mup_MC_MOTHER_ID)==23"

  pt    = "&&mup_PT>10000";
  pt   += "&&mum_PT>10000";

  trg   = "&&((mup_L0MuonEWDecision_TOS&&mup_Hlt1SingleMuonHighPTDecision_TOS&&mup_Hlt2SingleMuonHighPTDecision_TOS)||(mum_L0MuonEWDecision_TOS&&mum_Hlt1SingleMuonHighPTDecision_TOS&&mum_Hlt2SingleMuonHighPTDecision_TOS))"

  print "Ver MC Eff", tree.GetEntries("1"+ver_cuts)*100.0/tree.GetEntries("1"), "%"
  print "Eta MC Eff", tree.GetEntries("1"+etas)*100.0/tree.GetEntries("1"), "%"
  print "Pt  MC Eff",  tree.GetEntries("1"+pt)*100.0/tree.GetEntries("1"), "%"
  print "TRK MC Eff", tree.GetEntries("1"+trk)*100.0/tree.GetEntries("1"), "%"
  print "Shared MC Eff", tree.GetEntries("1"+nmuon)*100.0/tree.GetEntries("1"), "%"
  print "Iso MC Eff", tree.GetEntries("1"+iso)*100.0/tree.GetEntries("1"), "%"
  print "Pid MC Eff", tree.GetEntries("1"+pid)*100.0/tree.GetEntries("1"), "%"
  
  tpart=tree.CopyTree("1"+etas+trk+nmuon+pid+pt+trg)
  tall=tree.CopyTree("1"+etas+trk+nmuon+pid+pt+trg+iso+ver_cuts)

  CS  = array('d',[0])
  Phi = array('d',[0])
  Mup_PT = array('d',[0])
  Mum_PT = array('d',[0])
  swits = array('d',[0])
  Mup_ETA = array('d',[0])
  Mum_ETA = array('d',[0])

  br_CS = tall.Branch('CS',CS,'CS/D')
  br_Phi = tall.Branch('Phi',Phi,'Phi/D')
  br_Mup_ETA = tall.Branch('Mup_ETA',Mup_ETA,'Mup_ETA/D')
  br_Mum_ETA = tall.Branch('Mum_ETA',Mum_ETA,'Mum_ETA/D')
  br_Mup_PT = tall.Branch('Mup_PT',Mup_PT,'Mup_PT/D')
  br_Mum_PT = tall.Branch('Mum_PT',Mum_PT,'Mum_PT/D')
  br_swits = tall.Branch('swits',swits,'swits/D')

  for event in tall:

    vec_mup = TLorentzVector()
    vec_mum = TLorentzVector()
    vec_mup.SetPxPyPzE(event.mup_PX,event.mup_PY,event.mup_PZ,event.mup_PE)
    vec_mum.SetPxPyPzE(event.mum_PX,event.mum_PY,event.mum_PZ,event.mum_PE)
    Mup_PT[0] = event.mup_PT/1000.
    Mum_PT[0] = event.mum_PT/1000.
    Mup_ETA[0] = vec_mup.Eta()
    Mum_ETA[0] = vec_mum.Eta()
    swits[0] = 1.0
    CS[0] = Collins_Soper(vec_mum, vec_mup)
    Phi[0] = Collins_Soper_Phi(vec_mum,vec_mup)

    br_CS.Fill()
    br_Phi.Fill()
    br_Mup_PT.Fill()
    br_Mum_PT.Fill()
    br_Mup_ETA.Fill()
    br_Mum_ETA.Fill()
    br_swits.Fill()

  tpart.SetName("RSTree_Pass")
  tall.SetName("AnaTree")

  f.Write()
  f.Close()


copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/MC_2016_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/MC_2016_U_Pre.root",False)
copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/MC_2016_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/MC_2016_D_Pre.root",False)

copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/Signle_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/Signle_U_Pre.root",True)
copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/Signle_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/Signle_D_Pre.root",True)

copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/TTGG_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/TTGG_U_Pre.root",True)
copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/TTGG_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/TTGG_D_Pre.root",True)

copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/TTQQ_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/TTQQ_U_Pre.root",True)
copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/TTQQ_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/TTQQ_D_Pre.root",True)

copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/WW_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/WW_U_Pre.root",True)
copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/WW_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/WW_D_Pre.root",True)

copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/Z2TauTau_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/Z2TauTau_U_Pre.root",True)
copymctree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/Z2TauTau_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/Z2TauTau_D_Pre.root",True)

copymcttree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/MC_2016_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/MCT_2016_U_Pre.root")
copymcttree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/MC_2016_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/MCT_2016_D_Pre.root")

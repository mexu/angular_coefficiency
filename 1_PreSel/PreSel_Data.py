import ROOT
from ROOT import TLorentzVector
from ROOT import TVector3
from ROOT import TFile,TTree
import math
from array import array

def Collins_Soper(mum, mup):
  dimass12 = (mum + mup).M()
  diPx = (mum.Px() + mup.Px())
  diPy = (mum.Py() + mup.Py())
  dimassQT = math.sqrt( diPx*diPx + diPy*diPy )
  P1plus = ( mum.E() + mum.Pz() )
  P1mins = ( mum.E() - mum.Pz() )
  P2plus = ( mup.E() + mup.Pz() )
  P2mins = ( mup.E() - mup.Pz() )
  collins = ( P1plus*P2mins - P1mins*P2plus ) /( dimass12 * math.sqrt(dimass12*dimass12 + dimassQT*dimassQT) )
  return collins

def Collins_Soper_Phi(mum, mup):
  
  boostedMu = mum 
  Z = (mum+mup);
  boostedMu.Boost(-Z.BoostVector());
  ProtonMass = 938.272; # MeV
  BeamEnergy = 6500000; # MeV
  sign  = math.fabs(Z.Z())/Z.Z();
  p1 ,p2 = TLorentzVector(), TLorentzVector()
  p1.SetPxPyPzE(0, 0, sign*BeamEnergy,math.sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)) #quark
  p2.SetPxPyPzE(0, 0, -1*sign*BeamEnergy,math.sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass)) # antiquark
  p1.Boost(-Z.BoostVector());
  p2.Boost(-Z.BoostVector());
  CSAxis, xAxis, yAxis = TVector3(),TVector3(),TVector3()
  CSAxis = (p1.Vect().Unit()-p2.Vect().Unit()).Unit();
  yAxis = (p1.Vect().Unit()).Cross((p2.Vect().Unit()));
  yAxis = yAxis.Unit();
  xAxis = yAxis.Cross(CSAxis);
  xAxis = xAxis.Unit();
  phi = math.atan2((boostedMu.Vect()*yAxis),(boostedMu.Vect()*xAxis));
  return phi


def copytree(fin,fout):
 
  fin = ROOT.TFile(fin)
  tree_rs = fin.Get("Z2MUMU/DecayTree")
  tree_ws = fin.Get("WS/DecayTree")
   
  f=TFile(fout,"recreate")

  rs_etas  = "&&mup_ETA>2.0&&mup_ETA<4.5"
  rs_etas += "&&mum_ETA>2.0&&mum_ETA<4.5"
  rs_trk   = "&&sqrt(mup_PERR2)/mup_P<0.1&&sqrt(mum_PERR2)/mum_P<0.1"
  rs_nmuon = "&&mup_MuonNShared==0 && mum_MuonNShared==0"
  rs_pid   = "&&mup_isMuon ==1 && mum_isMuon ==1";
  rs_pt    = "&&mup_PT>10000";
  rs_pt   += "&&mum_PT>10000";
  rs_trg   = "&&((mup_L0MuonEWDecision_TOS&&mup_Hlt1SingleMuonHighPTDecision_TOS&&mup_Hlt2SingleMuonHighPTDecision_TOS)||(mum_L0MuonEWDecision_TOS&&mum_Hlt1SingleMuonHighPTDecision_TOS&&mum_Hlt2SingleMuonHighPTDecision_TOS))"
  rs_iso   = "&&(mup_PT*1.00/(mup_0.50_cc_vPT+mup_PT))>0.85&&(mum_PT*1.00/(mum_0.50_cc_vPT+mum_PT))>0.85"

  ver_cuts = "&&(ZBoson_ENDVERTEX_CHI2/ZBoson_ENDVERTEX_NDOF)< 9" 
 
  ws_etas  = "&&mu1_ETA>2.0&&mu1_ETA<4.5"
  ws_etas += "&&mu2_ETA>2.0&&mu2_ETA<4.5"
  ws_trk   = "&&sqrt(mu1_PERR2)/mu1_P<0.1&&sqrt(mu2_PERR2)/mu2_P<0.1"
  ws_nmuon = "&&mu1_MuonNShared==0 && mu2_MuonNShared==0"
  ws_pid   = "&&mu1_isMuon ==1 && mu2_isMuon ==1";
  ws_pt    = "&&mu1_PT>10000";
  ws_pt   += "&&mu2_PT>10000";
  ws_trg   = "&&((mu1_L0MuonEWDecision_TOS&&mu1_Hlt1SingleMuonHighPTDecision_TOS&&mu1_Hlt2SingleMuonHighPTDecision_TOS)||(mu2_L0MuonEWDecision_TOS&&mu2_Hlt1SingleMuonHighPTDecision_TOS&&mu2_Hlt2SingleMuonHighPTDecision_TOS))"
  ws_iso   = "&&(mu1_PT*1.00/(mu1_0.50_cc_vPT+mu1_PT))>0.85&&(mu2_PT*1.00/(mu2_0.50_cc_vPT+mu2_PT))>0.85"


  print "Ver WS Eff", 100.0-(tree_ws.GetEntries("1"+ver_cuts)*100.0/tree_ws.GetEntries("1")), "%"
  print "Eta WS Eff", 100.0-(tree_ws.GetEntries("1"+ws_etas)*100.0/tree_ws.GetEntries("1")), "%"
  print "Pt WS Eff",  100.0-(tree_ws.GetEntries("1"+ws_pt)*100.0/tree_ws.GetEntries("1")), "%"
  print "TRK WS Eff", 100.0-(tree_ws.GetEntries("1"+ws_trk)*100.0/tree_ws.GetEntries("1")), "%"
  print "Shared WS Eff", 100.0-(tree_ws.GetEntries("1"+ws_nmuon)*100.0/tree_ws.GetEntries("1")), "%"
  print "Iso WS Eff", 100.0-(tree_ws.GetEntries("1"+ws_iso)*100.0/tree_ws.GetEntries("1")), "%"
  print "Pid WS Eff", 100.0-(tree_ws.GetEntries("1"+ws_pid)*100.0/tree_ws.GetEntries("1")), "%"


  rs_part=tree_rs.CopyTree("1"+rs_etas+rs_trk+rs_nmuon+rs_pid+rs_pt+rs_trg)
  rs_all =tree_rs.CopyTree("1"+rs_etas+rs_trk+rs_nmuon+rs_pid+rs_pt+rs_trg+rs_iso+ver_cuts)

  ws_part=tree_ws.CopyTree("1"+ws_etas+ws_trk+ws_nmuon+ws_pid+ws_pt+ws_trg)
  ws_all =tree_ws.CopyTree("1"+ws_etas+ws_trk+ws_nmuon+ws_pid+ws_pt+ws_trg+ws_iso+ver_cuts)

  CS  = array('d',[0])
  Phi = array('d',[0])
  Mup_PT = array('d',[0])
  Mum_PT = array('d',[0])
  swits = array('d',[0])
  Mup_ETA = array('d',[0])
  Mum_ETA = array('d',[0])

  br_CS = rs_all.Branch('CS',CS,'CS/D')
  br_Phi = rs_all.Branch('Phi',Phi,'Phi/D')
  br_Mup_ETA = rs_all.Branch('Mup_ETA',Mup_ETA,'Mup_ETA/D')
  br_Mum_ETA = rs_all.Branch('Mum_ETA',Mum_ETA,'Mum_ETA/D')
  br_Mup_PT = rs_all.Branch('Mup_PT',Mup_PT,'Mup_PT/D')
  br_Mum_PT = rs_all.Branch('Mum_PT',Mum_PT,'Mum_PT/D')
  br_swits = rs_all.Branch('swits',swits,'swits/D')

  for event in rs_all:
    vec_mup = TLorentzVector()
    vec_mum = TLorentzVector()
    vec_mup.SetPxPyPzE(event.mup_PX,event.mup_PY,event.mup_PZ,event.mup_PE) 
    vec_mum.SetPxPyPzE(event.mum_PX,event.mum_PY,event.mum_PZ,event.mum_PE) 
    Mup_PT[0] = event.mup_PT/1000.
    Mum_PT[0] = event.mum_PT/1000.
    Mup_ETA[0] = vec_mup.Eta()
    Mum_ETA[0] = vec_mum.Eta()
    swits[0] = 1.0
    CS[0] = Collins_Soper(vec_mum, vec_mup)
    Phi[0] = Collins_Soper_Phi(vec_mum,vec_mup)

    br_CS.Fill()
    br_Phi.Fill()
    br_Mup_PT.Fill()
    br_Mum_PT.Fill()
    br_Mup_ETA.Fill()
    br_Mum_ETA.Fill()
    br_swits.Fill()

  rs_part.SetName("RSTree_Pass")
  rs_all.SetName("AnaTree")
  ws_part.SetName("WSTree_Pass")
  ws_all.SetName("WSTree_All")

  f.Write()
  f.Close()

copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2016_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2016_U_Pre.root");
copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2017_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2017_U_Pre.root");
copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2018_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2018_U_Pre.root");
####
copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2016_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2016_D_Pre.root");
copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2017_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2017_D_Pre.root");
copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2018_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Data/Data_2018_D_Pre.root");
###

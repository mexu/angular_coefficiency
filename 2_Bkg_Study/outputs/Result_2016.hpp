const double N_16_TT = 240.17;
const double E_16_Stat_TT = 0.0645268;
const double E_16_Sys_TT = 11.293;
const double N_16_WW = 60.3285;
const double E_16_Stat_WW = 0.128747;
const double E_16_Sys_WW = 0.912213;
const double N_16_TauTau = 559.555;
const double E_16_Stat_TauTau = 0.0422745;
const double E_16_Sys_TauTau = 8.39355;
const double N_16_Mis = 121;
const double E_16_Stat_Mis = 0.0909091;



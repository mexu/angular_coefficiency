from Configurables import DaVinci
dv = DaVinci (
    DataType        = '2016'                ,
    InputType       = 'DST'                 ,
    TupleFile       = 'Tuples.root' ,  
    HistogramFile   = 'Histos.root'         , 
    PrintFreq = 5000,
    SkipEvents = 0,
    EvtMax = -1,
    Simulation = True
    )

from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS,FilterDesktop
from Configurables import BackgroundCategory,  TupleToolMCTruth, TupleToolMCBackgroundInfo, TupleToolDecay, TupleToolVtxIsoln,TupleToolPid,EventCountHisto,TupleToolRecoStats
from Configurables import LoKi__Hybrid__TupleTool, TupleToolVeto, MCDecayTreeTuple
tuple = DecayTreeTuple("tuple")
#tuple.Inputs = ["/Event/ALLTURBO/Phys/Z02MuMuLine/Particles"]
#tuple.Inputs = ["/Phys/Z02MuMuLine/Particles"]
tuple.Inputs = ["/Event/AllStreams/Phys/Z02MuMuLine/Particles"]
tuple.Decay = "Z0 -> ^mu+ ^mu-"
tuple.Branches = {
        "mup"   : "Z0 -> ^mu+  mu-",
        "mum"   : "Z0 ->  mu+ ^mu-",
        "ZBoson"   : "Z0 ->  mu+  mu-"
   }
tl= [
      "TupleToolAngles",
      "TupleToolEventInfo",
      "TupleToolGeometry",
      "TupleToolKinematic",
      "TupleToolPid",
      "TupleToolPrimaries",
      "TupleToolTrackInfo",
      "TupleToolTagging",
      "TupleToolANNPID",
      "MCTupleToolPrimaries",
      "TupleToolConeIsolation",
#      "TupleToolMCTruth",
      "MCTupleToolInteractions"
      ,"TupleToolMCBackgroundInfo"
      ,"MCTupleToolKinematic"
      ,"MCTupleToolHierarchy"
    , "MCTupleToolReconstructed"

    ]
tuple.ToolList += tl


import DecayTreeTuple.Configuration
from Configurables import TupleToolMCTruth
tool = tuple.addTupleTool(TupleToolMCTruth)
tool.ToolList = ["MCTupleToolKinematic", "MCTupleToolHierarchy"]

"""
tuple.ToolList += [
    "TupleToolMCBackgroundInfo",
    "TupleToolMCTruth"
    ]
MCTruth = TupleToolMCTruth()
MCTruth.ToolList =  [
      "MCTupleToolAngles"
    , "MCTupleToolHierarchy"
    , "MCTupleToolKinematic"
    ]
tuple.addTool(MCTruth)
"""
from Configurables import TupleToolRecoStats 
tool = tuple.addTupleTool(TupleToolRecoStats)
tool.Verbose=True


from Configurables import TupleToolTISTOS
tool = tuple.addTupleTool(TupleToolTISTOS)
tool.Verbose=True
tool.TriggerList = [
"L0PhotonDecision",
      "L0MuonDecision",
      "L0DiMuonDecision",
      "L0MuonEWDecision"
     ,"Hlt1SingleMuonNoIPDecision"
     ,"Hlt1SingleMuonHighPTDecision"
     ,"Hlt1TrackAllL0Decision"
     ,"Hlt1TrackMVADecision"
     ,"Hlt1TrackTwoMVADecision"
     ,"Hlt1TrackMuonDecision"
     ,"Hlt1L0AnyDecision"

     ,"Hlt2SingleMuonDecision"
     ,"Hlt2SingleMuonHighPTDecision"
     ,"Hlt2SingleMuonVHighPTDecision"
  ,"Hlt2EWSingleMuonVHighPTDecision"
     ,"Hlt2DiMuonDecision"
     ,"Hlt2DiMuonDY1Decision"
     ,"Hlt2DiMuonDY2Decision"
     ,"Hlt2DiMuonDY3Decision"
     ,"Hlt2DiMuonDY4Decision"

]


tool.Verbose=True
from Configurables import LoKi__Hybrid__EvtTupleTool
tool = tuple.addTupleTool( LoKi__Hybrid__EvtTupleTool, name = 'LoKi_EvtTuple')
tool.VOID_Variables = {
#
    "LoKi_nPVs"                : "CONTAINS('Rec/Vertex/Primary')",
    "LoKi_nSpdMult"            : "CONTAINS('Raw/Spd/Digits')",
    "LoKi_nVeloClusters"       : "CONTAINS('Raw/Velo/Clusters')",
    "LoKi_nVeloLiteClusters"   : "CONTAINS('Raw/Velo/LiteClusters')",
    "LoKi_nITClusters"         : "CONTAINS('Raw/IT/Clusters')",
    "LoKi_nTTClusters"         : "CONTAINS('Raw/TT/Clusters')",
    "LoKi_nOThits"             : "CONTAINS('Raw/OT/Times')"
    }


LoKiTool = LoKi__Hybrid__TupleTool( 'LoKiTool' )
tuple.addTupleTool(LoKi__Hybrid__TupleTool, name = 'LoKiTool' )
tuple.LoKiTool.Variables = {
                              "PERR2":"PERR2"
                              }
tuple.LoKiTool.Preambulo = ["from LoKiTracks.decorators import *"]

simulation = True
mct = MCDecayTreeTuple("mct")
mct.Decay = "[Z0 -> ^mu+ ^mu-]CC"
mct.Branches = {
  "mum"     : "[Z0 -> mu+ ^mu-]CC",
  "mup"   : "[Z0 -> ^mu+ mu-]CC",
  "ZBoson"     : "[Z0 -> mu+ mu-]CC",

}

mctl=[
   'MCTupleToolAngles',
   'MCTupleToolHierarchy',
   'MCTupleToolKinematic',
   'MCTupleToolPrimaries',
   'MCTupleToolReconstructed',
   'MCTupleToolInteractions'
]
mct.ToolList=mctl

dv.UserAlgorithms += [ tuple ]
dv.UserAlgorithms += [ mct]
#from GaudiConf import IOHelper
#IOHelper().inputFiles([
#   '/eos/lhcb/user/m/mexu/For_Z/00049527_00000016_3.AllStreams.dst'
#], clear=True)


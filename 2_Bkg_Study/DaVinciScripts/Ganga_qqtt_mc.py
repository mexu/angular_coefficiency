#Start here
AppDt = DaVinci(version = "v40r0",
      #  setupProjectOptions = '--use-grid',
       # masterpackage = 'Phys/DecayTreeTuple',
        platform='x86_64-slc6-gcc48-opt')

def create_job(Name,Application,OptsFile,bkk_directory,NFilePerJob,MaxFiles,OutputTuple="Tuples.root"): 
   print bkk_directory

   Application.optsfile=[OptsFile]
   job=Job(
	   name = Name,
	   application  = Application,
	   splitter     = SplitByFiles( filesPerJob = NFilePerJob, maxFiles = MaxFiles, bulksubmit = False, ignoremissing = True ),
	   postprocessor  = RootMerger(files=[OutputTuple],overwrite=True,ignorefailed=True),
	   inputsandbox = [],
	   outputfiles  = [LocalFile(OutputTuple)],
	   backend      = Dirac(),
	   #inputdata    = BKQuery(bkk_directory,dqflag=['OK']).getDataset()
	   inputdata    = BKQuery(bkk_directory, type = "Path", dqflag=['OK']).getDataset()
	   )
   job.submit()

#
NumberOfFiles = -1   # -1 means all, one file == 20k events
# RADIATIVE
bkk = "/MC/2016/Beam6500GeV-2016-%s-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/41900007/ALLSTREAMS.DST"
#bkk = "/MC/Dev/Beam6500GeV-Jun2015-%s-Nu1.6-25ns-Pythia8/Sim09Dev03/Trig0x410700a1/Reco15/Turbo01a/Stripping23r1NoPrescalingFlagged/42112001/ALLSTREAMS.DST"
create_job("qq2ttmcD", AppDt, "Z_uu_mc.py", bkk%("MagDown"), 10, NumberOfFiles)
create_job("qq2ttmcU",   AppDt, "Z_uu_mc.py", bkk%("MagUp"), 10, NumberOfFiles)

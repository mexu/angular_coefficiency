#include "headfile.h"
using namespace std;

int GetJobNumber(string Sample_Type){

  int Job_Num;

  if(Sample_Type.find("TTGG_Up")!=string::npos) Job_Num  = 675;
  if(Sample_Type.find("TTGG_Down")!=string::npos) Job_Num  = 676;

  if(Sample_Type.find("TTQQ_Up")!=string::npos) Job_Num  = 677;
  if(Sample_Type.find("TTQQ_Down")!=string::npos) Job_Num  = 678;

  if(Sample_Type.find("WW_Up")!=string::npos) Job_Num  = 679;
  if(Sample_Type.find("WW_Down")!=string::npos) Job_Num  = 680;

  if(Sample_Type.find("ZTauTau_Up")!=string::npos) Job_Num  = 681;
  if(Sample_Type.find("ZTauTau_Down")!=string::npos) Job_Num  = 682;

  if(Sample_Type.find("Single_Up")!=string::npos) Job_Num  = 683;
  if(Sample_Type.find("Single_Down")!=string::npos) Job_Num  = 684;

   return Job_Num;
}

int Get_Lumi(int DaVinci_Job){

  system(Form("find /projects/lhcb/users/mexu/Z2MuMu/DVTuples/MC/%i/ -name \"Script1_Ganga_GaudiExec.log\" | xargs grep \"DaVinciInitAlg    SUCCESS\" > log1.txt",DaVinci_Job));
  system("find . -name \"log1.txt\" | xargs grep \"events processed\" >log2.txt");
  system("sed -n 's/.*SUCCESS \\(\\S\\)/\\1/p' log2.txt >log3.txt");
  system(Form("sed -n 's/ events.*\\(\\S\\)//p' log3.txt > ./Lumi_Info/Lumi_%i.txt",DaVinci_Job));
  system("rm log*.txt");

  ifstream input(Form("./Lumi_Info/Lumi_%i.txt",DaVinci_Job));
  int Lumi_Sum = 0;

   while (!input.eof()){
    int luminosity_tmp;
    input >> luminosity_tmp;
    Lumi_Sum += luminosity_tmp;
  }
  return  Lumi_Sum;
}

void Printhis(){
//void Print_Hist(TChain* ch, string type, int number){

//  cout<<"Start"<<endl;
//  TCanvas* c1 = new TCanvas("c1","c1",700,500);
//  delete c1;
/*
  double ZBoson_M;
  ch->SetBranchAddress( "ZBoson_M", &ZBoson_M );
  TH1D *Hist_Mass = new TH1D("Hist_Mass", "Hist_Mass", 20,60,150);
  for(int i=0;i<1000;i++){
  //for(int i=0;i<ch->GetEntries();i++){
    ch->GetEntry(i);
    Hist_Mass->Fill(ZBoson_M*1.0/1000);
  }
  Hist_Mass->SetXTitle("dimuon invariant mass [GeV]");
  Hist_Mass->SetYTitle("Events");
  char name_input[200];
  sprintf(name_input, "%s(%.0d events 60 < m_{#mu#mu} < 150)",type.c_str(),number);
  TLatex *myTex1 = new TLatex(0.40,0.80,name_input);
  myTex1->SetTextFont(132);
  myTex1->SetTextSize(0.04251055);
  myTex1->SetLineWidth(2);
  myTex1->SetNDC();
  cout<<"Finish "<<endl;
 // Hist_Mass->Draw("EP");
  cout<<"Finish "<<endl;
 // myTex1->Draw("same");
  cout<<"Finish "<<endl;

  c1->SaveAs(("plots/Hist_Mass_"+type+".pdf").c_str());
  cout<<"Finish "<<endl;

  delete ch;
  cout<<"Finish "<<endl;
  delete c1;
  cout<<"Finish "<<endl;
*/
}

void GetTemplatePdf(TTree* chain, string offile, string pdfname){

  double ZBoson_M;
  chain->SetBranchAddress( "ZBoson_M", &ZBoson_M );
  vector<double> vector_zmass;
  for(int i=0;i<chain->GetEntries();i++){
    chain->GetEntry(i);
    double Tmp_Mass = ZBoson_M*1.0/1000.;
    if(Tmp_Mass < 60) continue;
    if(Tmp_Mass > 150) continue;
    vector_zmass.push_back(Tmp_Mass);
   }

   RooRealVar *Mass = new RooRealVar("Mass" ,"",60,150);
   TFile *rootfile = new TFile(Form("%s.root",offile.c_str()), "recreate");
   RooDataSet *data  =new RooDataSet("data","",RooArgSet(*Mass) );

   for(int i=0;i<vector_zmass.size();i++){
       *Mass = vector_zmass[i];
       data->add(RooArgSet(*Mass));
   }

   cout<<"========================================="<<endl;
   cout<<" DataSet : "<<endl;
   data->Print();
   cout<<"========================================="<<endl<<endl;

   RooKeysPdf *shape_Ubin = new RooKeysPdf( Form("ubin_%s",pdfname.c_str()),"",*Mass,*data,RooKeysPdf::MirrorBoth,1.5);
   RooPlot *frame =  Mass->frame();
   data->plotOn(frame);
   shape_Ubin->plotOn(frame,Name("Unbinned"),LineColor(4),LineWidth(2));
   TLegend *leg = new TLegend(0.70,0.32,0.92,0.52);
   leg->AddEntry(frame->findObject("Unbinned"), "Unbinned","L");
   leg->SetBorderSize(0);
   leg->SetTextFont(132);
   leg->SetTextSize(0.0425);
   leg->SetFillColor(0);

   frame->SetTitle("");
   frame->GetXaxis()->SetTitle( pdfname.c_str() );
   frame->GetYaxis()->SetTitle( "" );
   frame->addObject(leg);
   //frame->Draw();
   shape_Ubin->Write();

   rootfile->Write();
   rootfile->Close();

}

void GetIsoVerPdf(TChain* chain, string offile, string pdfname,bool iso){

  TTree* ch = new TTree("DecayTree",  "DecayTree");
  string bkg;
  if(iso) bkg = "mup_0.50_cc_IT<0.7&&mum_0.50_cc_IT<0.7";
  else   bkg = "(Z_ENDVERTEX_CHI2/Z_ENDVERTEX_NDOF)>100";
  ch = chain->CopyTree(bkg.c_str());
  double Z_M;
  ch->SetBranchAddress( "Z_M", &Z_M );
  vector<double> vector_zmass;
  for(int i=0;i<ch->GetEntries();i++){
    ch->GetEntry(i);
    double Tmp_Mass = Z_M*1.0/1000.;
    if(Tmp_Mass < 60) continue;
    if(Tmp_Mass > 150) continue;
    vector_zmass.push_back(Tmp_Mass);
   }

   RooRealVar *Mass = new RooRealVar("Mass" ,"",60,150);
   TFile *rootfile = new TFile(Form("%s.root",offile.c_str()), "recreate");
   RooDataSet *data  =new RooDataSet("data","",RooArgSet(*Mass) );

   for(int i=0;i<vector_zmass.size();i++){
       *Mass = vector_zmass[i];
       data->add(RooArgSet(*Mass));
   }

   cout<<"========================================="<<endl;
   cout<<" DataSet : "<<endl;
   data->Print();
   cout<<"========================================="<<endl<<endl;

   RooKeysPdf *shape_Ubin = new RooKeysPdf( Form("ubin_%s",pdfname.c_str()),"",*Mass,*data,RooKeysPdf::MirrorBoth,1.5);
   RooPlot *frame =  Mass->frame();
   data->plotOn(frame);
   shape_Ubin->plotOn(frame,Name("Unbinned"),LineColor(4),LineWidth(2));
   TLegend *leg = new TLegend(0.70,0.32,0.92,0.52);
   leg->AddEntry(frame->findObject("Unbinned"), "Unbinned","L");
   leg->SetBorderSize(0);
   leg->SetTextFont(132);
   leg->SetTextSize(0.0425);
   leg->SetFillColor(0);

   frame->SetTitle("");
   frame->GetXaxis()->SetTitle( pdfname.c_str() );
   frame->GetYaxis()->SetTitle( "" );
   frame->addObject(leg);
   //frame->Draw();
   shape_Ubin->Write();

   rootfile->Write();
   rootfile->Close();


}

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <map>
#include <vector>
#include <unistd.h>

#include "TCanvas.h"
#include "TChain.h"
#include "TCut.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TMinuit.h"
#include "TNtuple.h"
#include "TObject.h"
#include "TPaveText.h"
#include "TPostScript.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TString.h"
#include "TSelector.h"
#include "TTree.h"
#include "TVector3.h"
#include "RooAbsCategory.h"
#include "RooAbsData.h"
#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooAddition.h"
#include "RooAddPdf.h"
#include "RooAddModel.h"
#include "RooArgList.h"
#include "RooArgusBG.h"
#include "RooCategory.h"
#include "RooBinning.h"
#include "RooBDecay.h"
#include "RooCBShape.h"
#include "RooChebychev.h"
#include "RooConstVar.h"
#include "RooDecay.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooExponential.h"
#include "RooExtendPdf.h"
#include "RooEffProd.h"
#include "RooFFTConvPdf.h"
#include "RooFitResult.h"
#include "RooFormulaVar.h"
#include "RooGaussian.h"
#include "RooGaussModel.h"
#include "RooHist.h"
#include "RooHistPdf.h"
#include "RooKeysPdf.h"
#include "RooMinuit.h"
#include "RooMultiVarGaussian.h"
#include "RooNLLVar.h"
#include "RooNovosibirsk.h"
#include "RooNumConvPdf.h"
#include "RooPlot.h"
#include "RooPolynomial.h"
#include "RooProdPdf.h"
#include "RooRandom.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooStats/BayesianCalculator.h"
#include "RooStats/SPlot.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/LikelihoodIntervalPlot.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"

#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
using namespace std;
using namespace RooFit;
using namespace RooStats;




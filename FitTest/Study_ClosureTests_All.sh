## ********************************************************************** ##
## ********************************************************************** ##
## before any running, please set up env for GB-reweight!!
#source setup.sh
## ********************************************************************** ##
## ********************************************************************** ##
## create output directories 
mkdir -p outputs plots
## number of iterations
nIteration=9
## use default Ai as inputs or use measured Ai as input
Use_Default=1
## ********************************************************************** ##
## ********************************************************************** ##
## first step: generator level study 
root -q -b -l Generator_Study.C
## second step: prepare MC samples
root -q -b -l Init_Samples.C\(${Use_Default}\) 
##
#### ********************************************************************** ##
### run the iteration method without any correction on sub-samples
for istep in $(seq 0 ${nIteration}) ; do
#  ## will not perform GB reweight for the first step
  if [[ ${istep} -ne 0 ]] ; then
    python gbreweight.py ${istep} ${Use_Default}
  fi
#  ## determine Ai in data
  root -q -b -l Ai_Extraction_Iteration.C\(\"2016\"\,${istep}\,${Use_Default}\)
done
root -q -b -l Draw_Iteration.C\(\"2016\"\,${nIteration}\,${Use_Default}\)
### save the fitted results into a new root file
cp outputs/Result_PT_step_${nIteration}_2016.root outputs/Generator_2016_measured.root
#
### ********************************************************************** ##
### ********************************************************************** ##
### the sub-sample is corrected for the input Ai's
Use_Default=0
root -q -b -l Init_Samples.C\(${Use_Default}\) 
#
for istep in $(seq 0 ${nIteration}) ; do
    ## will not perform GB reweight for the first step
  if [[ ${istep} -ne 0 ]] ; then
    python gbreweight.py ${istep} ${Use_Default}
  fi

  ## determine Ai in data
  root -q -b -l Ai_Extraction_Iteration.C\(\"2016\"\,${istep}\,${Use_Default}\)
done
#
root -q -b -l Draw_Iteration.C\(\"2016\"\,${nIteration}\,${Use_Default}\)
#
### ********************************************************************** ##
### ********************************************************************** ##
### make comparison plots
#root -q -b -l Uncertainty_From_Iteration.C\(\"2016\"\,${nIteration}\,${Use_Default}\)
#root -q -b -l Draw_ZPt.C\(\"2016\"\,141\,${Use_Default}\)
#root -q -b -l Draw_ZPt.C\(\"2016\"\,410\,${Use_Default}\)

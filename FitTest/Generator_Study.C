#include "inputs.hpp"

//
// main function to prepare the MC samples for closure tests
//
void Generator_Study(){
  gROOT->ProcessLine(".x ../Function/lhcbStyle.C");

  // read generator level PYTHIA events
  TChain *Ch = new TChain("DecayTree");
  Ch->Add(Form("%s/Pythia_Born.root",LFN_gene.c_str()));

  // set tree branch
  double ZBoson_M, ZBoson_Y, ZBoson_PT, Ana_CS, Ana_Phi, swits;
  Ch->SetBranchAddress("ZBoson_M",   &ZBoson_M);
  Ch->SetBranchAddress("ZBoson_Y",   &ZBoson_Y);
  Ch->SetBranchAddress("ZBoson_PT",  &ZBoson_PT);
  Ch->SetBranchAddress("Ana_CS",     &Ana_CS);
  Ch->SetBranchAddress("Ana_Phi",    &Ana_Phi);
  Ch->SetBranchAddress("swits",      &swits);

  // new root file
  TFile *File_MC_N = new TFile("outputs/PYTHIA_Predictions.root", "recreate");

  TH1D *Hist_CS_before  = new TH1D("Hist_CS_before",  "", 100, -1, 1);
  TH1D *Hist_Phi_before = new TH1D("Hist_Phi_before", "", 64, 0, 6.4);

  TH2D *Hist_A0_2D = new TH2D("Hist_A0_2D", "", nbins_fit_Z_pt, bins_fit_Z_pt, 100, -10, 10);
  TH2D *Hist_A1_2D = new TH2D("Hist_A1_2D", "", nbins_fit_Z_pt, bins_fit_Z_pt, 100, -10, 10);
  TH2D *Hist_A2_2D = new TH2D("Hist_A2_2D", "", nbins_fit_Z_pt, bins_fit_Z_pt, 100, -10, 10);
  TH2D *Hist_A3_2D = new TH2D("Hist_A3_2D", "", nbins_fit_Z_pt, bins_fit_Z_pt, 100, -10, 10);
  TH2D *Hist_A4_2D = new TH2D("Hist_A4_2D", "", nbins_fit_Z_pt, bins_fit_Z_pt, 100, -10, 10);
  TH1D *Hist_FA0   = new TH1D("Hist_FA0", "Hist_FA0", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_FA1   = new TH1D("Hist_FA1", "Hist_FA1", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_FA2   = new TH1D("Hist_FA2", "Hist_FA2", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_FA3   = new TH1D("Hist_FA3", "Hist_FA3", nbins_fit_Z_pt, bins_fit_Z_pt);
  TH1D *Hist_FA4   = new TH1D("Hist_FA4", "Hist_FA4", nbins_fit_Z_pt, bins_fit_Z_pt);

  Hist_CS_before->Sumw2();
  Hist_Phi_before->Sumw2();
  Hist_A0_2D->Sumw2();
  Hist_A1_2D->Sumw2();
  Hist_A2_2D->Sumw2();
  Hist_A3_2D->Sumw2();
  Hist_A4_2D->Sumw2();
  Hist_FA0->Sumw2();
  Hist_FA1->Sumw2();
  Hist_FA2->Sumw2();
  Hist_FA3->Sumw2();
  Hist_FA4->Sumw2();

  //
  // loop events for the first time, to get predicted Ai in PYTHIA
  //
  for(int i = 0; i < Ch->GetEntries(); i++){
    if(i%1000000 == 0) cout<<"  Processing Z-> mu mu events, Loop-1, nevts = "<<i<<" ..."<<endl;
    Ch->GetEntry(i);

    double CS_TRUE  = Ana_CS;
    double Phi_TRUE = Ana_Phi;
    double ZPT_TRUE = ZBoson_PT;
    if(ZPT_TRUE < 0 || ZPT_TRUE > 100) continue;
    if(ZBoson_M < mass_min_closure || ZBoson_M > mass_max_closure) continue;
    if(ZBoson_Y < 2.0) continue;

    double ai_weight = 1.;
    Hist_CS_before->Fill(CS_TRUE, ai_weight);
    Hist_Phi_before->Fill(Phi_TRUE, ai_weight);

    Hist_A0_2D->Fill(ZPT_TRUE, 0.5*(1-3*Ana_CS*Ana_CS)*20/3+2.0/3, swits);
    Hist_A1_2D->Fill(ZPT_TRUE, 2*Ana_CS*sqrt(1-Ana_CS*Ana_CS)*cos(Ana_Phi)*5, swits);
    Hist_A2_2D->Fill(ZPT_TRUE, (1-Ana_CS*Ana_CS)*cos(2*Ana_Phi)*10, swits);
    Hist_A3_2D->Fill(ZPT_TRUE, sqrt(1-Ana_CS*Ana_CS)*cos(Ana_Phi)*4, swits);
    Hist_A4_2D->Fill(ZPT_TRUE, Ana_CS*4, swits);
  }

  // calculate mean value for PYTHIA events
  Calculate_Ai(Hist_A0_2D, Hist_FA0);
  Calculate_Ai(Hist_A1_2D, Hist_FA1);
  Calculate_Ai(Hist_A2_2D, Hist_FA2);
  Calculate_Ai(Hist_A3_2D, Hist_FA3);
  Calculate_Ai(Hist_A4_2D, Hist_FA4);

  File_MC_N->Write();
  File_MC_N->Close();
}

#include "inputs.hpp"

const int n_start = 4;

void Uncertainty_From_Iteration(string _Year, int nstep, int default_Ai = 0){
  gROOT->ProcessLine(".x /home/mlxu/work/angular_v2/Function/lhcbStyle.C");

  // ----------------------------------------------------- //
  // read generator level Ai
  // ----------------------------------------------------- //
  TFile* file_orig = new TFile("outputs/PYTHIA_Predictions.root");
  TFile* file_gene;

  char name_input[200];
  TH1D* hist_Ai_orig[n_Ai];
  TH1D* hist_Ai_gene[n_Ai];
  for(int ipar = 0; ipar < n_Ai; ipar++){
    sprintf(name_input, "Hist_FA%d", ipar);
    hist_Ai_orig[ipar] = (TH1D*) file_orig->Get(name_input);
  }

  // ----------------------------------------------------- //
  // ----------------------------------------------------- //
  // only Loop 121, 131, 141 three tests
  vector<int> type_vec; type_vec.clear();
  type_vec.push_back(121);   type_vec.push_back(131);   type_vec.push_back(141); 
  type_vec.push_back(210);   type_vec.push_back(310);   type_vec.push_back(410); 

  // an array to save differences, from step-4 to the end of loops
  const int _nstep = nstep - n_start + 1;
  const int n_array = _nstep*type_vec.size();
  double diff_Ai[n_Ai][nbins_fit_Z_pt][n_array];
  double max_Ai[n_Ai][nbins_fit_Z_pt];

  // loop all types of studies, which are inputs for uncertainty studies
  for(int itype = 0; itype < type_vec.size(); itype++){
    int study_type = type_vec[itype];

    char testtype[200];
    sprintf(testtype, "Type_%d_Ai_%d", study_type, default_Ai);
    double factor = GetReweightFactor(study_type);

    if(default_Ai) 
      file_gene = new TFile("outputs/PYTHIA_Predictions.root");
    else{
      if(study_type %2 == 1)
        file_gene = new TFile("outputs/Generator_2016_Type_111_measured.root"); // measured input Ai in Sample-B, fix me
      else
        file_gene = new TFile("outputs/Generator_2016_Type_110_measured.root"); // measured input Ai in Sample-A    
    }

    //
    // TFiles for fitted results
    //
    TFile* files[_nstep];
    for(int istep = 0; istep < _nstep; istep++){
      sprintf(name_input, "outputs/Result_PT_step_%d_%s_%s.root", istep + n_start, _Year.c_str(), testtype);
      files[istep] = new TFile(name_input);
    }

    //
    // Loop Ais
    //
    //for(int ipar = 0; ipar < 1; ipar++){
    for(int ipar = 0; ipar < n_Ai; ipar++){
      TH1D* hist_Ai[_nstep];
      sprintf(name_input, "Hist_FA%d", ipar);

      hist_Ai_gene[ipar] = (TH1D*) file_gene->Get(name_input);

      for(int istep = 0; istep < _nstep; istep++){
        hist_Ai[istep] = (TH1D*) files[istep]->Get(name_input);
        for(int ipt = 0; ipt < nbins_fit_Z_pt; ipt++){
          diff_Ai[ipar][ipt][itype*_nstep + istep] = hist_Ai[istep]->GetBinContent(ipt+1) - hist_Ai_gene[ipar]->GetBinContent(ipt+1)*factor;
          //cout<<"--> Ai = "<<ipar<<"  ; PT = "<< ipt<<" ; istep = "<<istep<<"  ; diff = "<<diff_Ai[ipar][ipt][itype*_nstep + istep]<<endl;
        }
      }
      for(int i = 0; i < _nstep; i++){
        delete hist_Ai[i];
      }
    } // ipar loop
    for(int i = 0; i < _nstep; i++){
      files[i]->Close();
    }
  }
  //
  TH1D* hist_err[nbins_fit_Z_pt*n_Ai];
  for(int ipt = 0; ipt < nbins_fit_Z_pt; ipt++){
    for(int ia = 0; ia < n_Ai; ia++){
      char name_tmp[200];
      sprintf(name_tmp, "error_hist_%d_%d", ipt, ia);
      if(ipt < 3) 
        hist_err[nbins_fit_Z_pt*ia+ipt] = new TH1D(name_tmp, "", 20, -0.01, 0.01);
      else
        hist_err[nbins_fit_Z_pt*ia+ipt] = new TH1D(name_tmp, "", 20, -0.03, 0.03);

      double max_index = -9999.;
      for(int i = 0; i < n_array; i++){
        double Ai_index = diff_Ai[ia][ipt][i];
        hist_err[nbins_fit_Z_pt*ia+ipt]->Fill(Ai_index);
        if(fabs(Ai_index) > max_index) 
          max_index = fabs(Ai_index);
      }
      //cout<<max_index<<"  "<<hist_err[nbins_fit_Z_pt*ia+ipt]->GetRMS()<<endl;
      //max_Ai[ia][ipt] = max_index;
      max_Ai[ia][ipt] = hist_err[nbins_fit_Z_pt*ia+ipt]->GetRMS();

    }
  }

  //
  // print out results
  //
  TCanvas* c1 = new TCanvas("c1", "c1", 2100, 1250);
  c1->Divide(6, 5);

  for(int ipt = 0; ipt < nbins_fit_Z_pt; ipt++){
    for(int ia = 0; ia < n_Ai; ia++){
      c1->cd(nbins_fit_Z_pt*ia+ipt+1);
      hist_err[nbins_fit_Z_pt*ia+ipt]->Draw();
    }
  }
  c1->Print("plots/Ai_deviation_ClosureTests.pdf");

  // saved into a root file
  TH1D* uncer_hist[n_Ai];
  TLegend* leg = new TLegend(0.2, 0.6, 0.5, 0.88);
  for(int ia = 0; ia < n_Ai; ia++){
    char name_tmp[200];
    sprintf(name_tmp, "Uncertainty_A%d", ia);
    uncer_hist[ia] = new TH1D(name_tmp, "", nbins_fit_Z_pt, bins_fit_Z_pt);

    for(int ipt = 0; ipt < nbins_fit_Z_pt; ipt++){
      uncer_hist[ia]->SetBinContent(ipt+1, max_Ai[ia][ipt]);
    }
    uncer_hist[ia]->SetLineColor(ia+1);
    uncer_hist[ia]->SetMarkerColor(ia+1);
    sprintf(name_tmp, "A_{%d}", ia);
    leg->AddEntry(uncer_hist[ia], name_tmp, "l");
  }

  TCanvas* c2 = new TCanvas("c2", "c2", 2100, 1250);
  uncer_hist[0]->SetAxisRange(0.0, 0.02, "Y");
  uncer_hist[0]->SetXTitle("Z p_{T} (GeV)");
  uncer_hist[0]->SetYTitle("Uncertainty");
  uncer_hist[0]->Draw("hist");

  for(int ia = 1; ia < n_Ai; ia++){
    uncer_hist[ia]->Draw("hist&&same");
  }
  leg->Draw("same");

  c2->Print("plots/Ai_uncertainty_ClosureTests.pdf");
}

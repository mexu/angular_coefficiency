#include "inputs.hpp"

void Draw_Iteration(string _Year, int nstep, int default_Ai){
  gROOT->ProcessLine(".x ../Function/lhcbStyle.C");

  char testtype[200];
  sprintf(testtype, "Ai_%d",  default_Ai);
  char name_input[200];

  //
  // read generator level Ai
  //
  TFile* file_orig = new TFile("outputs/PYTHIA_Predictions.root");

  TFile* file_gene;
  if(default_Ai) 
    file_gene = new TFile("outputs/PYTHIA_Predictions.root");
  else{
    file_gene = new TFile("outputs/Generator_2016_measured.root"); // measured input Ai in Sample-A    
  }

  TH1D* hist_Ai_orig[n_Ai];
  TH1D* hist_Ai_gene[n_Ai];
  for(int ipar = 0; ipar < n_Ai; ipar++){
    sprintf(name_input, "Hist_FA%d", ipar);
    hist_Ai_orig[ipar] = (TH1D*) file_orig->Get(name_input);
    hist_Ai_gene[ipar] = (TH1D*) file_gene->Get(name_input);
  }

  //
  // TFiles for fitted results
  //
  const int _nstep = nstep+2;
  TFile* files[_nstep];
  for(int istep = 0; istep <= nstep; istep++){
    sprintf(name_input, "outputs/Result_PT_step_%d_%s.root", istep, _Year.c_str());
    files[istep] = new TFile(name_input);
  }

  //
  // Loop Ais
  //
  //for(int ipar = 0; ipar < 1; ipar++){
  for(int ipar = 0; ipar < n_Ai; ipar++){
    double x_axis[_nstep] ;
    double x_err[_nstep] ;
    double y_axis[nbins_fit_Z_pt][_nstep] ;
    double y_err[nbins_fit_Z_pt][_nstep] ;

    TH1D* hist_Ai[_nstep-1];
    sprintf(name_input, "Hist_FA%d", ipar);

    for(int istep = 0; istep <= nstep; istep++){
      hist_Ai[istep] = (TH1D*) files[istep]->Get(name_input);
      for(int ipt = 0; ipt < nbins_fit_Z_pt; ipt++){
        //y_axis[ipt][istep+1] = hist_Ai[istep]->GetBinContent(ipt+1);
        y_axis[ipt][istep+1] = hist_Ai[istep]->GetBinContent(ipt+1) - hist_Ai_gene[ipar]->GetBinContent(ipt+1)*1;
        y_err[ipt][istep+1] = 0.;
        //y_err[ipt][istep+1] = hist_Ai[istep]->GetBinError(ipt+1);
      }
      x_axis[istep+1] = istep+1;
      x_err[istep+1]  = 0.;
    }
    // the first bin, should be the Ai in the MC sample
    x_axis[0] = 0.;
    x_err[0]  = 0.;
    for(int ipt = 0; ipt < nbins_fit_Z_pt; ipt++){
      //y_axis[ipt][0] = hist_Ai_orig[ipar]->GetBinContent(ipt+1);
      y_axis[ipt][0] = hist_Ai_orig[ipar]->GetBinContent(ipt+1) - hist_Ai_gene[ipar]->GetBinContent(ipt+1)*1;
      y_err[ipt][0] = 0.;
      //y_err[ipt][0] = hist_Ai_orig[ipar]->GetBinError(ipt+1);
    }

    // create TGraph
    TLegend* leg = new TLegend(0.29, 0.69, 0.49, 0.89);
    TLegend* leg2 = new TLegend(0.51, 0.69, 0.71, 0.89);
    TGraphErrors* gr_Ai[nbins_fit_Z_pt];
    for(int ipt = 0; ipt < nbins_fit_Z_pt; ipt++){
      gr_Ai[ipt] = new TGraphErrors(_nstep, x_axis, y_axis[ipt], x_err, y_err[ipt]);
      gr_Ai[ipt]->SetLineColor(ipt+1);
      gr_Ai[ipt]->SetMarkerColor(ipt+1);
      gr_Ai[ipt]->SetLineWidth(2);
      if(ipt < 3)
        leg->AddEntry(gr_Ai[ipt],Form("Z p_{T} bin: %d", ipt+1), "ep");
      else
        leg2->AddEntry(gr_Ai[ipt],Form("Z p_{T} bin: %d", ipt+1), "ep");
    }

    //
    // print out results
    //
    TCanvas* c1 = new TCanvas("c1", "c1", 700, 500);
    TH1F* frame = new TH1F("frame", "", 100, -0.5, _nstep-0.5);

    frame->SetXTitle("n-Iteration");
    frame->SetYTitle("Deviation");
    frame->SetTitle("");
    frame->SetAxisRange(-0.1, 0.3, "Y");
    if(!default_Ai)
      frame->SetAxisRange(-0.05, 0.1, "Y");
    frame->SetLineColor(0);

    frame->Draw("l");
    for(int ipt = 0; ipt < nbins_fit_Z_pt; ipt++)
      gr_Ai[ipt]->Draw("LP&&same");

    leg->Draw("same");
    leg2->Draw("same");

    sprintf(name_input, "plots/Deviation_A%d_2016_Iteration.pdf", ipar);
    c1->Print(name_input);

    for(int i = 0; i < nbins_fit_Z_pt; i++){
      delete gr_Ai[i];
    }
    for(int i = 0; i < (_nstep-1); i++){
      delete hist_Ai[i];
    }
    delete leg;
    delete leg2;
    delete c1;
    delete frame;
  }
}

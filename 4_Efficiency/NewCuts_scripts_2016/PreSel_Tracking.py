from ROOT import *
import os,sys

def inver_variable(tree,var1,var2,var3,var4,var5,var6,var7,var8,var9,var10,var11,var12,var13,var14):

    tree.SetBranchStatus("*",0)
    tree.SetBranchStatus(var1,1)
    tree.SetBranchStatus(var2,1)
    tree.SetBranchStatus(var3,1)
    tree.SetBranchStatus(var4,1)
    tree.SetBranchStatus(var5,1)
    tree.SetBranchStatus(var6,1)
    tree.SetBranchStatus(var7,1)
    tree.SetBranchStatus(var8,1)
    tree.SetBranchStatus(var9,1)
    tree.SetBranchStatus(var10,1)
    tree.SetBranchStatus(var11,1)
    tree.SetBranchStatus(var12,1)
    tree.SetBranchStatus(var13,1)
    tree.SetBranchStatus(var14,1)

def variable(tree,var1,var2,var3,var4,var5,var6,var7,var8,var9,var10,var11,var12):

    tree.SetBranchStatus("*",0)
    tree.SetBranchStatus(var1,1)
    tree.SetBranchStatus(var2,1)
    tree.SetBranchStatus(var3,1)
    tree.SetBranchStatus(var4,1)
    tree.SetBranchStatus(var5,1)
    tree.SetBranchStatus(var6,1)
    tree.SetBranchStatus(var7,1)
    tree.SetBranchStatus(var8,1)
    tree.SetBranchStatus(var9,1)
    tree.SetBranchStatus(var10,1)
    tree.SetBranchStatus(var11,1)
    tree.SetBranchStatus(var12,1)

def copytree_mct(fin,fout):

    ch_mct  =TChain("MCTZ/MCDecayTree")
    ch_mct.Add(fin)

    f=TFile(fout,"recreate")
    
    mass_cuts= "&& Z_M>70000 && Z_M<110000";
    id_cuts = "&& abs(Z_TRUEID) ==23 && abs(mup_TRUEID) ==13 && abs(mup_MC_MOTHER_ID) ==23 && abs(mum_TRUEID) ==13 && abs(mum_MC_MOTHER_ID) ==23";
    pt_cuts = "&&mum_ETA >2.0 && mum_ETA <4.5 && mum_PT>10000 && mup_ETA >2.0 && mup_ETA <4.5 && mup_PT>10000";

    pass_mct  = "1" + mass_cuts+id_cuts+pt_cuts;
    pre_mct = ch_mct.CopyTree(pass_mct)
    pre_mct.SetName("MCT_Pre");

    pre_mct.Write();
    f.Close();


def copytree(fin,fout,mc):

    ch_plus =TChain("PlusTag/DecayTree")
    ch_minus=TChain("MinusTag/DecayTree")
    ch_plus.Add(fin)
    ch_minus.Add(fin)

    f=TFile(fout,"recreate")
 
    tag_cuts        = "&& (sqrt(tag_PERR2)/tag_P) < 0.1";
    #tag_cuts        = "&& tag_TRACK_PCHI2 > 0.001 && (sqrt(tag_PERR2)/tag_P) < 0.1";
    tag_cuts       += "&& tag_isMuon ==1";
    tag_cuts       += "&& tag_PT > 20000 && tag_ETA > 2.0 && tag_ETA < 4.5";
    tag_cuts       += "&& tag_Hlt2SingleMuonHighPTDecision_TOS==1";
    tag_cuts       += "&& tag_Hlt1SingleMuonHighPTDecision_TOS == 1";
    tag_cuts       += "&& tag_L0MuonEWDecision_TOS ==1";
 
    probe_cuts      = "&& probe_PT > 10000 && probe_ETA > 1.8 && probe_ETA < 4.5";
 
    Z_cuts          = "&& (abs(tag_PHI-probe_PHI)<TMath::Pi() ? abs(tag_PHI-probe_PHI) : 2*TMath::Pi()-abs(tag_PHI-probe_PHI))>0.1";
    Z_cuts         += "&& boson_M > 70000 && boson_M < 110000";
    Z_cuts         += "&& boson_ENDVERTEX_CHI2/boson_ENDVERTEX_NDOF < 5";
    if(mc):
      Z_cuts       += "&& abs(tag_TRUEID) == 13 && abs(tag_MC_MOTHER_ID) ==23";

    sel_cuts        = "&&probe_AssocZM == 1";
    fail_cuts       = "&&probe_AssocZM == 0";
 
    pass_den   = "1" + tag_cuts + probe_cuts + Z_cuts;
    pass_num   = "1" + tag_cuts + probe_cuts + Z_cuts+ sel_cuts;
    fail_num   = "1" + tag_cuts + probe_cuts + Z_cuts + fail_cuts;
    
    plus_den =  ch_plus.CopyTree(pass_den)
    minus_den = ch_minus.CopyTree(pass_den)
    plus_num =  ch_plus.CopyTree(pass_num)
    minus_num = ch_minus.CopyTree(pass_num)
    plus_fail  = ch_minus.CopyTree(fail_num)
    minus_fail = ch_plus.CopyTree(fail_num)

    variable(plus_den,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")
    variable(plus_num,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")
    variable(minus_den,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")
    variable(minus_num,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")
    variable(plus_fail,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")
    variable(minus_fail,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")

    plus_den.SetName("TreePlus_Den");
    plus_num.SetName("TreePlus_Num");
    minus_den.SetName("TreeMinus_Den");
    minus_num.SetName("TreeMinus_Num");
    plus_fail.SetName("TreePlus_Fail");
    minus_fail.SetName("TreeMinus_Fail");

    plus_den.Write()
    plus_num.Write()
    minus_den.Write()
    minus_num.Write()
    plus_fail.Write()
    minus_fail.Write()
    f.Close()


def copylongtrack(fin,fout):

  ch_long = TChain("LongLong/DecayTree")
  #ch_long = TChain("LongLongTuple/DecayTree")
  ch_long.Add(fin)
  f=TFile(fout,"recreate")
  long_cuts      = "&& (sqrt(muplus_PERR2)/muplus_P) < 0.1";
  #long_cuts      = "&& muplus_TRACK_PCHI2 > 0.001 && (sqrt(muplus_PERR2)/muplus_P) < 0.1";
  long_cuts      = "&& (sqrt(muminus_PERR2)/muminus_P) < 0.1";
  #long_cuts      = "&& muminus_TRACK_PCHI2 > 0.001 && (sqrt(muminus_PERR2)/muminus_P) < 0.1";
  long_cuts     += "&& muplus_isMuon==1 && muminus_isMuon==1";
  long_cuts     += "&& muplus_PT > 10000 && muplus_ETA>2.0 && muplus_ETA<4.5";
  long_cuts     += "&& muminus_PT > 10000 && muminus_ETA>2.0 && muminus_ETA<4.5";
  long_cuts     += "&& (abs(muplus_PHI-muminus_PHI)<TMath::Pi() ? abs(muplus_PHI-muminus_PHI) : 2*TMath::Pi()-abs(muplus_PHI-muminus_PHI))>0.1";
  long_cuts     += "&& Z0_M >70000 && Z0_M <110000";
  long_cutso    += "&&(mup_PT*1.00/(mup_0.50_cc_vPT+mup_PT))>0.85&&(mum_PT*1.00/(mum_0.50_cc_vPT+mum_PT))>0.85"
  long_cuts     += "&& Z0_ENDVERTEX_CHI2/Z0_ENDVERTEX_NDOF < 5";
  long_cuts     += "&&((muplus_L0MuonEWDecision_TOS==1&&muplus_Hlt1SingleMuonHighPTDecision_TOS==1&&muplus_Hlt2SingleMuonHighPTDecision_TOS==1)||(muminus_L0MuonEWDecision_TOS==1&&muminus_Hlt1SingleMuonHighPTDecision_TOS==1&&muminus_Hlt2SingleMuonHighPTDecision_TOS==1))";
  long_pre = ch_long.CopyTree("1"+long_cuts) 
    #rs_nmuon = "&&mup_MuonNShared==0 && mum_MuonNShared==0"
  long_pre.SetName("TreLong")
  long_pre.Write()
  f.Close()


#Data
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2016_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2016_U_Pre.root",False); #Data MagUp Tag-and-Probe
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2016_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2016_D_Pre.root",False); #Data MagDown Tag-and-Probe
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2017_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2017_U_Pre.root",False); #Data MagUp Tag-and-Probe
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2017_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2017_D_Pre.root",False); #Data MagDown Tag-and-Probe
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2018_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2018_U_Pre.root",False); #Data MagUp Tag-and-Probe
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2018_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2018_D_Pre.root",False); #Data MagDown Tag-and-Probe
#2016
#MC
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_U_Pre.root",True); #MagUp Tag-and-Probe
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_D_Pre.root",True); #MagDown Tag-and-Probe
##
#####MCT
#copytree_mct("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_MCT_U_Pre.root");
#copytree_mct("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_MCT_D_Pre.root");
#
#copylongtrack("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_Long_U_Pre.root"); #MagUp Tag-and-Probe
#copylongtrack("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_MC_2016_Long_D_Pre.root"); #MagDown Tag-and-Probe

#Long Track
#copylongtrack("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2016_L_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2016_Long_D_Pre.root"); #MagUp Tag-and-Probe
copylongtrack("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2016_L_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2016_Long_U_Pre.root"); #MagUp Tag-and-Probe
#copylongtrack("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2017_L_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2017_Long_D_Pre.root"); #MagUp Tag-and-Probe
#copylongtrack("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2017_L_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2017_Long_U_Pre.root"); #MagUp Tag-and-Probe
#copylongtrack("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2018_L_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2018_Long_D_Pre.root"); #MagUp Tag-and-Probe
#copylongtrack("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2018_L_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/TRK/TRK_Data_2018_Long_U_Pre.root"); #MagUp Tag-and-Probe


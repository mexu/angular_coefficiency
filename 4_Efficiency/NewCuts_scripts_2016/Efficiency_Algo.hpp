//==============================================================//
// Algorithms to study muon efficiency, with Z->MuMu events     //
//  using both tag-and-probe method and MC-Matched method.      //
//==============================================================//
#include <TStyle.h>

#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TF1.h"
#include "TROOT.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TFile.h"
#include "TLine.h"
#include "TMath.h"
#include "TLatex.h"
#include "TAttText.h"
#include <TLegend.h>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include "TChain.h"
#include "TTree.h"
#include "TCut.h"
#include "TLorentzVector.h"
#include "TCanvas.h"

using namespace std;

// names
string EffName(int _type){
  string name_tmp = "";
  if(_type == 0) name_tmp = "TRKEFF";
  else if(_type == 1) name_tmp = "ID";
  else if(_type == 2) name_tmp = "TRG";
  else cout<<"Wrong input efficiency type, please check !!!"<<endl;
  return name_tmp;
}
// var names
string VarName(int _type){
  string name_tmp = "";
  if(_type == 0) name_tmp = "ETA";
  else if(_type == 1) name_tmp = "PT";
  else if(_type == 2) name_tmp = "PHI";
  else if(_type == 3) name_tmp = "SPD";
  else cout<<"Wrong input efficiency variable, please check !!!"<<endl;
  return name_tmp;
} 

// var names
string VarName2D(int _type){
  string name_tmp = "";
  if(_type == 0) name_tmp = "ETA_PHI";
  else if(_type == 1) name_tmp = "ETA_PT";
  else cout<<"Wrong input efficiency variable, please check !!!"<<endl;
  return name_tmp;
} 

// Eff types
string SampleName(int _type){
  string name_tmp = "";
  if(_type == 0) name_tmp = "ALL";
  else if(_type == 1) name_tmp = "DOWN";
  else if(_type == 2) name_tmp = "UP";
  else if(_type == 3) name_tmp = "PLUS";
  else if(_type == 4) name_tmp = "MINUS";
  else cout<<"Wrong input efficiency sample, please check !!!"<<endl;
  return name_tmp;
}

// calcualte the efficiency error
double Efficiency_Error(double a, double b){
  return TMath::Sqrt((a+1.0)*fabs(b-a+1.0)/(b+2.0)/(b+2.0)/(b+3.0));
}

// calculate ratio error
double Ratio_Error(double a, double b, double a_err, double b_err){
  double part_1 = (a_err*a_err)/(b*b);
  double part_2 = (a*a*b_err*b_err)/(b*b*b*b);
  return TMath::Sqrt(part_1 + part_2);
}
//Prit eff result to txt
void Pinrt_Eff_Result(string _type_eff,string year,TH1D* Hist_Eff,string name){

  ofstream out;
  cout<<"Ru----------------------------------"<<endl;
  out.open(Form("./outputs/Eff_Result_%s_%s.hpp",_type_eff.c_str(),year.c_str()));
  out<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
  out<<"\\begin{table}"<<endl;
  out<<"\\centering"<<endl;
  out<<"\\begin{tabular}{c|c|c}"<<endl;
  out<<"\\hline"<<endl;
  out<<"$\\eta$ & $\\epsilon$ & $\\delta epsilon_{trg}$\\"<<endl;
  out<<"\\hline"<<endl;
  for(int i = 1; i < Hist_Eff->GetNbinsX()+1; i++){
    out<< setiosflags(ios::fixed)<< setprecision(3)<<bins_eta[i-1]*1.000<<"-"<<bins_eta[i]<<" & "<<Hist_Eff->GetBinContent(i)<<" & "<<Hist_Eff->GetBinError(i)<<endl;
  }
  out<<"\\end{tabular}\\label{tab:eff_"<<year.c_str()<<"_"<<_type_eff.c_str()<<"}"<<endl;
  out<<"\\end{table}"<<endl;
  out<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
  out.close();
}


// 1D efficiency
void Efficiency_1D(TH1D* hist_den, TH1D* hist_num, TH1D* hist){
  for(int i = 1; i < hist_den->GetNbinsX()+1; i++){
    hist->SetBinContent(i, 0.);
    hist->SetBinError(i, 0.);
        
    double n_deno = hist_den->GetBinContent(i);
    double n_nume = hist_num->GetBinContent(i);
        
    double ratio = 0.;
    double error = 0.;
    if(n_deno > 0){
      ratio = n_nume/n_deno;
      error = Efficiency_Error(n_nume, n_deno);
    }
    hist->SetBinContent(i, ratio);
    hist->SetBinError(i, error);
  }
}

// 2D efficiency
void Efficiency_2D(TH2D* hist_den, TH2D* hist_num, TH2D* hist){
  for(int i = 1; i < hist_den->GetNbinsX()+1; i++){
    for(int j = 1; j < hist_den->GetNbinsY()+1; j++){
      hist->SetBinContent(i, j, 0.);
      hist->SetBinError(i, j, 0.);
            
      double n_deno = hist_den->GetBinContent(i, j);
      double n_nume = hist_num->GetBinContent(i, j);
      double ratio = 0.;
      double error = 0.;
      if(n_deno > 0){
	ratio = n_nume/n_deno;
	error = Efficiency_Error(n_nume, n_deno);
      }
            
      hist->SetBinContent(i, j, ratio);
      hist->SetBinError(i, j, error);
    }
  }
}

// 1D ratio
void Ratio_1D(TH1D* hist_den, TH1D* hist_num, TH1D* hist){
  for(int i = 1; i < hist_den->GetNbinsX()+1; i++){
    hist->SetBinContent(i, 0.);
    hist->SetBinError(i, 0.);
        
    double n_deno = hist_den->GetBinContent(i);
    double n_nume = hist_num->GetBinContent(i);
    double e_deno = hist_den->GetBinError(i);
    double e_nume = hist_num->GetBinError(i);
        
    double ratio = 0.;
    double error = 0.;

    if(n_deno > 0 && n_nume > 0){
      ratio = n_nume/n_deno;
       cout<<ratio<<endl;
      double err1 = e_deno/n_deno;
      double err2 = e_nume/n_nume;
      error = ratio*sqrt(err1*err1 + err2*err2);
    }
    hist->SetBinContent(i, ratio);
    hist->SetBinError(i, error);
  }
}

// 2D ratio
void Ratio_2D(TH2D* hist_den, TH2D* hist_num, TH2D* hist){
  for(int i = 1; i < hist_den->GetNbinsX()+1; i++){
    for(int j = 1; j < hist_den->GetNbinsY()+1; j++){
      hist->SetBinContent(i, j, 0.);
      hist->SetBinError(i, j, 0.);
        
      double n_deno = hist_den->GetBinContent(i, j);
      double n_nume = hist_num->GetBinContent(i, j);
      double e_deno = hist_den->GetBinError(i, j);
      double e_nume = hist_num->GetBinError(i, j);
        
      double ratio = 0.;
      double error = 0.;

      if(n_deno > 0 && n_nume > 0){
	ratio = n_nume/n_deno;
	double err1 = e_deno/n_deno;
	double err2 = e_nume/n_nume;
	error = ratio*sqrt(err1*err1 + err2*err2);
      }
      hist->SetBinContent(i, j, ratio);
      hist->SetBinError(i, j, error);
    }
  }
}

// calculate chi
void Calculate_Chi(TH1D* hist1, TH1D* hist2, TH1D* chi_hist){
  for(int i = 1; i < (hist1->GetNbinsX() + 1); i++){
    double n1 = hist1->GetBinContent(i);
    double n2 = hist2->GetBinContent(i);
    double e1 = hist1->GetBinError(i);
    double e2 = hist2->GetBinError(i);

    double chi = 0.;
    double err = sqrt(e1*e1+e2*e2);
    if(err > 0) chi = (n1 - n2)/ err;
    chi_hist->SetBinContent(i, chi);
    chi_hist->SetBinError(i, 1.);
  }
}

// compare tag and probe eff and print ratio plot  

void Compare_Efficiency(TH1D* Hist_Tag, TH1D* Hist_Match,string _vartype, string _year,string _efftype){

  // calcualte chi
  TH1D* Hist_Ratio = (TH1D*)(Hist_Tag->Clone());
  Ratio_1D(Hist_Tag, Hist_Match, Hist_Ratio);
  Hist_Ratio->SetName(Form("Hist_Ratio_%s_%s_%s",_efftype.c_str(),_vartype.c_str(),_year.c_str()));
  // x-title 
  string xtitle = "";
  if(_vartype.find("ETA")!=string::npos)      xtitle = "#eta";
  else if(_vartype.find("PHI")!=string::npos) xtitle = "#phi";
  else if(_vartype.find("PT")!=string::npos)  xtitle = "p_{T} (GeV/#it{c}^{2})";
  else if(_vartype.find("SPD")!=string::npos) xtitle = "nSPD";
  else xtitle = "";

  //y range
  double y_min,y_max;
  if(_efftype.find("ID")!=string::npos)       { y_min = 0.3; y_max = 1.05;}
  else if(_efftype.find("TRK")!=string::npos) { y_min = 0.85; y_max = 1.00;}
  else if(_efftype.find("TRG")!=string::npos) { y_min = 0.5; y_max = 0.9;}

 // define TCanvas
  TCanvas* c = new TCanvas("c", "c", 700, 600);
  gROOT->ForceStyle();
  gStyle->SetOptStat(0);

  // split the canvas into 2 pads
  c->Divide(1,2,0,0,0) ;

  // go to pad 2, and draw eff distribution
  c->cd(2);

  // pad 2 setting
  gPad->SetGrid();
  gPad->SetPad(0.02, 0.25, 0.95, 0.97);
  gPad->SetTopMargin(0);
  gPad->SetLeftMargin(0.12);
  gPad->SetRightMargin(0.045);
  gPad->SetBottomMargin(0.48);
  gPad->SetPad(0.02, 0.02,0.95,0.25);

  Hist_Ratio->GetXaxis()->SetNdivisions(508);
  Hist_Ratio->GetXaxis()->SetLabelSize(0.20);
  Hist_Ratio->GetXaxis()->SetTitleSize(0.22);
  Hist_Ratio->GetXaxis()->SetTitleOffset(0.85);
  Hist_Ratio->GetYaxis()->SetLabelSize(0.17);
  Hist_Ratio->GetYaxis()->SetTitleSize(0.20);
  Hist_Ratio->GetYaxis()->SetTitleOffset(0.24);
  Hist_Ratio->SetYTitle("Ratio");
  Hist_Ratio->GetYaxis()->SetNdivisions(503);
  Hist_Ratio->SetAxisRange(0.95,1.05, "Y");

  int high_bin = Hist_Tag->GetNbinsX();
  TLine* line1 = new TLine(Hist_Tag->GetXaxis()->GetBinLowEdge(1), 0.1, Hist_Tag->GetXaxis()->GetBinLowEdge(high_bin) + Hist_Tag->GetXaxis()->GetBinWidth(high_bin), 0.1);
  line1->SetLineColor(2);
  line1->SetLineWidth(2);
  line1->SetLineStyle(2);

  Hist_Ratio->Draw();
  Hist_Ratio->SetXTitle(xtitle.c_str());
  line1->Draw("same");

  // go to pad 1
  c->cd(1);
  gPad->SetGrid();
  gPad->SetBottomMargin(0);
  gPad->SetLeftMargin(0.12);
  gPad->SetRightMargin(0.045);
  gPad->SetPad(0.02, 0.25, 0.95, 0.97);

  // histograms settings
  Hist_Tag->SetLineColor(4);
  Hist_Tag->SetLineWidth(2);
  Hist_Tag->SetMarkerColor(4);
  Hist_Tag->SetMarkerStyle(23);

  Hist_Match->SetLineColor(2);
  Hist_Match->SetLineWidth(2);
  Hist_Match->SetMarkerColor(2);
  Hist_Match->SetMarkerStyle(26);

  Hist_Tag->SetYTitle(Form("Muon %s Efficiency",_efftype.c_str()));
  Hist_Tag->SetAxisRange(y_min, y_max, "Y");
  Hist_Tag->GetYaxis()->SetLabelSize(0.05);
  // Tlegend
  TLegend* leg = new TLegend(0.2, 0.2, 0.4, 0.4);
  leg->SetLineColor(0);
  leg->SetFillColor(0);
  leg->AddEntry(Hist_Tag, "Tag&Probe", "epl");
  leg->AddEntry(Hist_Match, "MC-Match", "epl");

  Hist_Tag->Draw();
  Hist_Match->Draw("same");
  leg->Draw("same");

  // print out
  for(int i=0;i<n_format;i++){
    c->Print(Form("../../plots/Eff/Sys/Method_%s_%s_%s%s",_year.c_str(),_efftype.c_str(),_vartype.c_str(),format[i].c_str()));
   }
  delete c;  delete leg;

}


// handle different eff depends: 1D
void Calculate_ZMuMu_Efficiency_1D(TChain* ch_den, TChain* ch_num, TChain* ch_den_match, TChain* ch_num_match,
                                   string name, string sample, string subname,
                                   string year, bool _Study_Match, string _type_eff){
    
  // define TH1D histogram for denomator and numerator
  TH1D* Hist_Den; TH1D* Hist_Num; TH1D* Hist_Eff;
  TH1D* Hist_Matched_Den;  TH1D* Hist_Matched_Num;  TH1D* Hist_Matched_Eff;

  // fill histograms for eta, pt, phi, and nSPD

  if(name.find("ETA")!=string::npos){
    Hist_Den = new TH1D(Form("Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_Num = new TH1D(Form("Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_Eff = new TH1D(Form("Hist_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_Den->SetXTitle("Muon #eta");
    ch_den->Draw(Form("probe_ETA>>Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
    ch_num->Draw(Form("probe_ETA>>Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
//For Match
    if(_Study_Match){
      Hist_Matched_Den = new TH1D(Form("Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta);
      Hist_Matched_Num = new TH1D(Form("Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta);
      Hist_Matched_Eff = new TH1D(Form("Hist_Matched_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta);
      Hist_Matched_Den->SetXTitle("Muon #eta");
     if(_type_eff.find("TRK")!=string::npos){
        ch_den_match->Draw(Form("mup_ETA>>Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()),"mup_Reconstructed==1");
        ch_den_match->Draw(Form("mup_ETA>>Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
     }
     else{
        ch_den_match->Draw(Form("probe_ETA>>Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
        ch_num_match->Draw(Form("probe_ETA>>Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
      }
     }
  }else if(name.find("PT")!=string::npos){
    Hist_Den = new TH1D(Form("Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_Num = new TH1D(Form("Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_Eff = new TH1D(Form("Hist_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_Den->SetXTitle("Muon p_{T} (GeV)");
    ch_den->Draw(Form("probe_PT/1000.>>Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
    ch_num->Draw(Form("probe_PT/1000.>>Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
    if(_Study_Match){
      Hist_Matched_Den = new TH1D(Form("Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_pt, bins_pt); 
      Hist_Matched_Num = new TH1D(Form("Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_pt, bins_pt); 
      Hist_Matched_Eff = new TH1D(Form("Hist_Matched_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_pt, bins_pt);
      Hist_Matched_Den->SetXTitle("Muon  p_{T} (GeV)");
      if(_type_eff.find("TRK")!=string::npos){
        ch_den_match->Draw(Form("mup_PT/1000.>>Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()),"mup_Reconstructed==1");
        ch_den_match->Draw(Form("mup_PT/1000.>>Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
     }
     else{
        ch_den_match->Draw(Form("probe_PT/1000.>>Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
        ch_num_match->Draw(Form("probe_PT/1000.>>Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
      }
    }
  }else if(name.find("PHI")!=string::npos){
    Hist_Den = new TH1D(Form("Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_phi, bins_phi);
    Hist_Num = new TH1D(Form("Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_phi, bins_phi);
    Hist_Eff = new TH1D(Form("Hist_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_phi, bins_phi);
    Hist_Den->SetXTitle("Muon #phi");
    ch_den->Draw(Form("probe_PHI>>Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
    ch_num->Draw(Form("probe_PHI>>Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
    if(_Study_Match){
      Hist_Matched_Den = new TH1D(Form("Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_phi, bins_phi); 
      Hist_Matched_Num = new TH1D(Form("Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_phi, bins_phi); 
      Hist_Matched_Eff = new TH1D(Form("Hist_Matched_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_phi, bins_phi);
      Hist_Matched_Den->SetXTitle("Muon #phi");
      if(_type_eff.find("TRK")!=string::npos){
        ch_den_match->Draw(Form("mup_PHI>>Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()),"mup_Reconstructed==1");
        ch_den_match->Draw(Form("mup_PHI>>Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
     }
     else{
        ch_den_match->Draw(Form("probe_PHI>>Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
        ch_num_match->Draw(Form("probe_PHI>>Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
      }
     }
  }else if(name.find("SPD")!=string::npos){
    Hist_Den = new TH1D(Form("Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_spd, bins_spd);
    Hist_Num = new TH1D(Form("Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_spd, bins_spd);
    Hist_Eff = new TH1D(Form("Hist_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_spd, bins_spd);
    Hist_Den->SetXTitle("nSPDHits");
    ch_den->Draw(Form("nSPDHits>>Hist_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
    ch_num->Draw(Form("nSPDHits>>Hist_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
    if(_Study_Match){
      Hist_Matched_Den = new TH1D(Form("Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_spd, bins_spd);
      Hist_Matched_Num = new TH1D(Form("Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_spd, bins_spd);
      Hist_Matched_Eff = new TH1D(Form("Hist_Matched_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), Form("Hist_Matched_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_spd, bins_spd);
      Hist_Matched_Den->SetXTitle("nSPDHits");
      if(_type_eff.find("TRK")!=string::npos){
        ch_den_match->Draw(Form("nSPDHits>>Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()),"mum_Reconstructed==1");
        ch_den_match->Draw(Form("nSPDHits>>Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
     }
     else{
        ch_den_match->Draw(Form("nSPDHits>>Hist_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
        ch_num_match->Draw(Form("nSPDHits>>Hist_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()));
      }
     }
  }

//Calculate Effciency and save histogram 
  Hist_Den->SetYTitle("Entries");
  Hist_Eff->SetYTitle("Efficiency");
  Efficiency_1D(Hist_Den, Hist_Num, Hist_Eff);

  double ymin,ymax;
  if(_type_eff.find("ID")!=string::npos)       { ymin = 0.3; ymax = 1.00;}
  else if(_type_eff.find("TRK")!=string::npos) { ymin = 0.65; ymax = 1.00;}
  else if(_type_eff.find("TRG")!=string::npos) { ymin = 0.4;  ymax = 0.9;}
  Hist_Eff->SetAxisRange(ymin, ymax, "Y");
  Hist_Eff->Write(); 
  // draw comaprions plots
  TCanvas* c1 = new TCanvas("c1", "c1", 700, 500);
  Hist_Den->SetLineColor(2);
  Hist_Den->SetMarkerColor(2);
  Hist_Num->SetLineColor(4);
  Hist_Num->SetMarkerColor(4);
  TLegend* leg = new TLegend(0.4, 0.4, 0.6, 0.6);
  leg->AddEntry(Hist_Den, "Denominator", "p");
  leg->AddEntry(Hist_Num, "Numerator", "p"); 
  Hist_Den->Draw();
  Hist_Num->Draw("same");
  leg->Draw("same");
  // draw eff plot
  TCanvas* c2 = new TCanvas("c2", "c2", 700, 500);
  Hist_Eff->SetName("Eff");
  Hist_Eff->Draw("");
  //draw and save mathced plots
  TCanvas* c3 = new TCanvas("c3", "c3", 700, 500);
  if(_Study_Match){
    c3->cd();
    Hist_Matched_Eff->SetYTitle("Efficiency");
    Efficiency_1D(Hist_Matched_Den, Hist_Matched_Num, Hist_Matched_Eff);
    Hist_Matched_Eff->Write();
    Hist_Matched_Eff->SetName("Eff");
    Hist_Matched_Eff->Draw("");
    //Compare Tag-and-Probe and Matched result
    Compare_Efficiency(Hist_Eff, Hist_Matched_Eff,name.c_str(),year.c_str(),_type_eff.c_str());
  }

  // print comparison and eff plots
  for(int i=0;i<n_format;i++){
    c1->Print(Form("../../plots/Eff/%s/%s_%s_%s%s%s%s", sample.c_str(), sample.c_str(), subname.c_str(),"Comparison_", name.c_str(), year.c_str(),format[i].c_str()));
    c2->Print(Form("../../plots/Eff/%s/%s_%s_%s%s%s%s", sample.c_str(), sample.c_str(), subname.c_str(),"Efficiency_", name.c_str(), year.c_str(),format[i].c_str()));
    if(_Study_Match) 
      c3->Print(Form("../../plots/Eff/%s/%s_%s_%s%s%s%s", sample.c_str(), sample.c_str(), subname.c_str(),"Matched_Efficiency_", name.c_str(), year.c_str(),format[i].c_str()));
  }

  //out the final result in each bin
  if((name.find("ETA")!=string::npos) && (subname.find("ALL")!=string::npos))
     Pinrt_Eff_Result(_type_eff.c_str(),year.c_str(),Hist_Eff,name);

  // clean
  delete Hist_Den; delete Hist_Num; delete Hist_Eff;
  delete c1; delete c2; delete c3;
  delete leg;
}

// handle different eff depends: 1D, steve's plots
void Calculate_ZMuMu_Efficiency_1D(string rootname, string dirname,
				   string denname, string numname, 
                                   string outdir, string output){
  // output name
  char outname[500];
  // define TH1D histogram for denomator and numerator
  TH1D* hist_den; TH1D* hist_num;
   
  TFile* _file = new TFile(rootname.c_str()); 
  hist_den = (TH1D*)_file->Get((dirname + "/" + denname).c_str());
  hist_num = (TH1D*)_file->Get((dirname + "/" + numname).c_str());
    
  //
  // calculate eff
  //
  TH1D* hist_eff;
  if(dirname.find("ETA")!=string::npos)
    hist_eff = new TH1D("eff", "eff", nbins_eta, bins_eta);
  else if(dirname.find("PHI")!=string::npos)
    hist_eff = new TH1D("eff", "eff", nbins_phi, bins_phi);
  else if(dirname.find("PT")!=string::npos)
    hist_eff = new TH1D("eff", "eff", nbins_pt, bins_pt);
  else if(dirname.find("SPD")!=string::npos)
    hist_eff = new TH1D("eff", "eff", nbins_spd, bins_spd);

  hist_eff->SetYTitle("Efficiency");
    
  Efficiency_1D(hist_den, hist_num, hist_eff);
    
  // draw eff plot
  TCanvas* c2 = new TCanvas("c2", "c2", 700, 500);
  hist_eff->Draw("");
    
  // print eff plot
  sprintf(outname, "../../plots/Eff/%s/%s.pdf", outdir.c_str(), output.c_str());
  c2->Print(outname);
  sprintf(outname, "../../plots/Eff/%s/%s.C", outdir.c_str(), output.c_str());
  c2->Print(outname);
  
  // clean
  delete hist_den; delete hist_num;
  delete c2;       delete hist_eff;
  delete _file;
}

// handle different eff depends: 2D
void Calculate_ZMuMu_Efficiency_2D(TChain* ch_den, TChain* ch_num,
                                   TChain* ch_den_match, TChain* ch_num_match,
				   string name, string name1, string sample,
				   string subname, string year,bool _Study_Match,string _type_eff){
  // output name
  char outname[500];
    
  // define TH2D histogram for denomator and numerator
  TH2D* Hist_2D_Den; TH2D* Hist_2D_Num; TH2D* Hist_2D_Eff;
  TH2D* Hist_Matched_Den;  TH2D* Hist_Matched_Num;  TH2D* Hist_Matched_Eff; TH2D* Hist_Diff;
 
  if(name.find("ETA")!=string::npos && name1.find("PHI")!=string::npos){

    Hist_2D_Den = new TH2D(Form("Hist_2D_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_2D_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_phi, bins_phi);
    Hist_2D_Num = new TH2D(Form("Hist_2D_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_2D_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_phi, bins_phi);
    Hist_2D_Eff = new TH2D(Form("Hist_2D_Eff_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_2D_Eff_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_phi, bins_phi);
    Hist_2D_Eff->SetXTitle("Muon #eta");
    Hist_2D_Eff->SetYTitle("Muon #phi");
    ch_den->Draw(Form("probe_PHI:probe_ETA>>Hist_2D_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()));
    ch_num->Draw(Form("probe_PHI:probe_ETA>>Hist_2D_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()));
    if(_Study_Match){
      Hist_Matched_Den = new TH2D(Form("Hist_2D_Matched_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_2D_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_phi, bins_phi);
      Hist_Matched_Num = new TH2D(Form("Hist_2D_Matched_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_2D_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_phi, bins_phi);
      Hist_Matched_Eff = new TH2D(Form("Hist_2D_Matched_Eff_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_Matched_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_phi, bins_phi);
      Hist_Diff = new TH2D(Form("Hist_2D_Diff_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_Diff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta, nbins_phi, bins_phi);
      Hist_Diff->SetXTitle("Muon #eta");
      Hist_Diff->SetYTitle("Muon #phi");
     if(_type_eff.find("TRK")!=string::npos){
        ch_den_match->Draw(Form("mup_PHI:mup_ETA>>Hist_2D_Matched_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()),"mup_Reconstructed==1");
        ch_den_match->Draw(Form("mup_PHI:mup_ETA>>Hist_2D_Matched_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()));
     }
     else{
        ch_den_match->Draw(Form("probe_PHI:probe_ETA>>Hist_2D_Matched_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()));
        ch_num_match->Draw(Form("probe_PHI:probe_ETA>>Hist_2D_Matched_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()));
      }
    } 
  }else if(name.find("ETA")!=string::npos && name1.find("PT")!=string::npos){

     Hist_2D_Den = new TH2D(Form("Hist_2D_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_2D_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
     Hist_2D_Num = new TH2D(Form("Hist_2D_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_2D_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
     Hist_2D_Eff = new TH2D(Form("Hist_2D_Eff_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_2D_Eff_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
     Hist_2D_Eff->SetXTitle("Muon #eta");
     Hist_2D_Eff->SetYTitle("Muon p_{T} (GeV)");
     ch_den->Draw(Form("probe_PT/1000:probe_ETA>>Hist_2D_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()));
     ch_num->Draw(Form("probe_PT/1000:probe_ETA>>Hist_2D_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()));
     if(_Study_Match){
      Hist_Matched_Den = new TH2D(Form("Hist_2D_Matched_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_2D_Matched_Den_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
      Hist_Matched_Num = new TH2D(Form("Hist_2D_Matched_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_2D_Matched_Num_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
      Hist_Matched_Eff = new TH2D(Form("Hist_2D_Matched_Eff_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_Matched_Eff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
      Hist_Diff = new TH2D(Form("Hist_2D_Diff_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()), Form("Hist_Diff_%s_%s_%s",subname.c_str(),name.c_str(),year.c_str()), nbins_eta, bins_eta, nbins_pt, bins_pt);
      Hist_Diff->SetXTitle("Muon #eta");
      Hist_Diff->SetYTitle("Muon p_{T} (GeV)");
     if(_type_eff.find("TRK")!=string::npos){
        ch_den_match->Draw(Form("mup_PT/1000:mup_ETA>>Hist_2D_Matched_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()),"mup_Reconstructed==1");
        ch_den_match->Draw(Form("mup_PT/1000:mup_ETA>>Hist_2D_Matched_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()));
     }
     else{
        ch_den_match->Draw(Form("probe_PT/1000:probe_ETA>>Hist_2D_Matched_Den_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()));
        ch_num_match->Draw(Form("probe_PT/1000:probe_ETA>>Hist_2D_Matched_Num_%s_%s_%s_%s",subname.c_str(),name.c_str(),name1.c_str(),year.c_str()));
      }
    }
  }
    
  //
  // calculate eff
  //
  Efficiency_2D(Hist_2D_Den, Hist_2D_Num, Hist_2D_Eff);
  Hist_2D_Eff->Write(); 
  // draw comaprions plots
  TCanvas* c1 = new TCanvas("c1", "c1", 700, 500);
  Hist_2D_Den->SetLineColor(2);
  Hist_2D_Den->SetMarkerColor(2);
  Hist_2D_Num->SetLineColor(4);
  Hist_2D_Num->SetMarkerColor(4);
  TLegend* leg = new TLegend(0.4, 0.4, 0.6, 0.6);
  leg->AddEntry(Hist_2D_Den, "Denominator", "p");
  leg->AddEntry(Hist_2D_Num, "Numerator", "p");
    
  Hist_2D_Den->Draw("box");
  Hist_2D_Num->Draw("box&&same");
  leg->Draw("same");
    
  // print comparison plots
  sprintf(outname, "../../plots/Eff/%s/%s_%s_%s%s_%s%s.pdf", sample.c_str(), sample.c_str(), subname.c_str(),
	  "Comparison_", name.c_str(), name1.c_str(), year.c_str());
  c1->Print(outname);
  sprintf(outname, "../../plots/Eff/%s/%s_%s_%s%s_%s%s.C", sample.c_str(), sample.c_str(), subname.c_str(),
	  "comparison_", name.c_str(), name1.c_str(), year.c_str());
  c1->Print(outname);
    
  // draw eff plot
  TCanvas* c2 = new TCanvas("c2", "c2", 700, 500);
  c2->cd();
  gStyle->SetPalette(1, 0);
  gStyle->SetPaintTextFormat("0.2f");
  Hist_2D_Eff->Draw("COLZ&&Text");
  //Hist_2D_Eff->Draw("TEXTE");
 
  if(_Study_Match){

    Efficiency_2D(Hist_Matched_Den, Hist_Matched_Num, Hist_Matched_Eff);
    Hist_Matched_Eff->Write();
    Diff_2D(Hist_2D_Eff, Hist_Matched_Eff,Hist_Diff);
    TCanvas* c3 = new TCanvas("c3", "c3", 700, 500);
    c3->cd();
    Hist_Diff->Draw("COLZ&&TEXTE"); 
    c3->Print(Form("../../plots/Eff/%s/%s_%s_%s%s_%s%s.pdf", sample.c_str(), sample.c_str(), subname.c_str(),
          "Diff_", name.c_str(), name1.c_str(), year.c_str()));
  }  
  // print eff plot
  sprintf(outname, "../../plots/Eff/%s/%s_%s_%s%s_%s%s.pdf", sample.c_str(), sample.c_str(), subname.c_str(),
	  "Efficiency_", name.c_str(), name1.c_str(), year.c_str());
  c2->Print(outname);
  sprintf(outname, "../../plots/Eff/%s/%s_%s_%s%s_%s%s.C", sample.c_str(), sample.c_str(), subname.c_str(),
	  "Efficiency_", name.c_str(), name1.c_str(), year.c_str());
  c2->Print(outname);
    
  // clean
  delete Hist_2D_Den; delete Hist_2D_Num;
  delete c1; delete c2;
  delete leg; delete Hist_2D_Eff;
}

// printout eff and ratio plot in one 
void PrintOut_EffRatio(TH1D* data_hist, TH1D* mc_hist, TH1D* ratio_hist, string outputname, string varname){
  // x-title
  string xtitle = "";
  if(varname.find("ETA")!=string::npos)      xtitle = "Muon #eta"; 
  else if(varname.find("PHI")!=string::npos) xtitle = "Muon #phi"; 
  else if(varname.find("PT")!=string::npos)  xtitle = "Muon p_{T} (GeV/#it{c}^{2})"; 
  else if(varname.find("SPD")!=string::npos) xtitle = "nSPD"; 
  else xtitle = "";

  // define TCanvas
  TCanvas* c0 = new TCanvas("c0", "c0", 700, 500);
  ratio_hist->SetLineColor(1);
  ratio_hist->SetLineWidth(2);
  ratio_hist->SetMarkerColor(1);
  ratio_hist->SetXTitle(xtitle.c_str());
  ratio_hist->SetYTitle("Ratio");
  
  ratio_hist->Draw();

  string name_tmp = outputname + ".C"; 
  c0->Print(name_tmp.c_str());
  name_tmp = outputname + ".pdf"; 
  c0->Print(name_tmp.c_str());

  // define TCanvas
  TCanvas* c1 = new TCanvas("c1", "c1", 700, 600);
  // split the canvas into 2 pads
  c1->Divide(1,2,0,0,0) ; 
  // go to pad 2, and draw eff distribution
  c1->cd(2);
  // pad 2 setting
  gPad->SetTopMargin(0);
  gPad->SetLeftMargin(0.12);
  gPad->SetRightMargin(0.045);
  gPad->SetBottomMargin(0.48);
  gPad->SetPad(0.02, 0.02,0.95,0.25);

  ratio_hist->GetXaxis()->SetNdivisions(508);
  ratio_hist->GetXaxis()->SetLabelSize(0.20);
  ratio_hist->GetXaxis()->SetTitleSize(0.22);
  ratio_hist->GetXaxis()->SetTitleOffset(0.85);
  ratio_hist->GetYaxis()->SetLabelSize(0.20);
  ratio_hist->GetYaxis()->SetTitleSize(0.22);
  ratio_hist->GetYaxis()->SetTitleOffset(0.25);
  ratio_hist->GetYaxis()->SetNdivisions(501);
  ratio_hist->SetAxisRange(0.9, 1.1, "Y");

  ratio_hist->Draw();
  // go to pad 1
  c1->cd(1);
  gPad->SetBottomMargin(0);
  gPad->SetLeftMargin(0.12);
  gPad->SetRightMargin(0.045);
  gPad->SetPad(0.02, 0.25, 0.95, 0.97);
  // histograms settings
  data_hist->SetLineColor(4);
  data_hist->SetLineWidth(2);
  data_hist->SetMarkerColor(4);
  mc_hist->SetLineColor(2);
  mc_hist->SetLineWidth(2);
  mc_hist->SetMarkerColor(2);
  data_hist->SetXTitle(xtitle.c_str());
  data_hist->SetAxisRange(0.6, 1.05, "Y");

  // Tlegend
  TLegend* leg = new TLegend(0.2, 0.2, 0.4, 0.4);
  leg->SetLineColor(0);
  leg->SetFillColor(0);
  leg->AddEntry(data_hist, "Data", "epl");
  leg->AddEntry(mc_hist, "MC", "epl");

  data_hist->Draw();
  mc_hist->Draw("same");
  leg->Draw("same");

  // print out
  string name_tmp_C = outputname + "_comp.C"; 
  c1->Print(name_tmp_C.c_str());
  string name_tmp_P = outputname + "_comp.pdf"; 
  c1->Print(name_tmp_P.c_str());

  delete c0;  delete c1;  delete leg;
}

// printout eff and ratio plot in one 
void PrintOut_EffRatio_2D(TH2D* data_hist, TH2D* mc_hist, TH2D* ratio_hist, string outputname, string varname){
  // x-title
  string xtitle = "Muon #eta";
  string ytitle = "Muon #phi";
  if(varname.find("PT")!=string::npos)  ytitle = "Muon p_{T} (GeV/#it{c}^{2})"; 

  // define TCanvas
  TCanvas* c0 = new TCanvas("c0", "c0", 700, 500);
  ratio_hist->SetXTitle(xtitle.c_str());
  ratio_hist->SetYTitle(xtitle.c_str());
  
  ratio_hist->Draw("COLZ");

  string name_tmp = outputname + ".C"; 
  c0->Print(name_tmp.c_str());
  name_tmp = outputname + ".pdf"; 
  c0->Print(name_tmp.c_str());

  // define TCanvas
  TCanvas* c1 = new TCanvas("c1", "c1", 700, 500);

  // histograms settings
  data_hist->SetLineColor(4);
  data_hist->SetLineWidth(2);
  data_hist->SetMarkerColor(4);
  mc_hist->SetLineColor(2);
  mc_hist->SetLineWidth(2);
  mc_hist->SetMarkerColor(2);
  data_hist->SetXTitle(xtitle.c_str());
  data_hist->SetYTitle(ytitle.c_str());

  // Tlegend
  TLegend* leg = new TLegend(0.4, 0.4, 0.6, 0.6);
  leg->SetLineColor(0);
  leg->SetFillColor(0);
  leg->AddEntry(data_hist, "Data", "box");
  leg->AddEntry(mc_hist, "MC", "box");

  data_hist->Draw("BOX");
  mc_hist->Draw("same&&BOX");
  leg->Draw("same");

  // print out
  string name_tmp_C= outputname + "_comp.C"; 
  c1->Print(name_tmp_C.c_str());
  string name_tmp_P = outputname + "_comp.pdf"; 
  c1->Print(name_tmp_P.c_str());

  delete c0;  delete c1;  delete leg;
}

// efficiency ratio calculation
void Calculate_ZMuMu_EffRatio_1D(string data_name, string mc_name, string output_name, string histname, string varname){
  char commands[200];

  // read data histogram
  sprintf(commands, ".x %s", data_name.c_str());
  gROOT->ProcessLine(commands);
  TH1D* data_hist = (TH1D*) gROOT->FindObject(histname.c_str());
  data_hist->SetName("data_eff");

  // read MC histogram
  sprintf(commands, ".x %s", mc_name.c_str());
  gROOT->ProcessLine(commands);
  TH1D* mc_hist = (TH1D*) gROOT->FindObject(histname.c_str());
  mc_hist->SetName("MC_eff");

  // ratio histogram
  TH1D* ratio_hist = (TH1D*) (data_hist->Clone());
  ratio_hist->SetName("eff_ratio");

  // calculate ratio
  Ratio_1D(mc_hist, data_hist, ratio_hist);

  // make ratio plots
  PrintOut_EffRatio(data_hist, mc_hist, ratio_hist, output_name, varname);

  delete data_hist;  delete mc_hist;
  delete ratio_hist;
}

// efficiency ratio calculation
void Calculate_ZMuMu_EffRatio_2D(string data_name, string mc_name, string output_name, string histname, string varname){
  char commands[200];

  // read data histogram
  sprintf(commands, ".x %s", data_name.c_str());
  gROOT->ProcessLine(commands);
  TH2D* data_hist = (TH2D*) gROOT->FindObject(histname.c_str());
  data_hist->SetName("data_eff_2D");

  // read MC histogram
  sprintf(commands, ".x %s", mc_name.c_str());
  gROOT->ProcessLine(commands);
  TH2D* mc_hist = (TH2D*) gROOT->FindObject(histname.c_str());
  mc_hist->SetName("MC_eff_2D");

  // ratio histogram
  TH2D* ratio_hist = (TH2D*) (data_hist->Clone());
  ratio_hist->SetName("eff_ratio_2D");

  // calculate ratio
  Ratio_2D(mc_hist, data_hist, ratio_hist);

  // make ratio plots
  PrintOut_EffRatio_2D(data_hist, mc_hist, ratio_hist, output_name, varname);

  delete data_hist;  delete mc_hist;
  delete ratio_hist;
}

void Calculate_ZMuMu_TotalEfficiency(TH1D* Hist_Trig_Mup, TH1D* Hist_Trk_Mup, TH1D* Hist_ID_Mup, TH1D* Hist_Trig_Mum, TH1D* Hist_Trk_Mum, TH1D* Hist_ID_Mum, TH1D* Hist_Total){

  for(int i = 1; i < Hist_Trig_Mup->GetNbinsX()+1; i++){
    
    Hist_Total->SetBinContent(i,0);
    Hist_Total->SetBinError(i,0);
    double N_Trig_Mup = Hist_Trig_Mup->GetBinContent(i);
    double N_ID_Mup   = Hist_ID_Mup->GetBinContent(i);
    double N_Trk_Mup  = Hist_Trk_Mup->GetBinContent(i);
  
    double N_Trig_Mum = Hist_Trig_Mum->GetBinContent(i);
    double N_ID_Mum   = Hist_ID_Mum->GetBinContent(i);
    double N_Trk_Mum  = Hist_Trk_Mum->GetBinContent(i);

    double E_Trig_Mup = Hist_Trig_Mup->GetBinError(i);
    double E_ID_Mup   = Hist_ID_Mup->GetBinError(i);
    double E_Trk_Mup  = Hist_Trk_Mup->GetBinError(i);

    double E_Trig_Mum = Hist_Trig_Mum->GetBinError(i);
    double E_ID_Mum   = Hist_ID_Mum->GetBinError(i);
    double E_Trk_Mum  = Hist_Trk_Mum->GetBinError(i);


    double N_Total = N_Trk_Mup*N_Trk_Mum*N_ID_Mup*N_ID_Mum*(N_Trig_Mup+N_Trig_Mum-N_Trig_Mup*N_Trig_Mum);
    double E_Part1 = E_Trk_Mup*N_Trk_Mum*N_ID_Mup*N_ID_Mum*(N_Trig_Mup+N_Trig_Mum-N_Trig_Mup*N_Trig_Mum);
    double E_Part2 = N_Trk_Mup*E_Trk_Mum*N_ID_Mup*N_ID_Mum*(N_Trig_Mup+N_Trig_Mum-N_Trig_Mup*N_Trig_Mum);
    double E_Part3 = N_Trk_Mup*N_Trk_Mum*E_ID_Mup*N_ID_Mum*(N_Trig_Mup+N_Trig_Mum-N_Trig_Mup*N_Trig_Mum);
    double E_Part4 = N_Trk_Mup*N_Trk_Mum*N_ID_Mup*E_ID_Mum*(N_Trig_Mup+N_Trig_Mum-N_Trig_Mup*N_Trig_Mum);
    double E_Part5 = N_Trk_Mup*N_Trk_Mum*N_ID_Mup*N_ID_Mum*E_Trig_Mup*(1-N_Trig_Mum);
    double E_Part6 = N_Trk_Mup*N_Trk_Mum*N_ID_Mup*N_ID_Mum*E_Trig_Mum*(1-N_Trig_Mup);
    double E_Total = sqrt(E_Part1*E_Part1+E_Part2*E_Part2+E_Part3*E_Part3+E_Part4*E_Part4+E_Part5*E_Part5+E_Part6*E_Part6);
   
    cout<<N_Total<<"  "<<E_Total<<endl;
    Hist_Total->SetBinContent(i,N_Total); 
    Hist_Total->SetBinError(i,E_Total); 
  }
}

void Sub_1D(TH1D* hist_tag, TH1D* hist_match, TH1D* hist_diff){
  for(int i = 1; i < hist_tag->GetNbinsX()+1; i++){
    hist_diff->SetBinContent(i, 0.);
    hist_diff->SetBinError(i, 0.);

    double n_tag = hist_tag->GetBinContent(i);
    double n_match = hist_match->GetBinContent(i);
    double e_tag = hist_tag->GetBinError(i);
    double e_match = hist_match->GetBinError(i);

    double diff = n_match-n_tag;
    double error =sqrt(e_tag*e_tag+e_match*e_match);

    hist_diff->SetBinContent(i, diff);
    hist_diff->SetBinError(i, error);
  }
}

void Make_Mass_Comparison(TH1D* Hist_Mass_Pass, TH1D* Hist_Mass_Fail, string name_pass, string name_fail,string output){

  TCanvas* cmass = new TCanvas("cmass", "cmass", 700, 500);
  cmass->SetGrid();
  TLegend* leg = new TLegend(0.7,0.7,0.95,0.92);
  leg->SetLineColor(0);
  leg->SetFillColor(0);

  double n_pass = Hist_Mass_Pass->Integral();
  double n_fail = Hist_Mass_Fail->Integral();
  Hist_Mass_Pass -> Scale(1.0/n_pass);
  Hist_Mass_Fail -> Scale(1.0/n_fail);
  
  Hist_Mass_Pass->SetFillColor(2);
  Hist_Mass_Pass->SetLineColor(2);
  Hist_Mass_Pass->SetFillStyle(3004);

  Hist_Mass_Fail->SetFillColor(4);
  Hist_Mass_Fail->SetLineColor(4);
  Hist_Mass_Fail->SetFillStyle(3009);

  leg->AddEntry(Hist_Mass_Pass,name_pass.c_str(),"f");
  leg->AddEntry(Hist_Mass_Fail,name_fail.c_str(),"f");

  Hist_Mass_Pass->SetTitle("");
  Hist_Mass_Pass->SetXTitle("dimuon invariant mass [GeV]");
  Hist_Mass_Pass->SetYTitle("Entries");
  Hist_Mass_Pass->SetAxisRange(0.0,0.03, "Y");
  //Hist_Mass_Pass->SetAxisRange(0.0,0.15, "Y");
 
  Hist_Mass_Pass->Draw("hist same");
  Hist_Mass_Fail->Draw("hist same");
  leg->Draw("same");

  for(int i=0;i<n_format;i++){
    cmass->Print(Form("%s%s",output.c_str(),format[i].c_str()));
   }
 
  delete cmass;
  delete leg;
} 

void TH1D_Bias_Calculate(TH1D* Hist_TT, TH1D* Hist_NoTT, string _vartype,string _chargetype){

  // define TCanvas
  TCanvas* c_com = new TCanvas("c_com", "c_com", 700, 500);
  gROOT->ForceStyle();
  gStyle->SetOptStat(0);
  c_com->SetGrid();
  Hist_TT->SetYTitle("Muon Tracking Efficiency");
  if(_vartype.find("ETA")!=string::npos) Hist_TT->SetXTitle("#eta");
  if(_vartype.find("PT")!=string::npos) Hist_TT->SetXTitle("p_{T}");
  Hist_TT->SetAxisRange(0.7,1.00, "Y");
  Hist_TT->SetLineColor(4);
  Hist_TT->SetLineWidth(2);
  Hist_TT->SetMarkerColor(4);
  Hist_TT->SetMarkerStyle(23);
  Hist_NoTT->SetLineColor(2);
  Hist_NoTT->SetLineWidth(2);
  Hist_NoTT->SetMarkerColor(2);
  Hist_NoTT->SetMarkerStyle(26);
  TLegend* leg = new TLegend(0.6, 0.2, 0.85, 0.45);
  leg->SetLineColor(0);
  leg->SetFillColor(0);
  leg->AddEntry(Hist_TT, "With MuonTT Track", "epl");
  leg->AddEntry(Hist_NoTT, "Without Requirement", "epl");
  Hist_TT->Draw();
  Hist_NoTT->Draw("same");
  leg->Draw("same");
  for(int i=0;i<n_format;i++){
    c_com->Print(Form("../../plots/Eff/MC_2016TRKEFF/TRK_Bias_Com_%s_%s%s",_vartype.c_str(),_chargetype.c_str(),format[i].c_str()));
   }
 
  TCanvas* c_result = new TCanvas("c_result", "c_result", 700, 500);
  gROOT->ForceStyle();
  gStyle->SetOptStat(0);
  c_result->SetGrid();
  TH1D* Hist_Ratio = (TH1D*)(Hist_TT->Clone());
  Ratio_1D(Hist_TT, Hist_NoTT, Hist_Ratio);
  Hist_Ratio->SetName(Form("Hist_Ratio_%s_%s",_vartype.c_str(),_chargetype.c_str()));  
  Hist_Ratio->SetYTitle("Bias Correction");
  if(_vartype.find("ETA")!=string::npos) Hist_Ratio->SetXTitle("#eta");
  if(_vartype.find("PT")!=string::npos) Hist_Ratio->SetXTitle("p_{T}");
  Hist_Ratio->SetAxisRange(0.7,1.0, "Y");
  Hist_Ratio->Draw(); 
  // print out
  for(int i=0;i<n_format;i++){
    c_result->Print(Form("../../plots/Eff/MC_2016TRKEFF/TRK_Bias_%s_%s%s",_vartype.c_str(),_chargetype.c_str(),format[i].c_str()));
   }
  delete c_result; delete c_com; 
}


void TH2D_Bias_Calculate(TH2D* Hist_TT, TH2D* Hist_NoTT, string _vartype){

  // define TCanvas
  TCanvas* c = new TCanvas("c", "c", 700, 600);
  gROOT->ForceStyle();
  gStyle->SetOptStat(0);
  TH2D* Hist_Ratio = (TH2D*)(Hist_TT->Clone());
  Ratio_2D(Hist_TT, Hist_NoTT, Hist_Ratio);
  Hist_Ratio->SetName(Form("Hist_Bias_2D_%s",_vartype.c_str()));
  Hist_Ratio->SetXTitle("#eta");
  Hist_Ratio->SetYTitle("p_{T}");
  gStyle->SetPalette(1, 0);
  Hist_Ratio->Draw("COLZ&&Text");
  // print out
  for(int i=0;i<n_format;i++){
    c->Print(Form("../../plots/Eff/MC_2016TRKEFF/TH2D_TRK_Bias_%s%s",_vartype.c_str(),format[i].c_str()));
   }
  delete c; 
}


void TRK_Eff_Bias_Correction_1D(TTree* ch,string var_type){

  TH1D* Hist_Mup_Den_tt;   TH1D* Hist_Mup_Num_tt;   TH1D* Hist_Mup_Eff_tt;
  TH1D* Hist_Mup_Den_nott; TH1D* Hist_Mup_Num_nott; TH1D* Hist_Mup_Eff_nott;
  TH1D* Hist_Mum_Den_tt;   TH1D* Hist_Mum_Num_tt;   TH1D* Hist_Mum_Eff_tt;
  TH1D* Hist_Mum_Den_nott; TH1D* Hist_Mum_Num_nott; TH1D* Hist_Mum_Eff_nott;
  
  if(var_type.find("ETA")!=string::npos){
    Hist_Mup_Den_tt = new TH1D(Form("Hist_Mup_Den_tt_%s",var_type.c_str()), 
                               Form("Hist_Mup_Den_tt_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mup_Num_tt = new TH1D(Form("Hist_Mup_Num_tt_%s",var_type.c_str()),       
                               Form("Hist_Mup_Num_tt_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mup_Eff_tt = new TH1D(Form("Hist_Mup_Eff_tt_%s",var_type.c_str()),
                               Form("Hist_Mup_Eff_tt_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mup_Den_tt->SetXTitle("Muon #eta");
    Hist_Mum_Den_tt = new TH1D(Form("Hist_Mum_Den_tt_%s",var_type.c_str()),
                               Form("Hist_Mum_Den_tt_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mum_Num_tt = new TH1D(Form("Hist_Mum_Num_tt_%s",var_type.c_str()),
                               Form("Hist_Mum_Num_tt_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mum_Eff_tt = new TH1D(Form("Hist_Mum_Eff_tt_%s",var_type.c_str()),
                               Form("Hist_Mum_Eff_tt_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mum_Den_tt->SetXTitle("Muon #eta");
   
    ch->Draw(Form("mup_ETA>>Hist_Mup_Den_tt_%s",var_type.c_str()),"mup_MatchTT==1");
    ch->Draw(Form("mup_ETA>>Hist_Mup_Num_tt_%s",var_type.c_str()),"mup_MatchTT==1 && mup_Reconstructed==1");
    ch->Draw(Form("mum_ETA>>Hist_Mum_Den_tt_%s",var_type.c_str()),"mum_MatchTT==1");
    ch->Draw(Form("mum_ETA>>Hist_Mum_Num_tt_%s",var_type.c_str()),"mum_MatchTT==1 && mum_Reconstructed==1");

    Hist_Mup_Den_nott = new TH1D(Form("Hist_Mup_Den_nott_%s",var_type.c_str()),
                                 Form("Hist_Mup_Den_nott_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mup_Num_nott = new TH1D(Form("Hist_Mup_Num_nott_%s",var_type.c_str()),
                                 Form("Hist_Mup_Num_nott_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mup_Eff_nott = new TH1D(Form("Hist_Mup_Eff_nott_%s",var_type.c_str()),
                                 Form("Hist_Mup_Eff_nott_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mup_Den_nott->SetXTitle("Muon #eta");

    Hist_Mum_Den_nott = new TH1D(Form("Hist_Mum_Den_nott_%s",var_type.c_str()),
                                 Form("Hist_Mum_Den_nott_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mum_Num_nott = new TH1D(Form("Hist_Mum_Num_nott_%s",var_type.c_str()),
                                 Form("Hist_Mum_Num_nott_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mum_Eff_nott = new TH1D(Form("Hist_Mum_Eff_nott_%s",var_type.c_str()),
                                 Form("Hist_Mum_Eff_nott_%s",var_type.c_str()), nbins_eta, bins_eta);
    Hist_Mum_Den_nott->SetXTitle("Muon #eta");
    ch->Draw(Form("mup_ETA>>Hist_Mup_Den_nott_%s",var_type.c_str()),"mup_MatchTT==0");
    ch->Draw(Form("mup_ETA>>Hist_Mup_Num_nott_%s",var_type.c_str()),"mup_MatchTT==0 && mup_Reconstructed==1");
    ch->Draw(Form("mum_ETA>>Hist_Mum_Den_nott_%s",var_type.c_str()),"mum_MatchTT==0");
    ch->Draw(Form("mum_ETA>>Hist_Mum_Num_nott_%s",var_type.c_str()),"mum_MatchTT==0 && mum_Reconstructed==1"); 
  }
   else if(var_type.find("PT")!=string::npos){
    Hist_Mup_Den_tt = new TH1D(Form("Hist_Mup_Den_tt_%s",var_type.c_str()), 
                               Form("Hist_Mup_Den_tt_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mup_Num_tt = new TH1D(Form("Hist_Mup_Num_tt_%s",var_type.c_str()),       
                               Form("Hist_Mup_Num_tt_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mup_Eff_tt = new TH1D(Form("Hist_Mup_Eff_tt_%s",var_type.c_str()), 
                               Form("Hist_Mup_Eff_tt_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mup_Den_tt->SetXTitle("Muon p_{T}");

    Hist_Mum_Den_tt = new TH1D(Form("Hist_Mum_Den_tt_%s",var_type.c_str()),
                               Form("Hist_Mum_Den_tt_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mum_Num_tt = new TH1D(Form("Hist_Mum_Num_tt_%s",var_type.c_str()),
                               Form("Hist_Mum_Num_tt_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mum_Eff_tt = new TH1D(Form("Hist_Mum_Eff_tt_%s",var_type.c_str()),
                               Form("Hist_Mum_Eff_tt_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mum_Den_tt->SetXTitle("Muon p_{T}");
    ch->Draw(Form("mup_TRUEPT/1000.>>Hist_Mup_Den_tt_%s",var_type.c_str()),"mup_MatchTT==1");
    ch->Draw(Form("mup_TRUEPT/1000.>>Hist_Mup_Num_tt_%s",var_type.c_str()),"mup_MatchTT==1 && mup_Reconstructed==1");
    ch->Draw(Form("mum_TRUEPT/1000.>>Hist_Mum_Den_tt_%s",var_type.c_str()),"mum_MatchTT==1");
    ch->Draw(Form("mum_TRUEPT/1000.>>Hist_Mum_Num_tt_%s",var_type.c_str()),"mum_MatchTT==1 && mum_Reconstructed==1"); 

    Hist_Mup_Den_nott = new TH1D(Form("Hist_Mup_Den_nott_%s",var_type.c_str()),
                                 Form("Hist_Mup_Den_nott_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mup_Num_nott = new TH1D(Form("Hist_Mup_Num_nott_%s",var_type.c_str()),
                                 Form("Hist_Mup_Num_nott_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mup_Eff_nott = new TH1D(Form("Hist_Mup_Eff_nott_%s",var_type.c_str()),
                                 Form("Hist_Mup_Eff_nott_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mup_Den_nott->SetXTitle("Muon p_{T}");
    Hist_Mum_Den_nott = new TH1D(Form("Hist_Mum_Den_nott_%s",var_type.c_str()),
                                 Form("Hist_Mum_Den_nott_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mum_Num_nott = new TH1D(Form("Hist_Mum_Num_nott_%s",var_type.c_str()),
                                 Form("Hist_Mum_Num_nott_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mum_Eff_nott = new TH1D(Form("Hist_Mum_Eff_nott_%s",var_type.c_str()),
                                 Form("Hist_Mum_Eff_nott_%s",var_type.c_str()), nbins_pt, bins_pt);
    Hist_Mum_Den_nott->SetXTitle("Muon p_{T}");

    ch->Draw(Form("mup_TRUEPT/1000.>>Hist_Mup_Den_nott_%s",var_type.c_str()),"mup_MatchTT==0");
    ch->Draw(Form("mup_TRUEPT/1000.>>Hist_Mup_Num_nott_%s",var_type.c_str()),"mup_MatchTT==0 && mup_Reconstructed==1");
    ch->Draw(Form("mum_TRUEPT/1000.>>Hist_Mum_Den_nott_%s",var_type.c_str()),"mum_MatchTT==0");
    ch->Draw(Form("mum_TRUEPT/1000.>>Hist_Mum_Num_nott_%s",var_type.c_str()),"mum_MatchTT==0 && mum_Reconstructed==1");
  }

  Efficiency_1D(Hist_Mup_Den_tt,  Hist_Mup_Num_tt,  Hist_Mup_Eff_tt);
  Efficiency_1D(Hist_Mup_Den_nott,Hist_Mup_Num_nott,Hist_Mup_Eff_nott);
  TH1D_Bias_Calculate(Hist_Mup_Eff_tt,Hist_Mup_Eff_nott,var_type.c_str(),"Mup");
  Efficiency_1D(Hist_Mum_Den_tt,  Hist_Mum_Num_tt,  Hist_Mum_Eff_tt);
  Efficiency_1D(Hist_Mum_Den_nott,Hist_Mum_Num_nott,Hist_Mum_Eff_nott);
  TH1D_Bias_Calculate(Hist_Mum_Eff_tt,Hist_Mum_Eff_nott,var_type.c_str(),"Mum");
}

void TRK_Eff_Bias_Correction_2D(TTree* ch){


  TH2D *Hist_2D_TT_Den = new TH2D("Hist_2D_Den_TT_ETA_PT","Hist_2D_Den_TT_ETA_PT",nbins_eta, bins_eta,nbins_pt, bins_pt);
  TH2D *Hist_2D_TT_Num = new TH2D("Hist_2D_Num_TT_ETA_PT","Hist_2D_Num_TT_ETA_PT",nbins_eta, bins_eta,nbins_pt, bins_pt);
  TH2D *Hist_2D_TT_Eff = new TH2D("Hist_2D_Eff_TT_ETA_PT","Hist_2D_Eff_TT_ETA_PT",nbins_eta, bins_eta,nbins_pt, bins_pt);
  Hist_2D_TT_Den->SetXTitle("Muon #eta");
  Hist_2D_TT_Den->SetXTitle("Muon p_{T} (GeV)");

  TH2D *Hist_2D_NoTT_Den = new TH2D("Hist_2D_Den_NoTT_ETA_PT","Hist_2D_Den_NoTT_ETA_PT",nbins_eta, bins_eta,nbins_pt, bins_pt);
  TH2D *Hist_2D_NoTT_Num = new TH2D("Hist_2D_Num_NoTT_ETA_PT","Hist_2D_Num_NoTT_ETA_PT",nbins_eta, bins_eta,nbins_pt, bins_pt);
  TH2D *Hist_2D_NoTT_Eff = new TH2D("Hist_2D_Eff_NoTT_ETA_PT","Hist_2D_Eff_NoTT_ETA_PT",nbins_eta, bins_eta,nbins_pt, bins_pt);
  Hist_2D_NoTT_Den->SetXTitle("Muon #eta");
  Hist_2D_NoTT_Den->SetXTitle("Muon p_{T} (GeV)");

  ch->Draw("mup_TRUEPT/1000:mup_ETA>>Hist_2D_Den_TT_ETA_PT","mup_MatchTT==1");
  ch->Draw("mup_TRUEPT/1000:mup_ETA>>Hist_2D_Num_TT_ETA_PT","mup_MatchTT==1 && mup_Reconstructed==1");

  ch->Draw("mup_TRUEPT/1000:mup_ETA>>Hist_2D_Den_NoTT_ETA_PT","mup_MatchTT==0");
  ch->Draw("mup_TRUEPT/1000:mup_ETA>>Hist_2D_Num_NoTT_ETA_PT","mup_MatchTT==0 && mup_Reconstructed==1");
  Efficiency_2D(Hist_2D_TT_Den, Hist_2D_TT_Num, Hist_2D_TT_Eff);
  Efficiency_2D(Hist_2D_NoTT_Den, Hist_2D_NoTT_Num, Hist_2D_NoTT_Eff);

  TH2D_Bias_Calculate(Hist_2D_TT_Eff,Hist_2D_NoTT_Eff,"ETA_PT");
}

void Compare_Year_Efficiency(TH1D* Hist_2016, TH1D* Hist_2017, TH1D* Hist_2018, string _vartype, string _efftype){

  string xtitle = "";
  if(_vartype.find("ETA")!=string::npos)      xtitle = "#eta";
  else if(_vartype.find("PHI")!=string::npos) xtitle = "#phi";
  else if(_vartype.find("PT")!=string::npos)  xtitle = "p_{T} (GeV/#it{c}^{2})";
  else if(_vartype.find("SPD")!=string::npos) xtitle = "nSPD";
  else xtitle = "";

  //y range
  double y_min,y_max;
  if(_efftype.find("ID")!=string::npos)       { y_min = 0.3; y_max = 1.00;}
  else if(_efftype.find("TRK")!=string::npos) { y_min = 0.5; y_max = 1.00;}
  else if(_efftype.find("TRG")!=string::npos) { y_min = 0.4; y_max = 0.9;}
  else if(_efftype.find("L0_Check")!=string::npos) { y_min = 0.4; y_max = 1.01;}
  else if(_efftype.find("HLT1_Check")!=string::npos) { y_min = 0.6; y_max = 1.01;}
  else if(_efftype.find("HLT2_Check")!=string::npos) { y_min = 0.95; y_max = 1.01;}
  else if(_efftype.find("Matching")!=string::npos) { y_min = 0.8; y_max = 1.01;}
 // define TCanvas
  TCanvas* Can_Year = new TCanvas("Can_Year", "Can_Year", 700, 600);
//  gROOT->ForceStyle();
//  gStyle->SetOptStat(0);
  Can_Year->SetGrid();
  // histograms settings
  Hist_2016->SetLineColor(4);
  Hist_2016->SetLineWidth(2);
  Hist_2016->SetMarkerColor(4);
  Hist_2016->SetMarkerStyle(23);

  Hist_2017->SetLineColor(2);
  Hist_2017->SetLineWidth(2);
  Hist_2017->SetMarkerColor(2);
  Hist_2017->SetMarkerStyle(26);

  Hist_2016->SetYTitle(Form("Muon %s Efficiency",_efftype.c_str()));
  Hist_2016->SetXTitle(xtitle.c_str());
  Hist_2016->SetAxisRange(y_min, y_max, "Y");
  Hist_2016->GetYaxis()->SetLabelSize(0.05);
  Hist_2016->GetYaxis()->SetTitleOffset(1.24);
  // Tlegend
  TLegend* leg = new TLegend(0.2, 0.2, 0.4, 0.4);
  leg->SetLineColor(0);
  leg->SetFillColor(0);
  leg->AddEntry(Hist_2016, "2016", "epl");
  leg->AddEntry(Hist_2017, "2017", "epl");
  leg->AddEntry(Hist_2018, "2018", "epl");

  Hist_2016->Draw();
  Hist_2017->Draw("same");
  Hist_2018->Draw("same");
  leg->Draw("same");
   // print out
  for(int i=0;i<1;i++){
  //for(int i=0;i<n_format;i++){
    Can_Year->Print(Form("../../plots/Eff/EFF_Year/Eff_%s_%s%s",_efftype.c_str(),_vartype.c_str(),format[i].c_str()));
   }
  delete Can_Year;  delete leg;
}

// handle different eff depends: 1D
void Calculate_Track_Matching_Efficiency_1D(TChain* ch,string name, string sample,string year){

  // define TH1D histogram for denomator and numerator
  TH1D* Hist_Den_Mup; TH1D* Hist_Num_Mup; TH1D* Hist_Eff_Mup;
  TH1D* Hist_Den_Mum; TH1D* Hist_Num_Mum; TH1D* Hist_Eff_Mum;

  if(name.find("ETA")!=string::npos){
    Hist_Den_Mup = new TH1D(Form("Hist_Inver_Mup_Den_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mup_Den_%s_%s",name.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_Num_Mup = new TH1D(Form("Hist_Inver_Mup_Num_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mup_Num_%s_%s",name.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_Eff_Mup = new TH1D(Form("Hist_Inver_Mup_Eff_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mup_Eff_%s_%s",name.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_Den_Mup->SetXTitle("Muon #eta");
    ch->Draw(Form(" muplus_ETA>>Hist_Inver_Mup_Den_%s_%s",name.c_str(),year.c_str()),"muplus_MuonTT_DeltaR<0.1&&muplus_MuonTT_PT>10000&&muplus_MuonTT_M>40000&&(muplus_MuonTT_VtxChi2*1.0/muplus_MuonTT_VtxNDoF)<5&&abs(muplus_ID)==13&&muplus_MuonTT_ExistTT==1&&muminus_PT>20000&&muminus_ETA>2.0&&muminus_ETA<4.5&&abs(muminus_ID)==13&&muplus_PT>10000");
    ch->Draw(Form(" muplus_ETA>>Hist_Inver_Mup_Num_%s_%s",name.c_str(),year.c_str()),"muplus_MuonTT_DeltaR<0.1&&muplus_MuonTT_PT>10000&&muplus_MuonTT_M>40000&&(muplus_MuonTT_VtxChi2*1.0/muplus_MuonTT_VtxNDoF)<5&&abs(muplus_ID)==13&&muplus_MuonTT_ExistTT==1&&muminus_PT>20000&&muminus_ETA>2.0&&muminus_ETA<4.5&&abs(muminus_ID)==13&&muplus_PT>10000&&muplus_MuonTT_AssocZM==1");

    Hist_Den_Mum = new TH1D(Form("Hist_Inver_Mum_Den_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mum_Den_%s_%s",name.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_Num_Mum = new TH1D(Form("Hist_Inver_Mum_Num_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mum_Num_%s_%s",name.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_Eff_Mum = new TH1D(Form("Hist_Inver_Mum_Eff_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mum_Eff_%s_%s",name.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_Eff_Mum->SetXTitle("Muon #eta");
    ch->Draw(Form(" muminus_ETA>>Hist_Inver_Mum_Den_%s_%s",name.c_str(),year.c_str()),"muminus_MuonTT_DeltaR<0.1&&muminus_MuonTT_PT>10000&&muminus_MuonTT_M>40000&&(muminus_MuonTT_VtxChi2*1.0/muminus_MuonTT_VtxNDoF)<5&&abs(muminus_ID)==13&&muminus_MuonTT_ExistTT==1&&muplus_PT>20000&&muplus_ETA>2.0&&muplus_ETA<4.5&&abs(muplus_ID)==13&&muminus_PT>10000");
    ch->Draw(Form(" muminus_ETA>>Hist_Inver_Mum_Num_%s_%s",name.c_str(),year.c_str()),"muminus_MuonTT_DeltaR<0.1&&muminus_MuonTT_PT>10000&&muminus_MuonTT_M>40000&&(muminus_MuonTT_VtxChi2*1.0/muminus_MuonTT_VtxNDoF)<5&&abs(muminus_ID)==13&&muminus_MuonTT_ExistTT==1&&muplus_PT>20000&&muplus_ETA>2.0&&muplus_ETA<4.5&&abs(muplus_ID)==13&&muminus_PT>10000&&muminus_MuonTT_AssocZM==1");
  }else if(name.find("PT")!=string::npos){

    Hist_Den_Mup = new TH1D(Form("Hist_Inver_Mup_Den_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mup_Den_%s_%s",name.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_Num_Mup = new TH1D(Form("Hist_Inver_Mup_Num_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mup_Num_%s_%s",name.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_Eff_Mup = new TH1D(Form("Hist_Inver_Mup_Eff_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mup_Eff_%s_%s",name.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_Eff_Mup->SetXTitle("Muon p_{T} (GeV)");
    ch->Draw(Form(" muplus_PT/1000.>>Hist_Inver_Mup_Den_%s_%s",name.c_str(),year.c_str()),"muplus_MuonTT_ExistTT==1");
    ch->Draw(Form(" muplus_PT/1000.>>Hist_Inver_Mup_Num_%s_%s",name.c_str(),year.c_str()),"muplus_MuonTT_ExistTT==1&&muplus_MuonTT_AssocZM==1");
    
    Hist_Den_Mum = new TH1D(Form("Hist_Inver_Mum_Den_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mum_Den_%s_%s",name.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_Num_Mum = new TH1D(Form("Hist_Inver_Mum_Num_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mum_Num_%s_%s",name.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_Eff_Mum = new TH1D(Form("Hist_Inver_Mum_Eff_%s_%s",name.c_str(),year.c_str()), Form("Hist_Inver_Mum_Eff_%s_%s",name.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_Den_Mum->SetXTitle("Muon p_{T} (GeV)");
    ch->Draw(Form(" muminus_PT/1000.>>Hist_Inver_Mum_Den_%s_%s",name.c_str(),year.c_str()),"muminus_MuonTT_ExistTT==1");
    ch->Draw(Form(" muminus_PT/1000.>>Hist_Inver_Mum_Num_%s_%s",name.c_str(),year.c_str()),"muminus_MuonTT_ExistTT==1&&muminus_MuonTT_AssocZM==1");
  }
  //Calculate Effciency and save histogram 
  Hist_Den_Mup->SetYTitle("Entries");
  Hist_Eff_Mup->SetYTitle("Efficiency");
  Efficiency_1D(Hist_Den_Mup, Hist_Num_Mup, Hist_Eff_Mup);
  Hist_Eff_Mup->SetAxisRange(0.60, 1.0, "Y");
  Hist_Eff_Mup->Write();

  Hist_Den_Mum->SetYTitle("Entries");
  Hist_Eff_Mum->SetYTitle("Efficiency");
  Efficiency_1D(Hist_Den_Mum, Hist_Num_Mum, Hist_Eff_Mum);
  Hist_Eff_Mum->SetAxisRange(0.60, 1.0, "Y");
  Hist_Eff_Mum->Write();
  // draw eff plot
  TCanvas* c_com = new TCanvas("c_com", "c_com", 700, 500);
  Hist_Eff_Mup->SetLineColor(4);
  Hist_Eff_Mup->SetLineWidth(2);
  Hist_Eff_Mup->SetMarkerColor(4);
  Hist_Eff_Mup->SetMarkerStyle(23);

  Hist_Eff_Mum->SetLineColor(2);
  Hist_Eff_Mum->SetLineWidth(2);
  Hist_Eff_Mum->SetMarkerColor(2);
  Hist_Eff_Mum->SetMarkerStyle(26);

  Hist_Eff_Mup->SetYTitle("Muon Matching Efficiency");
//  Hist_Eff_Mup->SetXTitle("#eta");
  Hist_Eff_Mup->SetAxisRange(0.60, 1.0, "Y");
  Hist_Eff_Mup->GetYaxis()->SetLabelSize(0.05);
  // Tlegend
  TLegend* leg = new TLegend(0.2, 0.2, 0.35, 0.35);
  leg->SetLineColor(0);
  leg->SetFillColor(0);
  leg->AddEntry(Hist_Eff_Mup, "#mu^{+}", "epl");
  leg->AddEntry(Hist_Eff_Mum, "#mu^{-}", "epl");

  Hist_Eff_Mup->Draw("");
  Hist_Eff_Mum->Draw("same");
  leg->Draw("same");
  // print comparison and eff plots
  for(int i=0;i<n_format;i++){
    c_com->Print(Form("../../plots/Eff/%s/%s_%s_%s%s%s", sample.c_str(), sample.c_str(), "Inver_Efficiency", name.c_str(), year.c_str(),format[i].c_str()));
   }

  // clean
  delete Hist_Den_Mup; delete Hist_Num_Mup; delete Hist_Eff_Mup;
  delete Hist_Den_Mum; delete Hist_Num_Mum; delete Hist_Eff_Mum;
  delete c_com; 
}

void Calculate_Track_Matching_Efficiency_2D(TChain* ch,string sample,string year){

  // define TH1D histogram for denomator and numerator
  TH2D* Hist_Den_Mup; TH2D* Hist_Num_Mup; TH2D* Hist_Eff_Mup;
  TH2D* Hist_Den_Mum; TH2D* Hist_Num_Mum; TH2D* Hist_Eff_Mum;

  Hist_Den_Mup = new TH2D(Form("Hist_Inver_Mup_Den_PT_ETA_%s",year.c_str()), Form("Hist_Inver_Mup_Den_PT_ETA_%s",year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
  Hist_Num_Mup = new TH2D(Form("Hist_Inver_Mup_Num_PT_ETA_%s",year.c_str()), Form("Hist_Inver_Mup_Num_PT_ETA_%s",year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
  Hist_Eff_Mup = new TH2D(Form("Hist_Inver_Mup_Eff_PT_ETA_%s",year.c_str()), Form("Hist_Inver_Mup_Eff_PT_ETA_%s",year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
  Hist_Eff_Mup->SetXTitle("Muon #eta");
  Hist_Eff_Mup->SetYTitle("Muon p_{T} (GeV)"); 
  ch->Draw(Form("muplus_PT/1000.:muplus_ETA>>Hist_Inver_Mup_Den_PT_ETA_%s",year.c_str()),"muplus_MuonTT_DeltaR<0.1&&muplus_MuonTT_PT>10000&&muplus_MuonTT_M>40000&&(muplus_MuonTT_VtxChi2*1.0/muplus_MuonTT_VtxNDoF)<5&&abs(muplus_ID)==13&&muplus_MuonTT_ExistTT==1&&muminus_PT>20000&&muminus_ETA>2.0&&muminus_ETA<4.5&&abs(muminus_ID)==13&&muplus_PT>10000");
  ch->Draw(Form("muplus_PT/1000.:muplus_ETA>>Hist_Inver_Mup_Num_PT_ETA_%s",year.c_str()),"muplus_MuonTT_DeltaR<0.1&&muplus_MuonTT_PT>10000&&muplus_MuonTT_M>40000&&(muplus_MuonTT_VtxChi2*1.0/muplus_MuonTT_VtxNDoF)<5&&abs(muplus_ID)==13&&muplus_MuonTT_ExistTT==1&&muminus_PT>20000&&muminus_ETA>2.0&&muminus_ETA<4.5&&abs(muminus_ID)==13&&muplus_PT>10000&&muplus_MuonTT_AssocZM==1");

  Hist_Den_Mum = new TH2D(Form("Hist_Inver_Mum_Den_PT_ETA_%s",year.c_str()), Form("Hist_Inver_Mum_Den_PT_ETA_%s",year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
  Hist_Num_Mum = new TH2D(Form("Hist_Inver_Mum_Num_PT_ETA_%s",year.c_str()), Form("Hist_Inver_Mum_Num_PT_ETA_%s",year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
  Hist_Eff_Mum = new TH2D(Form("Hist_Inver_Mum_Eff_PT_ETA_%s",year.c_str()), Form("Hist_Inver_Mum_Eff_PT_ETA_%s",year.c_str()), nbins_eta, bins_eta,nbins_pt, bins_pt);
  Hist_Eff_Mum->SetXTitle("Muon #eta");
  Hist_Eff_Mum->SetYTitle("Muon p_{T} (GeV)");
  ch->Draw(Form("muminus_PT/1000.:muminus_ETA>>Hist_Inver_Mum_Den_PT_ETA_%s",year.c_str()),"muminus_MuonTT_DeltaR<0.1&&muminus_MuonTT_PT>10000&&muminus_MuonTT_M>40000&&(muminus_MuonTT_VtxChi2*1.0/muminus_MuonTT_VtxNDoF)<5&&abs(muminus_ID)==13&&muminus_MuonTT_ExistTT==1&&muplus_PT>20000&&muplus_ETA>2.0&&muplus_ETA<4.5&&abs(muplus_ID)==13&&muminus_PT>10000");
  ch->Draw(Form("muminus_PT/1000.:muminus_ETA>>Hist_Inver_Mum_Num_PT_ETA_%s",year.c_str()),"muminus_MuonTT_DeltaR<0.1&&muminus_MuonTT_PT>10000&&muminus_MuonTT_M>40000&&(muminus_MuonTT_VtxChi2*1.0/muminus_MuonTT_VtxNDoF)<5&&abs(muminus_ID)==13&&muminus_MuonTT_ExistTT==1&&muplus_PT>20000&&muplus_ETA>2.0&&muplus_ETA<4.5&&abs(muplus_ID)==13&&muminus_PT>10000&&muminus_MuonTT_AssocZM==1");

  Efficiency_2D(Hist_Den_Mup, Hist_Num_Mup, Hist_Eff_Mup);
  Hist_Eff_Mup->Write();
  Efficiency_2D(Hist_Den_Mum, Hist_Num_Mum, Hist_Eff_Mum);
  Hist_Eff_Mum->Write();

  TCanvas* C_Mup = new TCanvas("C_Mup", "C_Mup", 700, 500);
  gStyle->SetPalette(1, 0);
  Hist_Eff_Mup->Draw("COLZ&&Text");

  TCanvas* C_Mum = new TCanvas("C_Mum", "C_Mum", 700, 500);
  gStyle->SetPalette(1, 0);
  Hist_Eff_Mum->Draw("COLZ&&Text");

  C_Mup->Print(Form("../../plots/Eff/%s/Mup_%s_%s_%s%s%s", sample.c_str(), sample.c_str(), "Inver_Efficiency", "PT_ETA", year.c_str(),".pdf"));
  C_Mum->Print(Form("../../plots/Eff/%s/Mum_%s_%s_%s%s%s", sample.c_str(), sample.c_str(), "Inver_Efficiency", "PT_ETA", year.c_str(),".pdf"));

  // clean
  delete Hist_Den_Mup; delete Hist_Num_Mup; delete Hist_Eff_Mup;
  delete Hist_Den_Mum; delete Hist_Num_Mum; delete Hist_Eff_Mum;
  delete C_Mup; delete C_Mum;
}
void Double_Check_Trigger_Efficiency_1D(TChain *ch_den,string var_type, string outdir, string subname, string year){

  TH1D* Hist_Den; TH1D* Hist_L0Num; TH1D* Hist_HLT1Num; TH1D* Hist_HLT2Num;
  TH1D* Hist_L0Eff; TH1D* Hist_HLT1Eff; TH1D* Hist_HLT2Eff;
  if(var_type.find("ETA")!=string::npos){
    Hist_Den = new TH1D(Form("Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_L0Num = new TH1D(Form("Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_HLT1Num = new TH1D(Form("Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_HLT2Num = new TH1D(Form("Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_L0Eff = new TH1D(Form("Hist_L0Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_L0Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_HLT1Eff = new TH1D(Form("Hist_HLT1Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT1Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_HLT2Eff = new TH1D(Form("Hist_HLT2Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT2Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_eta, bins_eta);
    Hist_Den->SetXTitle("Muon #eta");

    ch_den->Draw(Form("probe_ETA>>Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()));
    ch_den->Draw(Form("probe_ETA>>Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_L0MuonEWDecision_TOS==1");
    ch_den->Draw(Form("probe_ETA>>Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_Hlt1SingleMuonHighPTDecision_TOS==1&&probe_L0MuonEWDecision_TOS==1");
    ch_den->Draw(Form("probe_ETA>>Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_Hlt2SingleMuonHighPTDecision_TOS==1&&probe_L0MuonEWDecision_TOS==1&&probe_Hlt1SingleMuonHighPTDecision_TOS==1");
  }else if(var_type.find("PT")!=string::npos){
    Hist_Den = new TH1D(Form("Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_L0Num = new TH1D(Form("Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_HLT1Num = new TH1D(Form("Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_HLT2Num = new TH1D(Form("Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_L0Eff = new TH1D(Form("Hist_L0Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_L0Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_HLT1Eff = new TH1D(Form("Hist_HLT1Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT1Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_HLT2Eff = new TH1D(Form("Hist_HLT2Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT2Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_pt, bins_pt);
    Hist_Den->SetXTitle("Muon p_{T} (GeV)");

    ch_den->Draw(Form("probe_PT/1000.>>Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()));
    ch_den->Draw(Form("probe_PT/1000.>>Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_L0MuonEWDecision_TOS==1");
    ch_den->Draw(Form("probe_PT/1000.>>Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_Hlt1SingleMuonHighPTDecision_TOS==1&&probe_L0MuonEWDecision_TOS==1");
    ch_den->Draw(Form("probe_PT/1000.>>Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_Hlt2SingleMuonHighPTDecision_TOS==1&&probe_L0MuonEWDecision_TOS==1&&probe_Hlt1SingleMuonHighPTDecision_TOS==1");
  }else if(var_type.find("PHI")!=string::npos){
    Hist_Den = new TH1D(Form("Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_phi, bins_phi);
    Hist_L0Num = new TH1D(Form("Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_phi, bins_phi);
    Hist_HLT1Num = new TH1D(Form("Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_phi, bins_phi);
    Hist_HLT2Num = new TH1D(Form("Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_phi, bins_phi);
    Hist_L0Eff = new TH1D(Form("Hist_L0Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_L0Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_phi, bins_phi);
    Hist_HLT1Eff = new TH1D(Form("Hist_HLT1Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT1Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_phi, bins_phi);
    Hist_HLT2Eff = new TH1D(Form("Hist_HLT2Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT2Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_phi, bins_phi);
    Hist_Den->SetXTitle("Muon #phi");

    ch_den->Draw(Form("probe_PHI>>Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()));
    ch_den->Draw(Form("probe_PHI>>Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_L0MuonEWDecision_TOS==1");
    ch_den->Draw(Form("probe_PHI>>Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_Hlt1SingleMuonHighPTDecision_TOS==1&&probe_L0MuonEWDecision_TOS==1");
    ch_den->Draw(Form("probe_PHI>>Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_Hlt2SingleMuonHighPTDecision_TOS==1&&probe_L0MuonEWDecision_TOS==1&&probe_Hlt1SingleMuonHighPTDecision_TOS==1");
  }else if(var_type.find("SPD")!=string::npos){
    Hist_Den = new TH1D(Form("Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_spd, bins_spd);
    Hist_L0Num = new TH1D(Form("Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_spd, bins_spd);
    Hist_HLT1Num = new TH1D(Form("Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_spd, bins_spd);
    Hist_HLT2Num = new TH1D(Form("Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_spd, bins_spd);
    Hist_L0Eff = new TH1D(Form("Hist_L0Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_L0Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_spd, bins_spd);
    Hist_HLT1Eff = new TH1D(Form("Hist_HLT1Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT1Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_spd, bins_spd);
    Hist_HLT2Eff = new TH1D(Form("Hist_HLT2Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), Form("Hist_HLT2Eff_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()), nbins_spd, bins_spd);
    Hist_Den->SetXTitle("Muon #spd");

    ch_den->Draw(Form("nSPDHits>>Hist_Den_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()));
    ch_den->Draw(Form("nSPDHits>>Hist_L0Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_L0MuonEWDecision_TOS==1");
    ch_den->Draw(Form("nSPDHits>>Hist_HLT1Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_Hlt1SingleMuonHighPTDecision_TOS==1&&probe_L0MuonEWDecision_TOS==1");
    ch_den->Draw(Form("nSPDHits>>Hist_HLT2Num_%s_%s_%s",subname.c_str(),var_type.c_str(),year.c_str()),"probe_Hlt2SingleMuonHighPTDecision_TOS==1&&probe_L0MuonEWDecision_TOS==1&&probe_Hlt1SingleMuonHighPTDecision_TOS==1");
  }


  Hist_Den->SetYTitle("Entries");
  Hist_L0Eff->SetYTitle("HLT1 Eff");
  Hist_HLT1Eff->SetYTitle("HLT1 Eff");
  Hist_HLT2Eff->SetYTitle("HLT2 Eff");
  Efficiency_1D(Hist_Den, Hist_L0Num,   Hist_L0Eff);
  Efficiency_1D(Hist_L0Num, Hist_HLT1Num, Hist_HLT1Eff);
  Efficiency_1D(Hist_HLT1Num, Hist_HLT2Num, Hist_HLT2Eff);

  Hist_L0Eff->SetAxisRange(0.5,1.0, "Y");
  Hist_HLT1Eff->SetAxisRange(0.5,1.0, "Y");
  Hist_HLT2Eff->SetAxisRange(0.5,1.0, "Y");
  Hist_L0Eff->Write();
  Hist_HLT1Eff->Write();
  Hist_HLT2Eff->Write();
}

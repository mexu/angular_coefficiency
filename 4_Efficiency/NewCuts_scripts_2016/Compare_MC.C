//=================================================================//
//#include <TTGraphAsymmErrors.h>
#include "/projects/lhcb/users/mexu/Z2MuMu/Function/const_number.hpp"
#include "/projects/lhcb/users/mexu/Z2MuMu/Function/AlgoZ.hpp"
using namespace const_num;
using namespace algoz;
void Compare_MC(){

  gROOT->ProcessLine(".x ~/lhcbStyle.C");   
  TFile* File_MC_Eff_V1  = new TFile("../Cuts_Default/outputs/TRKEff_MC_2016.root");
  TFile* File_MC_Eff_V2  = new TFile("./outputs/TRKEff_MC_2016.root");
  TFile* File_MC_Steve = new TFile("/projects/lhcb/users/qdeng/ZuuEff/Eff/Steve/MC2016/MuonTrackingMC2016.root");
  TGraphAsymmErrors *Hist_MC_Steve = (TGraphAsymmErrors*)File_MC_Steve->Get("ETA13/EfficiencyGraph");

  TH1D*  Hist_MC_Eff_V1  =(TH1D*)File_MC_Eff_V1->Get("Hist_Eff_ALL_ETA_2016");
  TH1D*  Hist_MC_Eff_V2  =(TH1D*)File_MC_Eff_V2->Get("Hist_Eff_ALL_ETA_2016");
  TH1D*  Hist_MC_MCT  =(TH1D*)File_MC_Eff_V2->Get("Hist_Matched_Eff_ALL_ETA_2016");

  TCanvas *TCan_MC_Eff= new TCanvas("TCan_MC_Eff","TCan_MC_Eff",900,700);
  TCan_MC_Eff->cd();
  Hist_MC_Eff_V1->SetXTitle("Muon #eta");
  Hist_MC_Eff_V1->SetYTitle("Muon Tracking Efficiency");
  Hist_MC_Eff_V1->SetAxisRange(0.4, 1.0, "Y");
  Hist_MC_Eff_V1->Draw();
  Hist_MC_Eff_V2->SetLineColor(4);
  Hist_MC_Eff_V2->SetMarkerColor(4);
  Hist_MC_Eff_V2->Draw("P&&same");
  Hist_MC_MCT->SetLineColor(2);
  Hist_MC_MCT->SetMarkerColor(2);
  Hist_MC_MCT->Draw("P&&same");
  TLegend* leg_MC = new TLegend(0.2,0.2,0.4,0.4);
  leg_MC->AddEntry(Hist_MC_Eff_V1,"TTTrack", "pl");
  leg_MC->AddEntry(Hist_MC_Eff_V2,"Striping", "pl");
  leg_MC->AddEntry(Hist_MC_MCT,"MCT", "pl");
  leg_MC->Draw("same");
  TCan_MC_Eff->Print("Compare_MC_Eff.pdf");
  

  TFile* File_MCCorre_Eff_V2  = new TFile("./outputs/TRKEff_MC_2016_1D_Corrected.root");
  TH1D*  Hist_MCCorre_Eff_V2  =(TH1D*)File_MCCorre_Eff_V2->Get("Hist_MC_1D_ETA_Corr");
  TFile* File_MCCorre_Eff_V1  = new TFile("../Cuts_Default/outputs/TRKEff_MC_2016_1D_Corrected.root");
  TH1D*  Hist_MCCorre_Eff_V1  =(TH1D*)File_MCCorre_Eff_V1->Get("Hist_MC_1D_ETA_Corr");

  TCanvas *TCan_MCCorre_Eff= new TCanvas("TCan_MCCorre_Eff","TCan_MCCorre_Eff",900,700);
  TCan_MCCorre_Eff->cd();
  gPad->SetGrid();
  Hist_MCCorre_Eff_V1->SetXTitle("Muon #eta");
  Hist_MCCorre_Eff_V1->SetYTitle("Muon Tracking Efficiency");
  Hist_MCCorre_Eff_V1->SetAxisRange(0.50, 1.02, "Y");
  Hist_MCCorre_Eff_V1->Draw("EP");
  Hist_MCCorre_Eff_V2->SetLineColor(4);
  Hist_MCCorre_Eff_V2->SetMarkerColor(4);
  Hist_MCCorre_Eff_V2->Draw("EP&&same");
  Hist_MC_MCT->SetLineColor(2);
  Hist_MC_MCT->SetMarkerColor(2);
  Hist_MC_MCT->Draw("EP&&same");
  TLegend* leg_MCCorre = new TLegend(0.2,0.2,0.4,0.4);
  leg_MCCorre->AddEntry(Hist_MCCorre_Eff_V1,"TTTrack", "pl");
  leg_MCCorre->AddEntry(Hist_MCCorre_Eff_V2,"Striping", "pl");
  leg_MCCorre->AddEntry(Hist_MC_MCT,"MCT", "pl");
  leg_MCCorre->Draw("same");
  TCan_MCCorre_Eff->Print("Compare_MCCorre_Eff.pdf");
 

/*
  TFile* File_Data_Eff_V2  = new TFile("./outputs/TRKEff_Data_2016_1D_Corrected.root");
  TH1D*  Hist_Data_Eff_V2  =(TH1D*)File_Data_Eff_V2->Get("Hist_Data_1D_ETA_Corr");
  TFile* File_Data_Eff_V1  = new TFile("../Cuts_Default/outputs/TRKEff_Data_2016_1D_Corrected.root");
  TH1D*  Hist_Data_Eff_V1  =(TH1D*)File_Data_Eff_V1->Get("Hist_Data_1D_ETA_Corr");
  TFile* File_Data_Steve = new TFile("/projects/lhcb/users/qdeng/ZuuEff/Eff/Steve/2016/MuonTracking2016.root");
  TGraphAsymmErrors *Hist_Data_Steve = (TGraphAsymmErrors*)File_Data_Steve->Get("ETA13/EfficiencyGraph");

  TCanvas *TCan_Data_Eff= new TCanvas("TCan_Data_Eff","TCan_Data_Eff",900,700);
  TCan_Data_Eff->cd();
  //Hist_Data_Eff_V1->SetAxisRange(0.7,0.9, "Y");
  Hist_Data_Eff_V1->SetXTitle("Muon #eta");
  Hist_Data_Eff_V1->SetYTitle("Muon Tracking Efficiency");
  Hist_Data_Eff_V1->Draw();
  Hist_Data_Eff_V2->SetLineColor(4);
  Hist_Data_Eff_V2->SetMarkerColor(4);
  Hist_Data_Eff_V2->Draw("P&&same");
  Hist_Data_Steve->SetLineColor(2);
  Hist_Data_Steve->SetMarkerColor(2);
  Hist_Data_Steve->Draw("P&&same");
  TLegend* leg_Data = new TLegend(0.2,0.2,0.4,0.4);
  leg_Data->AddEntry(Hist_Data_Eff_V1,"TTTrack", "pl");
  leg_Data->AddEntry(Hist_Data_Eff_V2,"Striping", "pl");
  leg_Data->AddEntry(Hist_Data_Steve,"Steve", "pl");
  leg_Data->Draw("same");
  TCan_Data_Eff->Print("Compare_Data_Eff.pdf");
*/
}

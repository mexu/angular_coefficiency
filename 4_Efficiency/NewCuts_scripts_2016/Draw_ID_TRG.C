//=================================================================//
//  To run it:                                                     //
//  > root -q -b -l Draw_ID_TRG.C\(\"2016\",\"Data\"\)
//  > root -q -b -l Draw_ID_TRG.C\(\"2016\")
//=================================================================//
#include "Efficiency_Algo.hpp"
void Draw_ID_TRG(string _year,string type){

  gROOT->ProcessLine(".x ~/lhcbStyle.C");   

  //Draw ID and Trigger Mass distribution to see if there are more background in the ID efficiency sample
  TFile* File_Data_TRG_Eff = new TFile(Form("./outputs/TRGEff_Data_%s.root",_year.c_str()));
  TFile* File_Data_ID_Eff  = new TFile(Form("./outputs/IDEff_Data_%s.root",_year.c_str()));

  TH1D* Hist_Data_TRG_Eff =(TH1D*)File_Data_TRG_Eff->Get(Form("Hist_Mass_TRG_%s_Pass",_year.c_str()));
  TH1D* Hist_Data_ID_Eff =(TH1D*)File_Data_ID_Eff->Get(Form("Hist_Mass_ID_%s_Pass",_year.c_str()));
  Make_Mass_Comparison(Hist_Data_TRG_Eff,Hist_Data_ID_Eff,Form("%s -Muon ID",_year.c_str()),Form("%s -Muon Trigger",_year.c_str()),Form("../plots/Eff/Sys/%s_TRG_ID_Mass_Compare",_year.c_str()));








}

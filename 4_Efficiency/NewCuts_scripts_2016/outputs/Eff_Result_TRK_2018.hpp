%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\centering
\begin{tabular}{c|c|c}
\hline
$\eta$ & $\epsilon$ & $\delta epsilon_{trg}$\
\hline
1.800-2.000 & 0.837 & 0.006
2.000-2.250 & 0.901 & 0.003
2.250-2.500 & 0.919 & 0.003
2.500-2.750 & 0.930 & 0.002
2.750-3.000 & 0.906 & 0.002
3.000-3.250 & 0.905 & 0.002
3.250-3.500 & 0.920 & 0.002
3.500-3.750 & 0.903 & 0.003
3.750-4.000 & 0.878 & 0.003
4.000-4.250 & 0.839 & 0.004
4.250-4.500 & 0.661 & 0.006
4.500-5.000 & 0.455 & 0.010
\end{tabular}\label{tab:eff_2018_TRK}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

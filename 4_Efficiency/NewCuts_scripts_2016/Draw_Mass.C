#include "Efficiency_Algo.hpp"
void Draw_Mass(){

  gROOT->ProcessLine(".x ~/lhcbStyle.C");
  TChain* ch = new TChain(); 
  ch->Add("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Eff/TRK/TRK_MC_2016_U_Test.root/TreePlus_Num");
  ch->Add("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Eff/TRK/TRK_MC_2016_D_Test.root/TreePlus_Num");
  //ch->Add("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Eff/TRK/TRK_Data_2016_U_Test.root/TreePlus_Num");
  //ch->Add("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Eff/TRK/TRK_Data_2016_D_Test.root/TreePlus_Num");

  TH1D* Hist_M_Eta_1 = new TH1D("Hist_M_Eta_1","Hist_M_Eta_1",40,50,150);
  TH1D* Hist_M_Eta_2 = new TH1D("Hist_M_Eta_2","Hist_M_Eta_2",40,50,150);

  ch->Draw("boson_M/1000.>>Hist_M_Eta_1","probe_ETA>3.5&&probe_ETA<4.5");
  ch->Draw("boson_M/1000.>>Hist_M_Eta_2","probe_ETA>2.0&&probe_ETA<3.5");

//  Hist_M_Eta_1->Draw();
//  Hist_M_Eta_2->SetLineColor(2);
//  Hist_M_Eta_2->SetMarkerColor(2);
//  Hist_M_Eta_2->Draw("same");

  
  Make_Mass_Comparison(Hist_M_Eta_1,Hist_M_Eta_2,"3.5<#eta_{probe}<4.5","2.0<#eta_{probe}<3.5","./Mass_Test");



}

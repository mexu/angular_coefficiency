from ROOT import *
import os,sys

def variable(tree,var1,var2,var3,var4,var5,var6,var7,var8,var9,var10,var11,var12):

    tree.SetBranchStatus("*",0)
    tree.SetBranchStatus(var1,1)
    tree.SetBranchStatus(var2,1)
    tree.SetBranchStatus(var3,1)
    tree.SetBranchStatus(var4,1)
    tree.SetBranchStatus(var5,1)
    tree.SetBranchStatus(var6,1)
    tree.SetBranchStatus(var7,1)
    tree.SetBranchStatus(var8,1)
    tree.SetBranchStatus(var9,1)
    tree.SetBranchStatus(var10,1)
    tree.SetBranchStatus(var11,1)
    tree.SetBranchStatus(var12,1)

def copytree(fin,fout,mc,match):

    ch_plus=TChain("IDPlusTag/DecayTree")
    ch_minus=TChain("IDMinusTag/DecayTree")
    ch_plus.Add(fin)
    ch_minus.Add(fin)

    f=TFile(fout,"recreate")
 
    tag_cuts        = "&& tag_TRACK_PCHI2 > 0.001 && (sqrt(tag_PERR2)/tag_P) < 0.1";
    tag_cuts       += "&&tag_isMuon ==1";
    if(match):
      tag_cuts     += "&& tag_PT > 10000 && tag_ETA > 1.8 && tag_ETA < 5.0";
      tag_cuts     += "&& tag_0.50_cc_vPT < 2000";
    else:
      tag_cuts     += "&& tag_PT > 20000 && tag_ETA > 2.0 && tag_ETA < 4.5";
      tag_cuts     += "&& tag_Hlt2SingleMuonHighPTDecision_TOS==1";
      tag_cuts     += "&& tag_Hlt1SingleMuonHighPTDecision_TOS == 1";
      tag_cuts     += "&& tag_L0MuonEWDecision_TOS ==1";
      tag_cuts     += "&& tag_0.50_cc_vPT < 2000"; 
 
    probe_cuts      = "&& probe_TRACK_PCHI2>0.001 && (sqrt(probe_PERR2)/probe_P) < 0.1";
    probe_cuts     += "&& probe_PT > 10000 && probe_ETA > 1.8 && probe_ETA < 5.0";
    probe_cuts     += "&& probe_0.50_cc_vPT < 2000";
 
    Z_cuts          = "&& (abs(tag_PHI-probe_PHI)<TMath::Pi() ? abs(tag_PHI-probe_PHI) : 2*TMath::Pi()-abs(tag_PHI-probe_PHI))>2.7";
    Z_cuts         += "&& boson_M > 60000 && boson_M < 120000";
    Z_cuts         += "&& boson_ENDVERTEX_CHI2/boson_ENDVERTEX_NDOF < 5";

    if(mc):
      Z_cuts       += "&& abs(tag_TRUEID) == 13 && abs(tag_MC_MOTHER_ID) ==23";
      if(match):     
        Z_cuts     += "&& abs(probe_TRUEID) == 13 && abs(probe_MC_MOTHER_ID) ==23";

    sel_cuts        = "&&probe_isMuon == 1";
    fail_cuts       = "&&probe_isMuon == 0";
 
    pass_den   = "1" +  tag_cuts + probe_cuts + Z_cuts;
    pass_num   = "1" +  tag_cuts + probe_cuts + Z_cuts+ sel_cuts;
    fail_num   = "1" +  tag_cuts + probe_cuts + Z_cuts + fail_cuts;

    plus_den =  ch_plus.CopyTree(pass_den)
    plus_num =  ch_plus.CopyTree(pass_num)
    minus_den = ch_minus.CopyTree(pass_den)
    minus_num = ch_minus.CopyTree(pass_num)
    plus_fail  = ch_minus.CopyTree(fail_num)
    minus_fail = ch_plus.CopyTree(fail_num)
 
    variable(plus_den,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")
    variable(plus_num,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")
    variable(minus_den,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")
    variable(minus_num,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")
    variable(plus_fail,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")
    variable(minus_fail,"probe_PT","probe_ETA","probe_PHI","nSPDHits","boson_M","tag_PT","tag_ETA","tag_PHI","tag_ID","probe_ID","boson_ENDVERTEX_CHI2","boson_ENDVERTEX_NDOF")

    plus_den.SetName("TreePlus_Den");
    plus_num.SetName("TreePlus_Num");
    minus_den.SetName("TreeMinus_Den");
    minus_num.SetName("TreeMinus_Num");
    plus_fail.SetName("TreePlus_Fail");
    minus_fail.SetName("TreeMinus_Fail");

    plus_den.Write()
    plus_num.Write()
    minus_den.Write()
    minus_num.Write()
    plus_fail.Write()
    minus_fail.Write()
    f.Close()
#2016
copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_MC_2016_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_MC_2016_U_Pre.root",True,False); #MC MagUp Tag-and-Probe
copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_MC_2016_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_MC_2016_D_Pre.root",True,False); #MC MagDown Tag-and-Probe
copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_MC_2016_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_MC_2016_U_Pre_Matched.root",True,True); #MC MagUp MC-Match
copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_MC_2016_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_MC_2016_D_Pre_Matched.root",True,True); #MC MagDown MC-Match
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2016_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2016_U_Pre.root",False,False); #Data MagUp Tag-and-Probe
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2016_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2016_D_Pre.root",False,False); #Data MagDown Tag-and-Probe
###2017
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2017_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2017_U_Pre.root",False,False); #Data MagUp Tag-and-Probe
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2017_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2017_D_Pre.root",False,False); #Data MagDown Tag-and-Probe
###2018
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2018_U.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2018_U_Pre.root",False,False); #Data MagUp Tag-and-Probe
#copytree("/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2018_D.root","/projects/lhcb/users/mexu/Z2MuMu/DVTuples/Efficiency/ID/ID_Data_2018_D_Pre.root",False,False); #Data MagDown Tag-and-Probe

# coding=utf-8
##############################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
##############################################################################
# Define a sequence
from Gaudi.Configuration import *
from Configurables import DaVinci , GaudiSequencer , LoKi__VoidFilter , ChargedProtoParticleMaker , NoPIDsParticleMaker , TrackSelector
from PhysSelPython.Wrappers import AutomaticData , Selection , SelectionSequence
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from Configurables import LoKi__HDRFilter as StripFilter
from Configurables import MuonTTTrack , PatAddTTCoord , TrackMasterFitter , MuonCombRec , TrackMasterExtrapolator
triggers = [
    "L0MuonDecision",
    "L0MuonEWDecision",
    "Hlt1SingleMuonHighPTDecision",
    "Hlt2SingleMuonDecision",
    "Hlt2SingleMuonLowPTDecision",
    "Hlt2SingleMuonHighPTDecision",
    "Hlt2SingleMuonVHighPTDecision",
    "Hlt2EWSingleMuonHighPtDecision",
    "Hlt2EWSingleMuonVHighPtDecision"
    ]
seq = GaudiSequencer("MuonTTSequence")
DaVinci().UserAlgorithms += [seq ]

FILTER = "HLT_PASS('StrippingWMuLineDecision')"
seq.Members += [ StripFilter( 'StripPassFilterForMuonTT', Code=FILTER , Location="/Event/Strip/Phys/DecReports" ) ]
seq.Members += [ LoKi__VoidFilter( 'Require_Muon_Raw' , Code = "CONTAINS ( 'Raw/Muon/Coords') > 0" )]

from Configurables import MuonTTTrack, MuonNNetRec, PatAddTTCoord, TrackMasterFitter, MuonCombRec, TrackMasterExtrapolator,MuonHitDecode
XTolParam = 25.0
MaxChi2TolParam = 7.0
MinAxProjParam = 5.5
MajAxProjParam = 25.0

MakeMuonTT = MuonTTTrack("MakeMuonTT")
MakeMuonTT.ToolName = "MuonCombRec"
MakeMuonTT.addTool( MuonCombRec )
MakeMuonTT.MuonCombRec.MeasureTime = True
MakeMuonTT.MuonCombRec.CloneKiller = False
MakeMuonTT.MuonCombRec.SkipStation = -1 # -1=no skip, 0=M1, 1=M2, 2=M3, 3=M4, 4=M5
MakeMuonTT.MuonCombRec.DecodingTool = "MuonHitDecode"
MakeMuonTT.MuonCombRec.PadRecTool = "MuonPadFromCoord"
MakeMuonTT.MuonCombRec.ClusterTool = "MuonFakeClustering" # to enable: "MuonClusterRec"
MakeMuonTT.MuonCombRec.PhysicsTiming = True
MakeMuonTT.MuonCombRec.AssumeCosmics = False
MakeMuonTT.MuonCombRec.AssumePhysics = True
MakeMuonTT.MuonCombRec.StrongCloneKiller = False
MakeMuonTT.MuonCombRec.SeedStation = 4 # default seed station is 2 - M3, better is 4 - M5
# #############################################################
MakeMuonTT.addTool( PatAddTTCoord )
MakeMuonTT.PatAddTTCoord.YTolSlope = 400000.0
MakeMuonTT.PatAddTTCoord.XTol = XTolParam
MakeMuonTT.PatAddTTCoord.XTolSlope = 400000.0
MakeMuonTT.PatAddTTCoord.MinAxProj = MinAxProjParam
MakeMuonTT.PatAddTTCoord.MajAxProj = MajAxProjParam
MakeMuonTT.PatAddTTCoord.MaxChi2Tol = MaxChi2TolParam
MakeMuonTT.addTool( TrackMasterFitter)
#MakeMuonTT.TrackMasterFitter.MaterialLocator = "SimplifiedMaterialLocator"
MakeMuonTT.addTool( TrackMasterExtrapolator )
#MakeMuonTT.TrackMasterExtrapolator.MaterialLocator = "SimplifiedMaterialLocator"
# ########################################################################################
MakeMuonTT.AddTTHits = True
MakeMuonTT.MC = False
MakeMuonTT.FillMuonStubInfo = True
MakeMuonTT.OutputLevel = 4
MakeMuonTT.Output = "Rec/Track/MuonTT"
seq.Members     += [MakeMuonTT]
# ########################################################################################
# Make the protoparticles
# ########################################################################################
from PhysSelPython.Wrappers import Selection, DataOnDemand, ChargedProtoParticleSelection
from Configurables import ChargedProtoParticleMaker
MakeProtoParticles = ChargedProtoParticleMaker()
MakeProtoParticles.Inputs = ["Rec/Track/MuonTT"]
MakeProtoParticles.Output = "Rec/MuonTTPParts/ProtoParticles"
seq.Members      += [MakeProtoParticles]
# ########################################################################################
# Make Particles out of the muonTT ProtoParticles
# ########################################################################################
from Configurables import NoPIDsParticleMaker, TrackSelector
MuonTTParts = NoPIDsParticleMaker("MuonTTParts")
MuonTTParts.Particle = 'muon'
MuonTTParts.addTool( TrackSelector )
MuonTTParts.TrackSelector.TrackTypes = [ "Long" ]
MuonTTParts.Input =  "Rec/MuonTTPParts/ProtoParticles"
MuonTTParts.OutputLevel = 4
seq.Members  += [ MuonTTParts]

from Configurables import MCDecayTreeTuple
simulation = True
DtupleMCTZ = MCDecayTreeTuple("MCTZ")
DtupleMCTZ.Decay = 'Z0 => ^mu+ ^mu- {e+} {e-} {e+} {e-}'
DtupleMCTZ.Branches = {
  "Z"  : "^(Z0 =>  mu+   mu- {e+} {e-} {e+} {e-})",
  "mup":  "(Z0 => ^mu+   mu- {e+} {e-} {e+} {e-})",
  "mum":  "(Z0 =>  mu+  ^mu- {e+} {e-} {e+} {e-})"}

mctl=[
   'MCTupleToolAngles',
   'MCTupleToolHierarchy',
   'MCTupleToolKinematic',
   'MCTupleToolPrimaries',
   'MCTupleToolReconstructed',
   'MCTupleToolInteractions',
   'TupleToolEventInfo',
   'MCTupleToolHierarchy',
   "MCTupleToolKinematic"
]

DtupleMCTZ.ToolList=mctl
from Configurables import MyTupleTollPID
DtupleMCTZ.addTool(MyTupleTollPID, name="MyTupleTollPID")
DtupleMCTZ.MyTupleTollPID.StubLocation = "Phys/MuonTTParts/Particles"
DtupleMCTZ.ToolList += ["MyTupleTollPID"]

from Configurables import LoKi__Hybrid__MCTupleTool
LoKi_All=LoKi__Hybrid__MCTupleTool("LoKi_All")
DtupleMCTZ.addTool(LoKi_All)
DtupleMCTZ.LoKi_All.Variables =  {
  'TRUEID' : 'MCID',
  'ETA' : 'MCETA',
  'PHI' : 'MCPHI',
  'PT'  : 'MCPT'
}

DtupleMCTZ.LoKi_All.Preambulo = [
      "from LoKiCore.math import log"
      ]
DtupleMCTZ.LoKi_All.Variables.update({"M" : "MCM"})
DtupleMCTZ.LoKi_All.Variables.update({"Y" : "0.5*log((MCE+MCPZ)/(MCE-MCPZ))"})
DtupleMCTZ.ToolList+=["LoKi::Hybrid::MCTupleTool/LoKi_All"]


# ################################################################
# LongLong
# ################################################################
from Configurables import DecayTreeTuple,TupleToolTISTOS
from Configurables import TupleToolTrackIsolation, TupleToolTISTOS, TupleToolMuonTT,TupleToolConeIsolation,L0TriggerTisTos
SeqZMuMu = GaudiSequencer("SeqZMuMu")
DtupleTrkLongLong = DecayTreeTuple("LongLongTuple")
DtupleTrkLongLong.Decay = "Z0 -> ^mu+ ^mu-"
DtupleTrkLongLong.Inputs = ["/Event/AllStreams/Phys/Z02MuMuLine/Particles"]
DtupleTrkLongLong.Branches = {
   "mu-" : "Z0 -> mu+ ^mu-"
  ,"mu+" : "Z0 -> ^mu+ mu-"
  ,"Z0"   : "^(Z0 -> mu+ mu-)"}
DtupleTrkLongLong.ToolList = [
         "TupleToolPrimaries"
         ,"TupleToolKinematic"
#         ,"TupleToolTrackInfo"
         ,"TupleToolRecoStats"
         ,"TupleToolPid"
         ,"TupleToolANNPID"
         ,"TupleToolEventInfo"
         ,"TupleToolConeIsolation"
         ,"TupleToolAngles"
         ,"TupleToolGeometry"
         ,"TupleToolTrackEff"
         ,"TupleToolMCTruth"
         ,"TupleToolMuonTT"
    ]
from Configurables import TupleToolMCTruth
MCTruth=TupleToolMCTruth()
MCTruth.ToolList = [ "MCTupleToolKinematic", "MCTupleToolHierarchy" ]
DtupleTrkLongLong.addTool(MCTruth)

from Configurables import TupleToolRecoStats
DtupleTrkLongLong.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
DtupleTrkLongLong.TupleToolRecoStats.Verbose=True
from Configurables import TupleToolMuonTT
DtupleTrkLongLong.addTool(TupleToolMuonTT, name="TupleToolMuonTT")
DtupleTrkLongLong.TupleToolMuonTT.StubLocation = "Phys/MuonTTParts/Particles"

DtupleTrkLongLong.addTool(TupleToolConeIsolation,name="TupleToolConeIsolation")
DtupleTrkLongLong.ToolList +=["TupleToolConeIsolation"]
DtupleTrkLongLong.TupleToolConeIsolation.MinConeSize = 0.4
DtupleTrkLongLong.TupleToolConeIsolation.MaxConeSize = 0.6
DtupleTrkLongLong.TupleToolConeIsolation.FillMaxPt = False
DtupleTrkLongLong.addTool( TupleToolTISTOS, name = "TISTOS" )

DtupleTrkLongLong.ToolList += [ "TupleToolTISTOS/TISTOS" ]
DtupleTrkLongLong.TISTOS.TriggerList = triggers
DtupleTrkLongLong.TISTOS.VerboseL0     = True
DtupleTrkLongLong.TISTOS.VerboseHlt1     = True
DtupleTrkLongLong.TISTOS.VerboseHlt2     = True
#Loki
from Configurables import LoKi__Hybrid__TupleTool
LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
LoKi_All.Variables = {
  "PT" : "PT",
  "ETA" : "ETA",
  "PHI" : "PHI",
  "PERR2":"PERR2"
  }
DtupleTrkLongLong.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
DtupleTrkLongLong.addTool(LoKi_All)

########################################
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().EvtMax = -1                     # Number of events
DaVinci().DataType = "2016"                    # Default is "DC06"
DaVinci().Simulation   = True                # It's MC
DaVinci().TupleFile='TRK_MC_2016_D.root'
DaVinci().Lumi= False
DaVinci().PrintFreq=2000
DaVinci().CondDBtag     = "sim-20170721-2-vc-md100"
DaVinci().DDDBtag       = "dddb-20170721-3"
DaVinci().UserAlgorithms = [seq,DtupleMCTZ,DtupleTrkLongLong]

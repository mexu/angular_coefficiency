#-- GAUDI jobOptions generated on Mon Aug  6 05:15:49 2018
#-- Contains event types : 
#--   90000000 - 2 files - 430984 events - 7.19 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-132873 

#--  StepId : 132873 
#--  StepName : Stripping28r1-Merging-DV-v41r4p4-AppConfig-v3r345 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v41r4p4 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DV-Stripping-Merging.py;$APPCONFIGOPTS/Persistency/Compression-LZMA-4.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20161004 
#--  ExtraPackages : AppConfig.v3r345;SQLDDDB.v7r10 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'/eos/lhcb/user/m/mexu/For_Z/2016_MC/Jpsi/00056285_00000007_3.AllStreams.dst'
], clear=True)

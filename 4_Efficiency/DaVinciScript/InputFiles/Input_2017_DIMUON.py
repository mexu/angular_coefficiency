from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
 '/eos/lhcb/user/m/mexu/For_Z/2017_Data/00071700_00000308_1.dimuon.dst'
,'/eos/lhcb/user/m/mexu/For_Z/2017_Data/00071700_00000462_1.dimuon.dst'
,'/eos/lhcb/user/m/mexu/For_Z/2017_Data/00071499_00000032_1.dimuon.dst'
,'/eos/lhcb/user/m/mexu/For_Z/2017_Data/00071700_00001115_1.dimuon.dst'
,'/eos/lhcb/user/m/mexu/For_Z/2017_Data/00071499_00000133_1.dimuon.dst'
,'/eos/lhcb/user/m/mexu/For_Z/2017_Data/00071499_00000076_1.dimuon.dst'
,'/eos/lhcb/user/m/mexu/For_Z/2017_Data/00071499_00000179_1.dimuon.dst'
,'/eos/lhcb/user/m/mexu/For_Z/2017_Data/00071499_00000102_1.dimuon.dst'
], clear=True)

import sys

bkq = BKQuery (
    dqflag="OK",
    path="/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/DIMUON.DST",
    type="Path"
    )
bkq2 = BKQuery (
    dqflag = "OK" ,
    path="/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/DIMUON.DST",
    type = "Path"
    )
ds=bkq.getDataset()
ds2=bkq2.getDataset()

#if len(ds.files) == 0 or len(ds2.files) == 0:
#    print "dataset(s) empty - exiting"
#    sys.exit()
#------------------------------------------------------------------------------
# Make a new job object for DaVinci
#------------------------------------------------------------------------------
j  = Job()
j2 = Job()
myApp = GaudiExec()
myApp.directory = "/afs/cern.ch/work/m/mexu/workspace/Z2MuMu_OrignalTree/DaVinci/DaVinciDev_v45r2"
myApp.options  = ["/afs/cern.ch/work/m/mexu/workspace/Z2MuMu_OrignalTree/4_Efficiency/submit/DV_Data_TRK_2018.py"]
myApp.platform  = "x86_64-centos7-gcc8-opt"

myApp2 = GaudiExec()
myApp2.directory = "/afs/cern.ch/work/m/mexu/workspace/Z2MuMu_OrignalTree/DaVinci/DaVinciDev_v45r2"
myApp2.options   = ["/afs/cern.ch/work/m/mexu/workspace/Z2MuMu_OrignalTree/4_Efficiency/submit/DV_Data_TRK_2018.py"]
myApp2.platform  = "x86_64-centos7-gcc8-opt"

j.application = myApp
j2.application = myApp2

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Define name for identifying the job (this name appears in the ganga job list)
#------------------------------------------------------------------------------

j.name = 'TRK_Data_U_2018'
j.do_auto_resubmit=True
j2.name = 'TRK_Data_D_2018'
j2.do_auto_resubmit=True

j.inputdata = ds
j2.inputdata = ds2

j.splitter = SplitByFiles ( filesPerJob = 30,maxFiles =-1, ignoremissing = True, bulksubmit=False)
j2.splitter = SplitByFiles ( filesPerJob = 30, maxFiles =-1, ignoremissing = True, bulksubmit=False)
#------------------------------------------------------------------------------
# Output data
j.outputfiles = [DiracFile("TRK_Data_2018.root")]
j2.outputfiles = [DiracFile("TRK_Data_2018.root")]

j.backend    = Dirac()
j2.backend   = Dirac()

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Submit the job
#------------------------------------------------------------------------------
j.submit()
j2.submit()
#------------------------------------------------------------------------------

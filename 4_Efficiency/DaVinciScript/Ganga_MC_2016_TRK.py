import os
bkq = BKQuery (
    dqflag="OK",
    path="/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09h/Trig0x6139160F/Reco16/Turbo03a/Stripping28r1NoPrescalingFlagged/42112001/ALLSTREAMS.DST",
    type="Path"
    )
bkq2 = BKQuery (
    dqflag = "OK" ,
    path="/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x6139160F/Reco16/Turbo03a/Stripping28r1NoPrescalingFlagged/42112001/ALLSTREAMS.DST",
    type = "Path"
    )

ds=bkq.getDataset()
ds2=bkq2.getDataset()

j  = Job()
j2 = Job()

myApp = GaudiExec()
myApp.directory = "/afs/cern.ch/work/m/mexu/workspace/Z2MuMu_OrignalTree/DaVinci/DaVinciDev_v45r2"
myApp.options   = [os.getcwd()+"/DV_MC_TRK_2016_U.py"]
myApp.platform  = "x86_64-centos7-gcc8-opt"

myApp2 = GaudiExec()
myApp2.directory = "/afs/cern.ch/work/m/mexu/workspace/Z2MuMu_OrignalTree/DaVinci/DaVinciDev_v45r2"
myApp2.options   = [os.getcwd()+"/DV_MC_TRK_2016_D.py"]
myApp2.platform  = "x86_64-centos7-gcc8-opt"

j.application = myApp
j2.application = myApp2

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Define name for identifying the job (this name appears in the ganga job list)
#------------------------------------------------------------------------------

j.name = 'TRK_MC_U_2016'
j.do_auto_resubmit=True
j2.name = 'TRK_MC_D_2016'
j2.do_auto_resubmit=True

j.inputdata = ds
j2.inputdata = ds2

j.splitter = SplitByFiles ( filesPerJob = 10,maxFiles =-1, ignoremissing = True, bulksubmit=False)
j2.splitter = SplitByFiles ( filesPerJob = 10, maxFiles =-1, ignoremissing = True, bulksubmit=False)
#------------------------------------------------------------------------------
# Output data
j.outputfiles = [DiracFile("TRK_MC_2016_U.root")]
j2.outputfiles = [DiracFile("TRK_MC_2016_D.root")]

j.backend    = Dirac()
j2.backend   = Dirac()

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Submit the job
#------------------------------------------------------------------------------
j.submit()
j2.submit()
#------------------------------------------------------------------------------

// $Id: TupleToolGeometry.h,v 1.10 2010-02-09 09:40:49 pkoppenb Exp $
#ifndef JBOREL_TUPLETOOLGEOMETRYNEW_H
#define JBOREL_TUPLETOOLGEOMETRYNEW_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"   
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackVertexer.h"         // Interface
#include "Kernel/IParticleCombiner.h"

class IDistanceCalculator;
class IDVAlgorithm;

class IPVReFitter;

namespace LHCb
{
  class Particle;
  class VertexBase;
}


class TupleToolMuonTT : public TupleToolBase,
                          virtual public IParticleTupleTool
{

public:

  /// Standard constructor
  TupleToolMuonTT( const std::string& type,
                     const std::string& name,
                     const IInterface* parent);

  virtual ~TupleToolMuonTT( ){}; ///< Destructor

  virtual StatusCode initialize();

  StatusCode fill( const LHCb::Particle*
                   , const LHCb::Particle*
                   , const std::string&
                   , Tuples::Tuple& );

private:
  LHCb::Particle::Range m_stubparts;
  std::string m_stublocation;
  std::pair<double, const LHCb::Particle*> getMuonTTPart(const LHCb::Particle* P);
  double getDeltaR(const LHCb::Particle* partA, const LHCb::Particle* partB);
  double getCommonMuonIDs(const LHCb::Particle* longMu, const LHCb::Particle* muonTT);
  double getCommonTTIDs(const LHCb::Particle* longMu, const LHCb::Particle* muonTT);
  std::pair<int, int> getMuonTTIDs(const LHCb::Particle* part);
  std::pair<int, int> getFullMuonTTIDs(const LHCb::Particle* part);
  const LHCb::Particle* getOppPart(const LHCb::Particle* mother, const LHCb::Particle* P);
  std::pair<double, int> getVtxChi2(const LHCb::Particle* muTT, const LHCb::Particle* oppPart);
    double getMass(const LHCb::Particle* muTT, const LHCb::Particle* oppPart);
  double getDeltaPhi(const LHCb::Particle* muTT, const LHCb::Particle* oppPart);

  std::pair<LHCb::Particle*, LHCb::Vertex*> getCombinedParticle(const LHCb::Particle* partA, const LHCb::Particle* partB);
  
  ITrackVertexer* m_trackVertexer;
  IParticleCombiner* m_particleCombiner;

  std::string m_vtxFitterName;

};
#endif // JBOREL_TUPLETOOLGEOMETRY_H

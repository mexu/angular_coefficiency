// $Id: MyTupleTollPID.h,v 1.2 2010-01-26 15:39:26 rlambert Exp $
#ifndef MCTUPLETOOLPID_H
#define MCTUPLETOOLPID_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IMCParticleTupleTool.h"            // Interface
#include "Event/MCParticle.h"            // Interface
#include "Event/Particle.h"            // Interface

/** @class MyTupleTollPID MyTupleTollPID.h
 *
 *  Fill MC Particle
 *
 * - head_ID : pid
 *
 *
 * \sa MyTupleTollPID, DecayTreeTuple, MCDecayTreeTuple
 *
 *  @author Rob Lambert
 *  @date   2009-11-19
 */

class MyTupleTollPID : public TupleToolBase,
                       virtual public IMCParticleTupleTool
{

public:

  /// Standard constructor
  MyTupleTollPID( const std::string& type,
                  const std::string& name,
                  const IInterface* parent);

  virtual ~MyTupleTollPID( ); ///< Destructor
  
  virtual StatusCode initialize();

  virtual StatusCode fill( const LHCb::MCParticle*
                           , const LHCb::MCParticle*
                           , const std::string&
                           , Tuples::Tuple& );
private:

  LHCb::Particle::Range m_stubparts;
  std::string m_stublocation;
  double getDeltaR(const LHCb::Particle* partA, const LHCb::MCParticle* partB);


};

#endif // MCTUPLETOOLPID_H


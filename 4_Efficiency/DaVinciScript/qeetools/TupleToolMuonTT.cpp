// $Id: TupleToolGeometryNew.cpp,v 1.17 2010-05-12 20:01:40 jpalac Exp $
// Include files

// from Gaudi
// yinh debug here, Sept. 2019
//#include "GaudiKernel/ToolFactory.h"

// local
#include "TupleToolMuonTT.h"
#include "Kernel/IDVAlgorithm.h"
#include <Kernel/GetIDVAlgorithm.h>
#include <Kernel/IDistanceCalculator.h>
#include "Kernel/IPVReFitter.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"
#include "TMath.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : GeometryTupleTool
//
// 2007-11-07 : Jeremie Borel
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
// actually acts as a using namespace TupleTool
// yinh debug here, Sept. 2019
DECLARE_COMPONENT( TupleToolMuonTT )
//DECLARE_TOOL_FACTORY( TupleToolMuonTT )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  TupleToolMuonTT::TupleToolMuonTT( const std::string& type,
                                        const std::string& name,
                                        const IInterface* parent )
    :
    TupleToolBase ( type, name , parent )
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty( "StubLocation", m_stublocation = "Phys/StdAllNoPIDsMuons/Particles");
  declareProperty( "VertexFitter", m_vtxFitterName = "OfflineVertexFitter");

  //                "Turn false if the mother is expected to be NULL, will not fill mother PV info");
  // replaced by Verbose

}

//=============================================================================

StatusCode TupleToolMuonTT::initialize()
{
  
  m_trackVertexer = tool<ITrackVertexer>( "TrackVertexer","Vertexer",this);
  const StatusCode sc = TupleToolBase::initialize();
  m_particleCombiner = tool<IParticleCombiner>(m_vtxFitterName, this);
  return sc;
}

//=============================================================================
StatusCode TupleToolMuonTT::fill( const Particle* mother
                                    , const Particle* P
                                    , const std::string& head
                                    , Tuples::Tuple& tuple )
{
  const std::string prefix = fullName(head);

  StatusCode sc = StatusCode::SUCCESS;
  double mass = -1.0;

  if ( P && P->isBasicParticle() ) {
    

    if(exist<LHCb::Particle::Range>(m_stublocation))  
      {
	m_stubparts = get<LHCb::Particle::Range>(m_stublocation);
      }
    else{
      m_stubparts = LHCb::Particle::Range();
    }
  

    std::pair<int, int> partpair = getFullMuonTTIDs( P );
    tuple->column( head + "_nMuonIDs", partpair.first );
    tuple->column( head + "_nTTIDs"  , partpair.second);
    

    std::pair<double, const LHCb::Particle*> pair = getMuonTTPart(P);
    const LHCb::Particle* muonTTpart = pair.second;
    double deltaRmin = pair.first;

    if (deltaRmin!=-1.0){
      std::pair<int, int> pair = getMuonTTIDs(muonTTpart);
      int nMuonIDs = pair.first;
      int nTTIDs = pair.second;
      int nCommonMuonIDs = getCommonMuonIDs(P, muonTTpart);
      int nCommonTTIDs   = getCommonTTIDs(P,muonTTpart);
      
      tuple->column( head + "_MuonTT_PX",             muonTTpart->momentum().x() );
      tuple->column( head + "_MuonTT_PY",             muonTTpart->momentum().y() );
      tuple->column( head + "_MuonTT_PZ",             muonTTpart->momentum().z() );
      tuple->column( head + "_MuonTT_P",              muonTTpart->p()  );
      tuple->column( head + "_MuonTT_PT",             muonTTpart->pt() );
      tuple->column( head + "_MuonTT_PHI",            muonTTpart->momentum().phi() );
      tuple->column( head + "_MuonTT_ETA",            muonTTpart->momentum().eta() );
      tuple->column( head + "_MuonTT_DeltaR",         deltaRmin );
      tuple->column( head + "_MuonTT_nTTIDs",         nTTIDs );
      tuple->column( head + "_MuonTT_nMuonIDs",       nMuonIDs );
      tuple->column (head + "_MuonTT_nCommonTTIDs",   nCommonTTIDs );
      tuple->column (head + "_MuonTT_nCommonMuonIDs", nCommonMuonIDs );

      if (mother){
	const LHCb::Particle* oppPart = getOppPart(mother, P);
	std::pair<LHCb::Particle*, LHCb::Vertex*> pair = getCombinedParticle(muonTTpart, oppPart);
	//std::pair<double, int> vtxchi2 = getVtxChi2( muonTTpart, oppPart);
	double vtxChi2 = -1;
	int vtxnDoF = 0;
	if (pair.second){
	  vtxChi2 = pair.second->chi2();
	  vtxnDoF = pair.second->nDoF();
	}
	//double mass = getMass(muonTTpart, oppPart);
	if (pair.first) mass = pair.first->momentum().M();
	double dphi = getDeltaPhi(muonTTpart, oppPart);
	tuple->column (head + "_MuonTT_VtxChi2", vtxChi2);
	tuple->column (head + "_MuonTT_VtxNDoF", vtxnDoF);
	tuple->column( head + "_MuonTT_M"      , mass);
	tuple->column( head + "_MuonTT_DPhi"   , dphi);
      } else{
	  tuple->column (head + "_MuonTT_VtxChi2",        -1.0 );
	  tuple->column (head + "_MuonTT_VtxNDoF",        -1   );
	  tuple->column( head + "_MuonTT_M"      , -1.0);
	  tuple->column( head + "_MuonTT_DPhi"   , -1.0);
      }

      double Fraction_TT =  (double)nCommonTTIDs/nTTIDs;
      double Fraction_Muon =  (double)nCommonMuonIDs/nMuonIDs;
      bool TTHitAssocZM = false;
      bool noTTHitAssocZM = false;
      bool exist = false;
      if(mass > 40000 && deltaRmin<0.1){
        exist = true;
        if(nTTIDs>0){
           if(Fraction_TT>0.6 && Fraction_Muon>0.4) {TTHitAssocZM=true;}
         }
        else{
          if(Fraction_Muon>0.4) {noTTHitAssocZM=true;}
        }
      }
      bool assocZM = TTHitAssocZM || noTTHitAssocZM;
      tuple->column( head + "_MuonTT_AssocZM", assocZM);
      tuple->column( head + "_MuonTT_ExistTT", exist);
    }
    else{
      tuple->column( head + "_MuonTT_PX",             -1.0 );
      tuple->column( head + "_MuonTT_PY",             -1.0 );
      tuple->column( head + "_MuonTT_PZ",             -1.0 );
      tuple->column( head + "_MuonTT_P",              -1.0 );
      tuple->column( head + "_MuonTT_PT",             -1.0 );
      tuple->column( head + "_MuonTT_PHI",            -1.0 );
      tuple->column( head + "_MuonTT_ETA",            -1.0 );
      tuple->column( head + "_MuonTT_DeltaR",         -1.0 );
      tuple->column( head + "_MuonTT_nTTIDs",         -1   );
      tuple->column( head + "_MuonTT_nMuonIDs",       -1   );
      tuple->column (head + "_MuonTT_nCommonTTIDs",   -1   );
      tuple->column (head + "_MuonTT_nCommonMuonIDs", -1   );
      tuple->column (head + "_MuonTT_VtxChi2",        -1.0 );
      tuple->column (head + "_MuonTT_VtxNDoF",        -1   );
      tuple->column( head + "_MuonTT_M"      , -1.0);
      tuple->column( head + "_MuonTT_DPhi"   , -1.0);
      tuple->column( head + "_MuonTT_AssocZM", false);
      tuple->column( head + "_MuonTT_ExistTT", false);
      }
    }
  return sc ;
}

std::pair<double, const LHCb::Particle*> TupleToolMuonTT::getMuonTTPart(const LHCb::Particle* P){
  double deltaRmin = -1.0;
  const LHCb::Particle* muonTTPart = new LHCb::Particle();

  for (LHCb::Particle::Range::const_iterator ip = m_stubparts.begin();ip!=m_stubparts.end();++ip){
    const LHCb::Particle* MuTT = (*ip);
    if (MuTT->charge() == P->charge() ){
      double deltaR = getDeltaR(MuTT, P);
      if (deltaR < deltaRmin || deltaRmin == -1.0){
	deltaRmin = deltaR;
	muonTTPart = MuTT;
      }
    }
  }
  std::pair<double, const LHCb::Particle*> pair;
  pair.first = deltaRmin;
  pair.second = muonTTPart;
  return pair;
}

double TupleToolMuonTT::getDeltaR(const LHCb::Particle* partA, const LHCb::Particle* partB){
  double deltaEta = partA->momentum().eta() - partB->momentum().eta();
  double deltaPhi = 0.0;
  if (fabs(partA->momentum().phi() - partB->momentum().phi()) < TMath::Pi()){
    deltaPhi = fabs(partA->momentum().phi() - partB->momentum().phi());
  }
  else deltaPhi = 2*TMath::Pi() - fabs(partA->momentum().phi() - partB->momentum().phi());

  double deltaR = sqrt( pow(deltaEta,2) + pow(deltaPhi,2));
  return deltaR;
}

double TupleToolMuonTT::getCommonMuonIDs(const LHCb::Particle* longMu, const LHCb::Particle* muonTT){
  int nCommon = 0;
  if ( longMu->proto() && longMu->proto()->muonPID() && longMu->proto()->muonPID()->muonTrack() &&
       muonTT->proto() && muonTT->proto()->track() ){
    const LHCb::Track* longTrack = longMu->proto()->muonPID()->muonTrack();
    const LHCb::Track* muonTrack = muonTT->proto()->track();
    nCommon = longTrack->nCommonLhcbIDs(*muonTrack);
  }
  return nCommon;
}

double TupleToolMuonTT::getCommonTTIDs(const LHCb::Particle* longMu, const LHCb::Particle* muonTT){
  int nCommon = 0;
  if ( longMu->proto() && longMu->proto()->track() &&
       muonTT->proto() && muonTT->proto()->track() ){
    const LHCb::Track* longTrack = longMu->proto()->track();
    const LHCb::Track* muonTrack = muonTT->proto()->track();
    nCommon = longTrack->nCommonLhcbIDs(*muonTrack);
  }
  return nCommon;
}

std::pair<int, int> TupleToolMuonTT::getMuonTTIDs(const LHCb::Particle* part){
  int nMuonIDs = 0;
  int nTTIDs = 0;
  if ( part->proto() && part->proto()->track()){
    std::vector<LHCb::LHCbID> lhcbIDs = part->proto()->track()->lhcbIDs();
    for (std::vector<LHCb::LHCbID>::iterator id = lhcbIDs.begin();id != lhcbIDs.end(); ++id){
      if ((*id).isMuon()) nMuonIDs++;
      if ((*id).isTT()) nTTIDs++;
    }
  }
  std::pair<int, int> pair;
  pair.first = nMuonIDs;
  pair.second = nTTIDs;
  
  return pair;
}


std::pair<int, int> TupleToolMuonTT::getFullMuonTTIDs(const LHCb::Particle* part){
  int nCommon = 0;
  int nMuonIDs = 0, nTTIDs = 0;

  if ( part->proto() && part->proto()->muonPID() && part->proto()->muonPID()->muonTrack() &&
       part->proto()->track() ){
    const LHCb::Track* muonTrack = part->proto()->muonPID()->muonTrack();
    const LHCb::Track* longTrack = part->proto()->track();

    std::vector<LHCb::LHCbID> lhcbIDs_mu = muonTrack->lhcbIDs();
    std::vector<LHCb::LHCbID> lhcbIDs_long = longTrack->lhcbIDs();

    for (std::vector<LHCb::LHCbID>::iterator id = lhcbIDs_mu.begin();id != lhcbIDs_mu.end(); ++id){
      if ((*id).isMuon()) nMuonIDs++;
      if ((*id).isTT()) nTTIDs++;
    }
    
    for (std::vector<LHCb::LHCbID>::iterator il = lhcbIDs_long.begin();il != lhcbIDs_long.end(); ++il){
      if ((*il).isMuon()) nMuonIDs++;
      if ((*il).isTT()) nTTIDs++;
    }
  }
  std::pair<int, int> pair;
  pair.first = nMuonIDs;
  pair.second = nTTIDs;
  
  return pair;

}

const LHCb::Particle* TupleToolMuonTT::getOppPart(const LHCb::Particle* mother, const LHCb::Particle* part){
    const LHCb::Particle* oppPart = 0;
   if (mother && mother!= part && mother->daughtersVector().size() == 2){
     if (mother->daughtersVector().at(0)->charge() == part->charge()){
       oppPart = mother->daughtersVector().at(1);
     }
     else {
       oppPart = mother->daughtersVector().at(0);
     }
   }
   return oppPart;
}

std::pair<LHCb::Particle*, LHCb::Vertex*> TupleToolMuonTT::getCombinedParticle(const LHCb::Particle* partA, const LHCb::Particle* partB){
  LHCb::Particle::ConstVector dauts;
  dauts.push_back(partA);
  dauts.push_back(partB);
  LHCb::Particle* P = new LHCb::Particle();
  LHCb::Vertex * vtx = new LHCb::Vertex();
  m_particleCombiner->combine(dauts, (*P), (*vtx));
  std::pair<LHCb::Particle*, LHCb::Vertex*> myPair;
  myPair.first = P;
  myPair.second = vtx;
  return myPair;
}

double TupleToolMuonTT::getMass(const LHCb::Particle* muTT, const LHCb::Particle* oppPart){
  Gaudi::LorentzVector vec  = Gaudi::LorentzVector(0,0,0,0); 
  if ( muTT && oppPart ){
    vec = muTT->momentum() + oppPart->momentum();
  }
  return vec.M();
}

double TupleToolMuonTT::getDeltaPhi(const LHCb::Particle* muTT, const LHCb::Particle* oppPart){
  double deltaPhi = 0.0;
  if ( muTT && oppPart ){
    Gaudi::LorentzVector muTTvec = muTT->momentum();
    Gaudi::LorentzVector oppPartvect = oppPart->momentum();
    
    deltaPhi = (abs(muTTvec.phi()-oppPartvect.phi())<TMath::Pi() ? 
		abs(muTTvec.phi()-oppPartvect.phi()) : 
		2*TMath::Pi()-abs(muTTvec.phi()-oppPartvect.phi()));
  }
  return deltaPhi;
}

std::pair<double, int> TupleToolMuonTT::getVtxChi2(const LHCb::Particle* muTT, const LHCb::Particle* oppPart){
  double vtxchi2 = -1.0;
  int vtxndof = -1;

  std::vector<const LHCb::Track*> tracks2vertex;
  if (muTT->proto() && muTT->proto()->track() &&
      oppPart->proto() && oppPart->proto()->track()){
    tracks2vertex.push_back(muTT->proto()->track());
    tracks2vertex.push_back(oppPart->proto()->track());
    auto vtx = m_trackVertexer->fit(tracks2vertex);
    if (vtx){
      vtxchi2 = vtx->chi2();
      vtxndof = vtx->nDoF();
    }
  }
 
  std::pair<double, int> pair;
  pair.first = vtxchi2;
  pair.second = vtxndof;
  return pair;
 
}

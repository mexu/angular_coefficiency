// $Id: TupleToolMuStub.h,v 1.5 2010/01/26 15:39:26 rlambert Exp $
#ifndef TUPLETOOLTRACKEFF_H 
#define TUPLETOOLTRACKEFF_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"            // Interface
#include "Math/Boost.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "Event/State.h"
#include "Event/CaloCluster.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticle2MCAssociator.h"


/** @class TupleToolMuStub TupleToolMuStub.h
 *  
 *  Fill MC Particle with decay angle in mother frame
 *
 * - head_CosTheta : angle in mother's frame
 * - if WRTMother is false, will calculate angle in frame of top of tree
 * 
 * \sa MCTupleToolTrackEff, DecayTreeTuple, MCDecayTreeTuple
 * 
 *  @author Patrick Koppenburg
 *  @date   2009-01-21
 *  @author R. Lambert
 *  @date   2009-09-04
 */

class TupleToolTrackEff : public TupleToolBase, virtual public IParticleTupleTool {
public: 
  /// Standard constructor
  TupleToolTrackEff( const std::string& type, 
                      const std::string& name,
                      const IInterface* parent);

  virtual ~TupleToolTrackEff( ); ///< Destructor
  virtual StatusCode initialize();
  virtual StatusCode fill( const LHCb::Particle*
			   , const LHCb::Particle*
			   , const std::string&
			   , Tuples::Tuple& );
 

protected:

private:

  void commonLHCbIDs(const LHCb::Track* longTrack, const LHCb::Track* muonTTTrack, double& muFraction, double& TTFraction, bool& hasTTHits);
  int numberAddedTTHits(const LHCb::Track* track);
 
  const LHCb::Particle* getOppPart(const LHCb::Particle* mother, const LHCb::Particle* part);
  Gaudi::LorentzVector getMuon(const LHCb::Track* track);
  //std::string m_extrapolatorName;
  IHitExpectation* m_ttExpectation;
  
  double m_ttAssocFraction;
  double m_muonAssocFraction;
 
  LHCb::IParticlePropertySvc* m_ppSvc;

  
};
#endif // TUPLETOOLANGLES_H

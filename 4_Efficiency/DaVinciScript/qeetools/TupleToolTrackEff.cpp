// $Id: TupleToolAngles.cpp,v 1.5 2010/01/26 15:39:26 rlambert Exp $
// Include files 
#include "gsl/gsl_sys.h"

// from Gaudi
// yinh debug here, Sept. 2019
//#include "GaudiKernel/ToolFactory.h" 
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Vector3DTypes.h"

// local
#include "TupleToolTrackEff.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"

//using namespace LHCb;
//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolTrackEff
//
// 2009-01-19 : Patrick Koppenburg
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
// yinh debug here, Sept. 2019
DECLARE_COMPONENT( TupleToolTrackEff );
//DECLARE_TOOL_FACTORY( TupleToolTrackEff );


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolTrackEff::TupleToolTrackEff( const std::string& type,
                                        const std::string& name,
                                        const IInterface* parent )
  : TupleToolBase ( type, name , parent )
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty("TTAssocFraction", m_ttAssocFraction = 0.7);
  declareProperty("MuonAssocFraction", m_muonAssocFraction = 0.7);
  //declareProperty("InverseMatch", m_inverseMatch = false);
  //declareProperty("MaxAssoc", m_getMaxAssoc = false);
  //declareProperty("EMax", m_maxE = 10000000);
  //declareProperty("Extrapolator", m_extrapolatorName = "TrackMasterExtrapolator");
}
//=============================================================================
// Destructor
//=============================================================================
TupleToolTrackEff::~TupleToolTrackEff() {} 

//=============================================================================
// initialize
//=============================================================================
StatusCode TupleToolTrackEff::initialize(){
  if( ! TupleToolBase::initialize() ) return StatusCode::FAILURE;

  //m_extrapolator = tool<ITrackExtrapolator>( m_extrapolatorName );
  m_ttExpectation = tool<IHitExpectation>("TTHitExpectation");
  m_ppSvc = svc<LHCb::IParticlePropertySvc>("LHCb::ParticlePropertySvc", true);

  return StatusCode::SUCCESS ;
}
//=============================================================================
// Fill
//=============================================================================
StatusCode TupleToolTrackEff::fill( const LHCb::Particle* top
                                     , const LHCb::Particle* part
                                     , const std::string& head
                                     , Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);
  
  

  // -- Navigate back from the (muonTT) particle to the track.
  const LHCb::ProtoParticle* proto = part->proto();
  if(!proto)return StatusCode::SUCCESS ;
  
  const LHCb::Track* muonTTTrack = proto->track();
  if(!muonTTTrack)return StatusCode::SUCCESS ;
 
  
  // -- Initialize some variables

  //if tt htis are present
  bool TTHitAssoc = false; // is any track matched
  bool TTHitAssocM = false; // is any track matched including the mass requirement
  bool TTHitAssocStd = false; // is any track satisfying std requirement (chi2/ndof < 3) matched
  bool TTHitAssocStdM = false; // including mass
  bool TTHitAssocZ = false; // any track satisfying z requirements
  bool TTHitAssocZM = false;
  bool TTHitAssocW = false; //w requirements
  bool TTHitAssocWM = false;
  bool TTHitAssocEwk = false;
  bool TTHitAssocEwkM = false;
  bool noTTHitAssoc = false;
  bool noTTHitAssocM = false;
  bool noTTHitAssocStd = false;
  bool noTTHitAssocStdM = false;
  bool noTTHitAssocZ = false;
  bool noTTHitAssocZM = false;
  bool noTTHitAssocW = false;
  bool noTTHitAssocWM = false;
  bool noTTHitAssocEwk = false;
  bool noTTHitAssocEwkM = false;
  int nExpectedTTHits = 1000;
  int nAddedTTHits = -1;
  int nAddedTTHitsLong = -1;

  int nTTHitAssocZ        = 0;
  int nNoTTHitAssocZ      = 0;
  int nTTHitAssocStd      = 0;
  int nNoTTHitAssocStd    = 0;
  int nTTHitAssocW        = 0;
  int nNoTTHitAssocW      = 0;
  int nTTHitAssocZM       = 0;
  int nNoTTHitAssocZM     = 0;
  int nTTHitAssocStdM      = 0;
  int nNoTTHitAssocStdM    = 0;
  int nTTHitAssocWM       = 0;
  int nNoTTHitAssocWM     = 0;
  int nTTHitAssocEwk      = 0;
  int nNoTTHitAssocEwk    = 0;
  int nTTHitAssocEwkM     = 0;
  int nNoTTHitAssocEwkM   = 0;

  double sigma_pLong = -1;
  double PLong = -1;
  double PChi2Long = -1;
  double chi2NDoFLong = -1;
  double mass = -1;

  double deltaR=0.0;
  double deltaEta=0.0;
  double deltaPhi=0.0;
  double upgradeM = 0.0;

  double maxMuFraction = 0;
  double maxTTFraction = 0;

  double assocPx = -10;
  double assocPy = -10;
  double assocPz = -10;

  int nAssocTracks = 0;
  int nAssocTracksM = 0;
  int nAssocTracksWithTT = 0;
  int nAssocTracksWithTTM = 0;
  int nAssocTracksWithoutTT = 0;
  int nAssocTracksWithoutTTM = 0;
  int nAssocTracksEwkLoose = 0;
  int nAssocTracksEwkLooseM = 0;
 
 
  LHCb::Tracks* tracks = get<LHCb::Tracks>(LHCb::TrackLocation::Default);
  
  for( LHCb::Tracks::const_iterator it = tracks->begin() ; it != tracks->end() ; ++it){

    LHCb::Track* longTrack = *it;
    if(longTrack->type() != 3) continue; // -- track has to be a long track
    

    const LHCb::Particle* oppPart = getOppPart(top, part);
    Gaudi::LorentzVector vectA(0,0,0,0);
    if (oppPart) vectA = oppPart->momentum();
    Gaudi::LorentzVector vectB = getMuon(longTrack);
    mass = (vectA + vectB).M();

    double muFraction = 0;
    double TTFraction = 0;
    bool hasTTHits = false;
    
    commonLHCbIDs(longTrack, muonTTTrack, muFraction, TTFraction, hasTTHits); 

    if ( longTrack->probChi2() > 0.001 && muFraction > m_muonAssocFraction )
      {
        nAssocTracksEwkLoose++;
	if (mass > 40000) nAssocTracksEwkLooseM++;
      }

    if(hasTTHits)
    {
      if(muFraction > m_muonAssocFraction && TTFraction > m_ttAssocFraction )
      {
        nAssocTracks++;
        nAssocTracksWithTT++;
	if ( mass > 40000 ){
	  nAssocTracksM++;
	  nAssocTracksWithTTM++;
	}
        
        if( TTFraction > maxTTFraction)
	  {
	    maxTTFraction = TTFraction;
	  }
        if( muFraction > maxMuFraction)
	  {
	    maxMuFraction = muFraction;
	  }
        // -- Standard tracking requirements
	if( longTrack->chi2PerDoF()  < 3 )
	  {
	    TTHitAssocStd = true;
	    nTTHitAssocStd++;
	    if (mass > 40000) {
	      TTHitAssocStdM = true;
	      nTTHitAssocStdM++;
	    }
	  }

        // -- Special criteria, used for Z/W studies
        if( sigma_pLong/longTrack->p() < 0.1 && longTrack->probChi2()  > 0.001 )
        {
          TTHitAssocZ = true;
	  nTTHitAssocZ++;
	  if (mass > 40000) {
	    TTHitAssocZM = true;
	    nTTHitAssocZM++;
	  }
        }
        if( sigma_pLong/longTrack->p() < 0.1 && longTrack->probChi2()  > 0.01 )
        {
          TTHitAssocW = true;
	  nTTHitAssocW++;
	  if (mass > 40000) {
	    TTHitAssocWM = true;
	    nTTHitAssocWM++;
	  }
        }
        if( longTrack->probChi2()  > 0.001 )
        {
          TTHitAssocEwk = true;
	  nTTHitAssocEwk++;
	  if (mass > 40000) {
	    TTHitAssocEwkM = true;
	    nTTHitAssocEwkM++;
	  }
        }
        TTHitAssoc = true;
        
	//Store information if it is the best muon match
	if (muFraction == maxMuFraction) {
	  nExpectedTTHits = m_ttExpectation->nExpected( *longTrack );
	  nAddedTTHits = numberAddedTTHits( muonTTTrack );
	  nAddedTTHitsLong  = numberAddedTTHits( longTrack );
	  sigma_pLong = sqrt(longTrack->firstState().errP2());
	  PChi2Long =  longTrack->probChi2();
	  chi2NDoFLong = longTrack->chi2()/longTrack->nDoF();
	  PLong = longTrack->p();
	  assocPx = longTrack->momentum().X();
	  assocPy = longTrack->momentum().Y();
	  assocPz = longTrack->momentum().Z(); 
	  upgradeM = mass;
	  
	  deltaPhi = fabs( longTrack->phi() - muonTTTrack->phi() );
	  if(deltaPhi > M_PI) deltaPhi  = 2*M_PI-deltaPhi;
	  deltaEta = longTrack->pseudoRapidity() - muonTTTrack->pseudoRapidity();
	  deltaR = sqrt( deltaPhi*deltaPhi + deltaEta*deltaEta );
	}
      }
      
    }
    else
    {
      if(muFraction >  m_muonAssocFraction)
      {
        nAssocTracks++;
        nAssocTracksWithoutTT++;
	if (mass > 40000){
	  nAssocTracksM++;
	  nAssocTracksWithoutTTM++;
	}
        
        if( muFraction > maxMuFraction)
	      {
          maxMuFraction = muFraction;
	      }
	// -- standard tracking requirements
	if (longTrack->chi2PerDoF() < 3){
	  noTTHitAssocStd = true;
	  nNoTTHitAssocStd++;
	  if ( mass > 40000 ){
	    noTTHitAssocStdM = true;
	    nNoTTHitAssocStdM++;
	  }
	}
        // -- Special criteria, used for Z/W studies
        if( sigma_pLong/longTrack->p() < 0.1 && longTrack->probChi2() > 0.001 )
	  {
	    noTTHitAssocZ = true;
	    nNoTTHitAssocZ++;
	    if (mass > 40000) {
	      noTTHitAssocZM = true;
	      nNoTTHitAssocZM++;
	    }
	  }
	if( sigma_pLong/longTrack->p() < 0.1 && longTrack->probChi2()  > 0.01 )
	  {
	    noTTHitAssocW = true;
	    nNoTTHitAssocW++;
	    if (mass > 40000) {
	      noTTHitAssocWM = true;
	      nNoTTHitAssocWM++;
	    }
	  }
        if ( longTrack->probChi2() > 0.001 )
	  {
	    noTTHitAssocEwk = true;
	    nNoTTHitAssocEwk++;
	    if (mass > 40000) {
	      noTTHitAssocEwkM = true;
	      nNoTTHitAssocEwkM++;
	    }
	  }
        
        noTTHitAssoc = true;
        
	//Store information if best muon match
	if (muFraction == maxMuFraction) {
	  nExpectedTTHits = m_ttExpectation->nExpected( *longTrack );
	  nAddedTTHits = numberAddedTTHits( muonTTTrack );
	  nAddedTTHitsLong  = numberAddedTTHits( longTrack );
	  sigma_pLong = sqrt(longTrack->firstState().errP2());
	  PChi2Long =  longTrack->probChi2();
	  chi2NDoFLong = longTrack->chi2()/longTrack->nDoF();
	  PLong = longTrack->p();
	  assocPx = longTrack->momentum().X();
	  assocPy = longTrack->momentum().Y();
	  assocPz = longTrack->momentum().Z(); 
	  upgradeM = mass;
	}
      }
    }
  }
  
  bool assoc = TTHitAssoc || noTTHitAssoc;
  bool assocStd = TTHitAssocStd || noTTHitAssocStd;
  bool assocZ = TTHitAssocZ || noTTHitAssocZ;
  bool assocW = TTHitAssocW || noTTHitAssocW;
  bool assocEWK = TTHitAssocEwk || noTTHitAssocEwk;
  bool assocEWKLoose = (nAssocTracksEwkLoose > 0);

  bool assocM = TTHitAssocM || noTTHitAssocM;
  bool assocStdM = TTHitAssocStdM || noTTHitAssocStdM;
  bool assocZM = TTHitAssocZM || noTTHitAssocZM;
  bool assocWM = TTHitAssocWM || noTTHitAssocWM;
  bool assocEWKM = TTHitAssocEwkM || noTTHitAssocEwkM;
  bool assocEWKLooseM = (nAssocTracksEwkLooseM > 0);
  
  
  
  // -- Fill all the variables in a tuple
  
  //std::cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" << std::endl;
  //std::cout << "Filling variables in tuple" << std::endl;
  //std::cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" << std::endl;
  
  
  tuple->column(head+"_ExpectedTTHits", nExpectedTTHits);
  tuple->column(head+"_TTHitAssoc", TTHitAssoc);
  tuple->column(head+"_noTTHitAssoc", noTTHitAssoc);
  tuple->column(head+"_Assoc", assoc);
  tuple->column(head+"_TTHits", nAddedTTHits);
  tuple->column(head+"_TTHitsLong", nAddedTTHitsLong);
  
  tuple->column(head+"_sigmaPLong", sigma_pLong);
  tuple->column(head+"_PLong", PLong);
  tuple->column(head+"_PChi2Long",PChi2Long);
  tuple->column(head+"_Chi2NDoFLong", chi2NDoFLong );
  
  tuple->column(head+"_nAssocTracks", nAssocTracks );
  tuple->column(head+"_nAssocTracksEwkLoose", nAssocTracksEwkLoose );
  tuple->column(head+"_nAssocTracksWithTT", nAssocTracksWithTT );
  tuple->column(head+"_nAssocTracksWithoutTT", nAssocTracksWithoutTT );
  tuple->column(head+"_nAssocTracksM", nAssocTracksM );
  tuple->column(head+"_nAssocTracksEwkLooseM", nAssocTracksEwkLooseM );
  tuple->column(head+"_nAssocTracksWithTTM", nAssocTracksWithTTM );
  tuple->column(head+"_nAssocTracksWithoutTTM", nAssocTracksWithoutTTM );
  
  tuple->column(head+"_maxTTFraction", maxTTFraction);
  tuple->column(head+"_maxMuFraction", maxMuFraction);
  // deltaR and deltaS                                                                                                             
  tuple->column(head+"_deltaR", deltaR);
  tuple->column(head+"_deltaphi", deltaPhi);
  tuple->column(head+"_deltaeta", deltaEta);
  
  //deltaPhi
  
  // for Standard tracks
  tuple->column(head+"_nTTHitAssocStd", nTTHitAssocStd);
  tuple->column(head+"_nNoTTHitAssocStd", nNoTTHitAssocStd);
  tuple->column(head+"_AssocStd", assocStd);
  tuple->column(head+"_nTTHitAssocStdM", nTTHitAssocStdM);
  tuple->column(head+"_nNoTTHitAssocStdM", nNoTTHitAssocStdM);
  tuple->column(head+"_AssocStdM", assocStdM);
  // for Zs
  tuple->column(head+"_nTTHitAssocZ", nTTHitAssocZ);
  tuple->column(head+"_nNoTTHitAssocZ", nNoTTHitAssocZ);
  tuple->column(head+"_AssocZ", assocZ);
  tuple->column(head+"_nTTHitAssocZM", nTTHitAssocZM);
  tuple->column(head+"_nNoTTHitAssocZM", nNoTTHitAssocZM);
  tuple->column(head+"_AssocZM", assocZM);
  // for Ws
  tuple->column(head+"_nTTHitAssocW", nTTHitAssocW);
  tuple->column(head+"_nNoTTHitAssocW", nNoTTHitAssocW);
  tuple->column(head+"_AssocW", assocW);
  tuple->column(head+"_nTTHitAssocWM", nTTHitAssocWM);
  tuple->column(head+"_nNoTTHitAssocWM", nNoTTHitAssocWM);
  tuple->column(head+"_AssocWM", assocWM);
  // for general electroweak
  tuple->column(head+"_AssocEWK", assocEWK);
  tuple->column(head+"_AssocEWKM", assocEWKM);
  // for general electroweak - loose (no tt requirement)
  tuple->column(head+"_AssocEWKLooseM", assocEWKLooseM);
 
  // -- Momentum of associated track
  tuple->column(head+"_PxLong", assocPx); 
  tuple->column(head+"_PyLong", assocPy); 
  tuple->column(head+"_PzLong", assocPz); 
  tuple->column(head+"_UpgradeM",upgradeM);

  
  return StatusCode::SUCCESS ;
  
}


  

//=============================================================================
// Count how many TT hit and Muon hit LHCbIDs are the same
//=============================================================================
void TupleToolTrackEff::commonLHCbIDs(const LHCb::Track* longTrack, const LHCb::Track* muonTTTrack, double& muFraction, double& TTFraction, bool& hasTTHits)
{

  LHCb::MuonPIDs* muPIDs   = get<LHCb::MuonPIDs>( LHCb::MuonPIDLocation::Offline ); 

  // -- Get the LHCbIDs of the Probe-track and fill them into a Muon- and a TT container
  std::vector<LHCb::LHCbID> muonTTTrackIDs = muonTTTrack->lhcbIDs();
  std::vector<LHCb::LHCbID> muonTTTrackTTIDs;
  std::vector<LHCb::LHCbID> muonTTTrackMuonIDs;
  
  std::vector<LHCb::LHCbID> longTrackTTIDs;

  LHCb::MuonPID* muonPID = NULL;

  int longMatch = 0;
  int muonMatch = 0;

  for(LHCb::MuonPIDs::iterator iMu = muPIDs->begin() ; iMu != muPIDs->end() ; ++iMu){

    LHCb::MuonPID* muPID = (*iMu);

    const LHCb::Track* muonTrack = muPID->muonTrack(); // The track segment in the muon station
    const LHCb::Track* idTrack = muPID->idTrack(); // The track which was id'ed
   
    // -- Match the track we want to test with the track from PIDs...
    if(idTrack != longTrack || muonTrack == NULL || idTrack == NULL) continue;
    
    longTrackTTIDs = idTrack->lhcbIDs();
    muonPID = muPID;

    muonMatch = (int)muonTrack->nCommonLhcbIDs( *muonTTTrack );
    longMatch = (int)idTrack->nCommonLhcbIDs( *muonTTTrack );
    
  }
  
 
  hasTTHits = false;
  for( std::vector<LHCb::LHCbID>::iterator it = longTrackTTIDs.begin() ; it != longTrackTTIDs.end() ; ++it){
    
    LHCb::LHCbID id = *it;
    if(id.isTT()) hasTTHits = true;
    
  }
  
  for( std::vector<LHCb::LHCbID>::iterator it = muonTTTrackIDs.begin() ; it != muonTTTrackIDs.end() ; ++it){

    LHCb::LHCbID id = (*it);

    if(id.isMuon()) muonTTTrackMuonIDs.push_back(id);
    if(id.isTT()) muonTTTrackTTIDs.push_back(id);

  }
  
  
  muFraction = double(muonMatch)/muonTTTrackMuonIDs.size();
  TTFraction = double(longMatch)/muonTTTrackTTIDs.size();

}
//=============================================================================
// Count how many TT hit and Muon hit LHCbIDs are the same
//=============================================================================
int TupleToolTrackEff::numberAddedTTHits(const LHCb::Track* track){

  int nTTHits = 0;

  for(std::vector<LHCb::LHCbID>::const_iterator it = track->lhcbIDs().begin() ;it != track->lhcbIDs().end(); ++it){
    LHCb::LHCbID id = (*it);
	
    if( id.isTT() )    nTTHits++;
  }
  
  return nTTHits;
}

const LHCb::Particle* TupleToolTrackEff::getOppPart(const LHCb::Particle* mother, const LHCb::Particle* part){
    const LHCb::Particle* oppPart = 0;
   if (mother && mother!= part && mother->daughtersVector().size() == 2){
     if (mother->daughtersVector().at(0)->charge() == part->charge()){
       oppPart = mother->daughtersVector().at(1);
     }
     else {
       oppPart = mother->daughtersVector().at(0);
     }
   }
   return oppPart;
}

Gaudi::LorentzVector TupleToolTrackEff::getMuon(const LHCb::Track* track){
  double mumass = 105.658;
  Gaudi::XYZVector trackMom = track->momentum();
  Gaudi::LorentzVector vect( trackMom.x() , trackMom.y() , trackMom.z() , sqrt(trackMom.Mag2() + pow(mumass,2)));
  return Gaudi::LorentzVector(vect);

}

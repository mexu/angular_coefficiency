// $Id: MyTupleTollPID.cpp,v 1.2 2010-01-26 15:39:26 rlambert Exp $
// Include files
#include "gsl/gsl_sys.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"

// local
#include "MyTupleTollPID.h"

// from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include <string>
#include "TMath.h"
//using namespace LHCb;
//-----------------------------------------------------------------------------
// Implementation file for class : MyTupleTollPID
//
// 2009-11-19 : Rob Lambert
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MyTupleTollPID )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MyTupleTollPID::MyTupleTollPID( const std::string& type,
                                const std::string& name,
                                const IInterface* parent )
  : TupleToolBase ( type, name , parent )
{
  declareInterface<IMCParticleTupleTool>(this);
  declareProperty("StubLocation", m_stublocation = "Phys/FltMuonTTParts/Particles");
}

//=============================================================================
// Destructor
//=============================================================================
MyTupleTollPID::~MyTupleTollPID() {}

StatusCode MyTupleTollPID::initialize()
{
  const StatusCode sc = TupleToolBase::initialize();
  return sc;
}

//=============================================================================
// Fill
//=============================================================================
StatusCode MyTupleTollPID::fill( const LHCb::MCParticle*
                                 , const LHCb::MCParticle* mcp
                                 , const std::string& head
                                 , Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);
  
  if(exist<LHCb::Particle::Range>(m_stublocation))
      {
        m_stubparts = get<LHCb::Particle::Range>(m_stublocation);
      }
    else{
      m_stubparts = LHCb::Particle::Range();
    }

  bool test = true;
  bool Match = false;
  
  if (msgLevel(MSG::DEBUG)) debug() << "MyTupleTollPID::fill " << head << endmsg ;

  int mcPid = 0;

  if (msgLevel(MSG::VERBOSE)) verbose() << "MyTupleTollPID::fill mcp " << mcp << endmsg ;
  // pointer is ready, prepare the values:
  if( mcp )
  {
    for (LHCb::Particle::Range::const_iterator ip = m_stubparts.begin();ip!=m_stubparts.end();++ip){
      const LHCb::Particle* MuTT = (*ip);
      double deltaR = getDeltaR(MuTT, mcp);
      mcPid = mcp->particleID().pid();
      if(deltaR <0.1 && (MuTT->particleID().pid() == mcPid)) Match = true;
    }

  }
  if (msgLevel(MSG::VERBOSE)) verbose() << "MyTupleTollPID::fill filling " << head << endmsg ;

  test &= tuple->column( prefix + "_MatchTT", Match );
  if (msgLevel(MSG::VERBOSE)) verbose() << "MyTupleTollPID::fill bye " << head << endmsg ;

  return StatusCode(test);
}

double MyTupleTollPID::getDeltaR(const LHCb::Particle* partA, const LHCb::MCParticle* partB){
//
  double deltaEta = partA->momentum().eta() - partB->momentum().eta();
  double deltaPhi = 0.0;
  if (fabs(partA->momentum().phi() - partB->momentum().phi()) < TMath::Pi()){
    deltaPhi = fabs(partA->momentum().phi() - partB->momentum().phi());
  }
  else deltaPhi = 2*TMath::Pi() - fabs(partA->momentum().phi() - partB->momentum().phi());

  double deltaR = sqrt( pow(deltaEta,2) + pow(deltaPhi,2));

  return deltaR;
}

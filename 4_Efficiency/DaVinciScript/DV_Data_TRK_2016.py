# coding=utf-8

##############################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
##############################################################################

# Define a sequence
from Configurables import GaudiSequencer
seqplustag = GaudiSequencer("PlusTagSeq")
seqminustag = GaudiSequencer("MinusTagSeq")

#######################################################################
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
#MessageSvc().OutputLevel = 2

triggers = [
    "L0MuonDecision",
    "L0MuonEWDecision",
    "Hlt1SingleMuonHighPTDecision",
    "Hlt2SingleMuonDecision",
    "Hlt2SingleMuonLowPTDecision",
    "Hlt2SingleMuonHighPTDecision",
    "Hlt2SingleMuonVHighPTDecision",
    "Hlt2EWSingleMuonHighPtDecision",
    "Hlt2EWSingleMuonVHighPtDecision"
    ]


def fillTuple(tuple):
  tuple.ToolList = [
          "TupleToolKinematic"
         ,"TupleToolTrackInfo"
         ,"TupleToolRecoStats"
         ,"TupleToolPid"
         ,"TupleToolGeometry"
         ,"TupleToolTrackEff"
         ,"TupleToolConeIsolation"
    ]
#TupleToolRecoStats
  from Configurables import TupleToolRecoStats
  tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
  tuple.TupleToolRecoStats.Verbose=True
#TupleToolConeIsolation
  from Configurables import TupleToolConeIsolation
  tuple.addTool(TupleToolConeIsolation,name="TupleToolConeIsolation")
  tuple.ToolList +=["TupleToolConeIsolation"]
  tuple.TupleToolConeIsolation.MinConeSize = 0.4
  tuple.TupleToolConeIsolation.MaxConeSize = 0.6
  tuple.TupleToolConeIsolation.FillMaxPt = False
#TupleToolTISTOS
  from Configurables import TriggerTisTos,L0TriggerTisTos,TupleToolTISTOS
  tuple.addTool( TupleToolTISTOS, name = "TISTOS" )
  tuple.ToolList += [ "TupleToolTISTOS/TISTOS" ]
  tuple.TISTOS.TriggerList = triggers
  tuple.TISTOS.addTool( TriggerTisTos, name="TriggerTisTos")
  tuple.TISTOS.TriggerTisTos.TOSFracMuon = 0.
  tuple.TISTOS.TriggerTisTos.TOSFracEcal = 0.
  tuple.TISTOS.TriggerTisTos.TOSFracHcal = 0.
  tuple.TISTOS.VerboseL0     = True
  tuple.TISTOS.VerboseHlt1     = True
  tuple.TISTOS.VerboseHlt2     = True
  tuple.TISTOS.addTool(L0TriggerTisTos)
  tuple.TISTOS.addTool(TriggerTisTos)
#Loki
  from Configurables import LoKi__Hybrid__TupleTool
  LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
  LoKi_All.Variables = {
    "PT" : "PT",
    "ETA" : "ETA",
    "PHI" : "PHI",
    "PERR2":"PERR2"
    }
  tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
  tuple.addTool(LoKi_All)
#################################################################
from Configurables import CondDB
CondDB(LatestGlobalTagByDataType = "2016")
from PhysConf.Filters import LoKi_Filters
sf = LoKi_Filters ( STRIP_Code = "HLT_PASS_RE( 'StrippingTrackEffMuonTT_ZLine1Decision' ) | HLT_PASS_RE( 'StrippingTrackEffMuonTT_ZLine2Decision' )")
# ################################################################
# DecayTreeTuple
# ################################################################
from Configurables import DecayTreeTuple, TupleToolTrackEff,TupleToolTISTOS
DtuplePTag = DecayTreeTuple("PlusTag")
DtuplePTag.addTool(TupleToolTrackEff, name="TupleToolTrackEff")
DtuplePTag.TupleToolTrackEff.TTAssocFraction=0.6
DtuplePTag.TupleToolTrackEff.MuonAssocFraction=0.4
DtuplePTag.Inputs = [ "/Event/Calibration/Phys/TrackEffMuonTT_ZLine2/Particles" ]
DtuplePTag.Decay = "Z0 -> ^mu+ ^mu-"
DtuplePTag.Branches = {
  "tag" : "Z0 -> ^mu+ mu-",
  "probe" : "Z0 -> mu+ ^mu-",
  "boson"   : "^(Z0 -> mu+ mu-)"}
seqplustag.Members      +=[DtuplePTag]

DtupleMTag = DecayTreeTuple("MinusTag")
DtupleMTag.addTool(TupleToolTrackEff, name="TupleToolTrackEff")
DtupleMTag.TupleToolTrackEff.TTAssocFraction=0.6
DtupleMTag.TupleToolTrackEff.MuonAssocFraction=0.4
DtupleMTag.addTool( TupleToolTISTOS, name = "TISTOS" )
DtupleMTag.Inputs = [ "/Event/Calibration/Phys/TrackEffMuonTT_ZLine1/Particles" ]
DtupleMTag.Decay = "Z0 -> ^mu+ ^mu-"
DtupleMTag.Branches = {
  "tag" : "Z0 -> mu+ ^mu-",
  "probe" : "Z0 -> ^mu+ mu-",
  "boson"   : "^(Z0 -> mu+ mu-)"}
seqminustag.Members      +=[DtupleMTag]

fillTuple(DtuplePTag)
fillTuple(DtupleMTag)

from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().EvtMax = -1                     # Number of events
DaVinci().DataType = "2016"                    # Default is "DC06"
DaVinci().Simulation   = False                 # It's MC
DaVinci().TupleFile='TRK_Data_2016.root'
DaVinci().Lumi=True
DaVinci().PrintFreq=2000
DaVinci().UserAlgorithms  = [seqplustag]
DaVinci().UserAlgorithms += [seqminustag]
DaVinci().EventPreFilters = sf.filters('Filters')

##############################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer
##############################################################################
# Define a sequence
seq = GaudiSequencer("Seq")
#######################################################################
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

triggers = [
    "L0MuonDecision",
    "L0MuonEWDecision",
    "Hlt1SingleMuonHighPTDecision",
    "Hlt2SingleMuonDecision",
    "Hlt2SingleMuonLowPTDecision",
    "Hlt2SingleMuonHighPTDecision",
    "Hlt2SingleMuonVHighPTDecision",
    "Hlt2EWSingleMuonHighPtDecision",
    "Hlt2EWSingleMuonVHighPtDecision"
    ]

def fillTuple(tuple):

  tuple.ToolList = [
    "TupleToolKinematic"
   ,"TupleToolConeIsolation"
   ,"TupleToolRecoStats"
   ,"TupleToolPid"
   ,"TupleToolAngles"
   ,"TupleToolGeometry"
   ,"TupleToolTrackEff"
   ,"TupleToolMCTruth"
    ]

  MCTruth=TupleToolMCTruth()
  MCTruth.ToolList = [ "MCTupleToolKinematic", "MCTupleToolHierarchy" ]
  tuple.addTool(MCTruth)

  from Configurables import TupleToolRecoStats
  tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
  tuple.TupleToolRecoStats.Verbose=True
#TupleToolConeIsolation
  from Configurables import TupleToolConeIsolation
  tuple.addTool(TupleToolConeIsolation,name="TupleToolConeIsolation")
  tuple.ToolList +=["TupleToolConeIsolation"]
  tuple.TupleToolConeIsolation.MinConeSize = 0.4
  tuple.TupleToolConeIsolation.MaxConeSize = 0.6
  tuple.TupleToolConeIsolation.FillMaxPt = False
#TupleToolTISTOS
  from Configurables import DaVinci, L0TriggerTisTos,TriggerTisTos
  tuple.addTool( TupleToolTISTOS, name = "TISTOS" )
  tuple.ToolList += [ "TupleToolTISTOS/TISTOS" ]
  tuple.TISTOS.TriggerList = triggers
  tuple.TISTOS.addTool( TriggerTisTos, name="TriggerTisTos")
  tuple.TISTOS.TriggerTisTos.TOSFracMuon = 0.
  tuple.TISTOS.TriggerTisTos.TOSFracEcal = 0.
  tuple.TISTOS.TriggerTisTos.TOSFracHcal = 0.
  tuple.TISTOS.VerboseL0     = True
  tuple.TISTOS.VerboseHlt1     = True
  tuple.TISTOS.VerboseHlt2     = True
  tuple.TISTOS.addTool(L0TriggerTisTos)
  tuple.TISTOS.addTool(TriggerTisTos)
#Loki
  from Configurables import LoKi__Hybrid__TupleTool
  LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
  LoKi_All.Variables = {
    "PT" : "PT",
    "ETA" : "ETA",
    "PHI" : "PHI",
    "PERR2":"PERR2"
    }
  tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
  tuple.addTool(LoKi_All)
# ########################################################################################
# -->
# --> PREPARE THE LONG TRACKS <--
# -->
# ########################################################################################

# ########################################################################################
# The long track preparation, including hard coded cuts
# ########################################################################################
from StandardParticles import StdLooseKaons, StdLooseMuons, StdAllLooseMuons 
from Configurables import FilterDesktop
FltMuons                = FilterDesktop( "FltMuons" )
FltMuons.Inputs = ["Phys/StdAllLooseMuons/Particles"]
FltMuons.Preambulo += [ "from LoKiTracks.decorators import *" ]
FltMuons.Code      = "(HASMUON) & (ISMUON) & (PIDmu > 2.0) & (P > 10000) & (PT > 3000*MeV) & (TRCHI2DOF < 2.0)"
seq.Members       += [ FltMuons ]
# ########################################################################################
# HLT 1 lines we run on
# ########################################################################################
from Configurables import TisTosParticleTagger
TaggerHLT1 = TisTosParticleTagger("TaggerHLT1")
TaggerHLT1.Inputs = ["Phys/FltMuons/Particles"]
TaggerHLT1.TisTosSpecs = {"Hlt1SingleMuonHighPTDecision%TOS" : 0}
TaggerHLT1.ProjectTracksToCalo = False
TaggerHLT1.CaloClustForCharged = False
TaggerHLT1.CaloClustForNeutral = False
TaggerHLT1.TOSFrac = { 4:0.0, 5:0.0 }
TaggerHLT1.NoRegex = True
seq.Members       += [ TaggerHLT1 ]
# ########################################################################################
# HLT 2 lines we run on
# ########################################################################################
TaggerHLT2 = TisTosParticleTagger("TaggerHLT2")
TaggerHLT2.Inputs = ["Phys/TaggerHLT1/Particles"]
TaggerHLT2.TisTosSpecs = {'Hlt2EWSingleMuonVHighPtDecision%TOS':0 }
TaggerHLT2.ProjectTracksToCalo = False
TaggerHLT2.CaloClustForCharged = False
TaggerHLT2.CaloClustForNeutral = False
TaggerHLT2.TOSFrac = { 4:0.0, 5:0.0 }
TaggerHLT2.NoRegex = True
seq.Members  += [ TaggerHLT2 ]
# ########################################################################################
# -->
# --> PATTERN RECOGNITION AND MUONTT TRACK MAKING <--
# -->
# ########################################################################################

# ########################################################################################
# The pattern recognition -> muonTT track stuff
# ########################################################################################
from Configurables import MuonTTTrack, MuonNNetRec, PatAddTTCoord, TrackMasterFitter, MuonCombRec, TrackMasterExtrapolator,MuonHitDecode
"""
 Make a muonTT track out of hits in the muon station and TT, and give it some options to configure
"""
XTolParam = 25.0
MaxChi2TolParam = 7.0
MinAxProjParam = 5.5
MajAxProjParam = 25.0
MakeMuonTT = MuonTTTrack("MakeMuonTT")
MakeMuonTT.ToolName = "MuonCombRec"
MakeMuonTT.addTool( MuonCombRec )
MakeMuonTT.MuonCombRec.MeasureTime = True
MakeMuonTT.MuonCombRec.CloneKiller = False
MakeMuonTT.MuonCombRec.SkipStation = -1 # -1=no skip, 0=M1, 1=M2, 2=M3, 3=M4, 4=M5
MakeMuonTT.MuonCombRec.DecodingTool = "MuonHitDecode"
MakeMuonTT.MuonCombRec.PadRecTool = "MuonPadRec"
MakeMuonTT.MuonCombRec.ClusterTool = "MuonFakeClustering" # to enable: "MuonClusterRec"
#MakeMuonTT.MuonCombRec.ClusterTool = "MuonClusterRec" # to enable: "MuonClusterRec"
MakeMuonTT.MuonCombRec.PhysicsTiming = True
MakeMuonTT.MuonCombRec.AssumeCosmics = False
MakeMuonTT.MuonCombRec.AssumePhysics = True
MakeMuonTT.MuonCombRec.StrongCloneKiller = False
MakeMuonTT.MuonCombRec.SeedStation = 2 # default seed station is 2 - M3, better is 4 - M5
MakeMuonTT.MuonCombRec.addTool( MuonHitDecode, ("MuonHitDecode") )
MakeMuonTT.MuonCombRec.MuonHitDecode.SkipHWNumber = True
# #############################################################
MakeMuonTT.addTool( PatAddTTCoord )
MakeMuonTT.PatAddTTCoord.YTolSlope = 400000.0
MakeMuonTT.PatAddTTCoord.XTol = XTolParam
MakeMuonTT.PatAddTTCoord.XTolSlope = 400000.0
MakeMuonTT.PatAddTTCoord.MinAxProj = MinAxProjParam
MakeMuonTT.PatAddTTCoord.MajAxProj = MajAxProjParam
MakeMuonTT.PatAddTTCoord.MaxChi2Tol = MaxChi2TolParam
# ################################################################
MakeMuonTT.addTool( TrackMasterFitter)
MakeMuonTT.TrackMasterFitter.MaterialLocator = "SimplifiedMaterialLocator"
MakeMuonTT.addTool( TrackMasterExtrapolator ) 
MakeMuonTT.TrackMasterExtrapolator.MaterialLocator = "SimplifiedMaterialLocator"
# ################################################################
MakeMuonTT.AddTTHits = True
MakeMuonTT.MC = False
MakeMuonTT.OutputLevel = 4
MakeMuonTT.Output = "Rec/Track/MuonTT"
seq.Members     += [MakeMuonTT]
# ########################################################################################
# Make the protoparticles
# ########################################################################################
from PhysSelPython.Wrappers import Selection, DataOnDemand, ChargedProtoParticleSelection
from Configurables import ChargedProtoParticleMaker
MakeProtoParticles = ChargedProtoParticleMaker()
MakeProtoParticles.Inputs = ["Rec/Track/MuonTT"]
MakeProtoParticles.Output = "Rec/MuonTTPParts/ProtoParticles"
seq.Members      += [MakeProtoParticles]
#MakeProtoParticles = ChargedProtoParticleSelection("MuonTTPParts","Rec/Track/MuonTT")
# ########################################################################################
# Make Particles out of the muonTT ProtoParticles
# ########################################################################################
from Configurables import NoPIDsParticleMaker, TrackSelector
MuonTTParts = NoPIDsParticleMaker("MuonTTParts")
MuonTTParts.Particle = 'muon'
MuonTTParts.addTool( TrackSelector )
MuonTTParts.TrackSelector.TrackTypes = [ "Long" ]
MuonTTParts.Input =  "Rec/MuonTTPParts/ProtoParticles"
MuonTTParts.OutputLevel = 4
seq.Members  += [ MuonTTParts]
# ########################################################################################
plusseq = GaudiSequencer("plusseq")
minusseq = GaudiSequencer("minusseq")
# ########################################################################################
# Charge filter, that filters, well, the charge and takes the particles from the right source (long or muonTT)
# ########################################################################################
FltPlusMuons                = FilterDesktop( "FltPlusMuons" )
FltPlusMuons.Inputs = ["Phys/TaggerHLT2/Particles"]
FltPlusMuons.Code           = "Q > 0"
plusseq.Members       += [ FltPlusMuons ]

FltMinusMuons                = FilterDesktop( "FltMinusMuons" )
FltMinusMuons.Inputs = ["Phys/TaggerHLT2/Particles"]
FltMinusMuons.Code           = "Q < 0"
minusseq.Members       += [ FltMinusMuons ]

FltMinusMuonTTs                = FilterDesktop( "FltMinusMuonTTs" )
FltMinusMuonTTs.Inputs = ["Phys/MuonTTParts/Particles"]
FltMinusMuonTTs.Code           = "Q < 0"
plusseq.Members       += [ FltMinusMuonTTs ]

FltPlusMuonTTs                = FilterDesktop( "FltPlusMuonTTs" )
FltPlusMuonTTs.Inputs = ["Phys/MuonTTParts/Particles"]
FltPlusMuonTTs.Code           = "Q > 0"
minusseq.Members       += [ FltPlusMuonTTs ]

# ########################################################################################
# Resonance maker, that fits two muons to a resonance (J/psi, Upsilon, Z)
# ########################################################################################    
from Configurables import CombineParticles
ptag = CombineParticles("Z2MuMuPlusTag")
ptag.Inputs = [ "Phys/FltPlusMuons/Particles","Phys/FltMinusMuonTTs/Particles" ]
ptag.DecayDescriptors = ["Z0 -> mu+ mu-"]
ptag.DaughtersCuts = { 
"mu+" : "(PT>10000) & (PIDmu > 2)",
"mu-" : "PT>500"}
ptag.CombinationCut = "ADAMASS('Z0') < 80000"
ptag.MotherCut = "(ADMASS('Z0') < 40000) & (VFASPF(VCHI2/VDOF) < 5) & (MIPDV(PRIMARY) < 10000)"
plusseq.Members      += [ptag]

mtag = CombineParticles("Z2MuMuMinusTag")
mtag.Inputs = [ "Phys/FltMinusMuons/Particles","Phys/FltPlusMuonTTs/Particles" ]
mtag.DecayDescriptors = ["Z0 -> mu+ mu-"]
mtag.DaughtersCuts = {
"mu-" : "(PT>10000) & (PIDmu > 2)",
"mu+" : "PT>500"}
mtag.CombinationCut = "ADAMASS('Z0') < 80000"
mtag.MotherCut = "(ADMASS('Z0') < 40000) & (VFASPF(VCHI2/VDOF) < 5) & (MIPDV(PRIMARY) < 10000)"
minusseq.Members      += [mtag]

from Configurables import  DecayTreeTuple,TupleToolMCTruth, TupleToolKinematic, TupleToolStripping, TupleToolTrackIsolation, TupleToolRecoStats, TupleToolTISTOS, TupleToolTrackEff, TupleToolDecay, TupleToolDecayTreeFitter, TupleToolDecay, TriggerTisTos
from DecayTreeTuple.Configuration import *

DtuplePlusTag = DecayTreeTuple("PlusTag")
DtuplePlusTag.Decay = "Z0 -> ^mu+ ^mu-"
DtuplePlusTag.Inputs = ["Phys/Z2MuMuPlusTag"]
DtuplePlusTag.addBranches({
"tag" : "Z0 -> ^mu+ mu-"
,"probe" : "Z0 -> mu+ ^mu-"
,"boson"   : "^(Z0 -> mu+ mu-)"})
DtuplePlusTag.addTool(TupleToolTrackEff, name="TupleToolTrackEff")
DtuplePlusTag.ToolList += ["TupleToolTrackEff"]
DtuplePlusTag.TupleToolTrackEff.TTAssocFraction=0.6
DtuplePlusTag.TupleToolTrackEff.MuonAssocFraction=0.4
fillTuple(DtuplePlusTag)
plusseq.Members += [ DtuplePlusTag ]

DtupleMinusTag = DecayTreeTuple("MinusTag")
DtupleMinusTag.Decay = "Z0 -> ^mu+ ^mu-"
DtupleMinusTag.Inputs = ["Phys/Z2MuMuMinusTag"]
DtupleMinusTag.addBranches({
"tag" : "Z0 -> mu+ ^mu-"
,"probe" : "Z0 -> ^mu+ mu-"
,"boson"   : "^(Z0 -> mu+ mu-)"})
DtupleMinusTag.addTool(TupleToolTrackEff, name="TupleToolTrackEff")
DtupleMinusTag.ToolList += ["TupleToolTrackEff"]
DtupleMinusTag.TupleToolTrackEff.TTAssocFraction=0.6
DtupleMinusTag.TupleToolTrackEff.MuonAssocFraction=0.4
fillTuple(DtupleMinusTag)
minusseq.Members += [ DtupleMinusTag ]

from Configurables import TupleToolTrackIsolation, TupleToolTISTOS, TupleToolMuonTT,TupleToolConeIsolation,L0TriggerTisTos
SeqZMuMu = GaudiSequencer("SeqZMuMu")
DtupleTrkLongLong = DecayTreeTuple("LongLongTuple")
DtupleTrkLongLong.Decay = "Z0 -> ^mu+ ^mu-"
DtupleTrkLongLong.Inputs = ["/Event/AllStreams/Phys/Z02MuMuLine/Particles"]
DtupleTrkLongLong.addBranches({
   "mu-" : "Z0 -> mu+ ^mu-"
  ,"mu+" : "Z0 -> ^mu+ mu-"
  ,"Z0"   : "^(Z0 -> mu+ mu-)"})
DtupleTrkLongLong.ToolList = [
         "TupleToolPrimaries"
         ,"TupleToolKinematic"
#         ,"TupleToolTrackInfo"
         ,"TupleToolRecoStats"
         ,"TupleToolPid"
         ,"TupleToolANNPID"
         ,"TupleToolEventInfo"
         ,"TupleToolConeIsolation"
         ,"TupleToolAngles"
         ,"TupleToolGeometry"
         ,"TupleToolTrackEff"
         ,"TupleToolMCTruth"
         ,"TupleToolMuonTT"
    ]

MCTruth=TupleToolMCTruth()
MCTruth.ToolList = [ "MCTupleToolKinematic", "MCTupleToolHierarchy" ]
DtupleTrkLongLong.addTool(MCTruth)
from Configurables import TupleToolRecoStats
DtupleTrkLongLong.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
DtupleTrkLongLong.TupleToolRecoStats.Verbose=True
from Configurables import TupleToolMuonTT
DtupleTrkLongLong.addTool(TupleToolMuonTT, name="TupleToolMuonTT")
DtupleTrkLongLong.TupleToolMuonTT.StubLocation = "Phys/MuonTTParts/Particles"

DtupleTrkLongLong.addTool(TupleToolConeIsolation,name="TupleToolConeIsolation")
DtupleTrkLongLong.ToolList +=["TupleToolConeIsolation"]
DtupleTrkLongLong.TupleToolConeIsolation.MinConeSize = 0.4
DtupleTrkLongLong.TupleToolConeIsolation.MaxConeSize = 0.6
DtupleTrkLongLong.TupleToolConeIsolation.FillMaxPt = False
DtupleTrkLongLong.addTool( TupleToolTISTOS, name = "TISTOS" )

DtupleTrkLongLong.ToolList += [ "TupleToolTISTOS/TISTOS" ]
DtupleTrkLongLong.TISTOS.TriggerList = triggers
DtupleTrkLongLong.TISTOS.addTool( TriggerTisTos, name="TriggerTisTos")
DtupleTrkLongLong.TISTOS.TriggerTisTos.TOSFracMuon = 0.
DtupleTrkLongLong.TISTOS.TriggerTisTos.TOSFracEcal = 0.
DtupleTrkLongLong.TISTOS.TriggerTisTos.TOSFracHcal = 0.
DtupleTrkLongLong.TISTOS.VerboseL0     = True
DtupleTrkLongLong.TISTOS.VerboseHlt1     = True
DtupleTrkLongLong.TISTOS.VerboseHlt2     = True
DtupleTrkLongLong.TISTOS.addTool(L0TriggerTisTos)
DtupleTrkLongLong.TISTOS.addTool(TriggerTisTos)
#Loki
from Configurables import LoKi__Hybrid__TupleTool
LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
LoKi_All.Variables = {
  "PT" : "PT",
  "ETA" : "ETA",
  "PHI" : "PHI",
  "PERR2":"PERR2"
  }
DtupleTrkLongLong.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
DtupleTrkLongLong.addTool(LoKi_All)
# ################################################################
# mct
# ################################################################
from Configurables import MCDecayTreeTuple
simulation = True
DtupleMCTZ = MCDecayTreeTuple("MCTZ")
DtupleMCTZ.Decay = 'Z0 => ^mu+ ^mu- {e+} {e-} {e+} {e-}'
DtupleMCTZ.addBranches({
  "Z"  : "^(Z0 =>  mu+   mu- {e+} {e-} {e+} {e-})",
  "mup":  "(Z0 => ^mu+   mu- {e+} {e-} {e+} {e-})",
  "mum":  "(Z0 =>  mu+  ^mu- {e+} {e-} {e+} {e-})"
})

mctl=[
   'MCTupleToolAngles',
   'MCTupleToolHierarchy',
   'MCTupleToolKinematic',
   'MCTupleToolPrimaries',
   'MCTupleToolReconstructed',
   'MCTupleToolInteractions',
   'TupleToolEventInfo',
   'MCTupleToolHierarchy',
   "MCTupleToolKinematic"
]

DtupleMCTZ.ToolList=mctl
from Configurables import MyTupleTollPID
DtupleMCTZ.addTool(MyTupleTollPID, name="MyTupleTollPID")
DtupleMCTZ.ToolList += ["MyTupleTollPID"]
DtupleMCTZ.MyTupleTollPID.StubLocation = "Phys/MuonTTParts/Particles"

DtupleMCTZ.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_All")
DtupleMCTZ.LoKi_All.Variables =  {
  'TRUEID' : 'MCID',
  'ETA' : 'MCETA',
  'PHI' : 'MCPHI',
  'PT'  : 'MCPT',
 }

DtupleMCTZ.LoKi_All.Preambulo = [
      "from LoKiCore.math import log"
      ]
DtupleMCTZ.LoKi_All.Variables.update({"M" : "MCM"})
DtupleMCTZ.LoKi_All.Variables.update({"Y" : "0.5*log((MCE+MCPZ)/(MCE-MCPZ))"})
  
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().PrintFreq = 2000
DaVinci().UserAlgorithms = [DtupleMCTZ]
DaVinci().UserAlgorithms += [ seq,plusseq]
DaVinci().UserAlgorithms += [ seq,minusseq]
DaVinci().UserAlgorithms += [ seq,DtupleTrkLongLong]
DaVinci().EvtMax = -1
DaVinci().Simulation   = True                  # It's MC
DaVinci().CondDBtag     = "sim-20161124-vc-mu100"
DaVinci().DDDBtag       = "dddb-20150724"
DaVinci().DataType = "2016" 
DaVinci().TupleFile="TRK_MC_2016_U.root"

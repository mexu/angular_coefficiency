# coding=utf-8

##############################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
##############################################################################
# Define a sequence
from Configurables import GaudiSequencer
seq = GaudiSequencer("Seq")
seqplustag = GaudiSequencer("PlusTagSeq")
seqminustag = GaudiSequencer("MinusTagSeq")
#######################################################################
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
#MessageSvc().OutputLevel = 2

triggers = [
    "L0MuonDecision",
    "L0MuonEWDecision",
    "Hlt1SingleMuonHighPTDecision",
    "Hlt2SingleMuonDecision",
    "Hlt2SingleMuonLowPTDecision",
    "Hlt2SingleMuonHighPTDecision",
    "Hlt2SingleMuonVHighPTDecision",
    "Hlt2EWSingleMuonHighPtDecision",
    "Hlt2EWSingleMuonVHighPtDecision"
    ]


def fillTuple(tuple):
  tuple.ToolList = [
          "TupleToolKinematic"
         ,"TupleToolAngles"
         ,"TupleToolTrackInfo"
         ,"TupleToolRecoStats"
         ,"TupleToolPid"
         ,"TupleToolGeometry"
         ,"TupleToolTrackEff"
         ,"TupleToolConeIsolation"
         ,"TupleToolMCTruth"
    ]
#TupleToolMCTruth
  from Configurables import TupleToolMCTruth
  MCTruth=TupleToolMCTruth()
  MCTruth.ToolList = [ "MCTupleToolKinematic", "MCTupleToolHierarchy" ]
  tuple.addTool(MCTruth)
#TupleToolRecoStats
  from Configurables import TupleToolRecoStats
  tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
  tuple.TupleToolRecoStats.Verbose=True
#TupleToolConeIsolation
  from Configurables import TupleToolConeIsolation
  tuple.addTool(TupleToolConeIsolation,name="TupleToolConeIsolation")
  tuple.ToolList +=["TupleToolConeIsolation"]
  tuple.TupleToolConeIsolation.MinConeSize = 0.4
  tuple.TupleToolConeIsolation.MaxConeSize = 0.6
  tuple.TupleToolConeIsolation.FillMaxPt = False
#TupleToolTISTOS
  tuple.addTool( TupleToolTISTOS, name = "TISTOS" )
  tuple.ToolList += [ "TupleToolTISTOS/TISTOS" ]
  tuple.TISTOS.TriggerList = triggers
  tuple.TISTOS.VerboseL0     = True
  tuple.TISTOS.VerboseHlt1     = True
  tuple.TISTOS.VerboseHlt2     = True
#Loki
  from Configurables import LoKi__Hybrid__TupleTool
  LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
  LoKi_All.Variables = {
    "PT" : "PT",
    "ETA" : "ETA",
    "PHI" : "PHI",
    "PERR2":"PERR2"
    }
  tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
  tuple.addTool(LoKi_All)
#################################################################
from PhysConf.Filters import LoKi_Filters
sf = LoKi_Filters ( STRIP_Code = "HLT_PASS_RE( 'StrippingTrackEffMuonTT_ZLine1Decision' ) | HLT_PASS_RE( 'StrippingTrackEffMuonTT_ZLine2Decision' )")
# ################################################################
# DecayTreeTuple
# ################################################################
from Configurables import DecayTreeTuple, TupleToolTrackEff,TupleToolTISTOS
DtuplePTag = DecayTreeTuple("PlusTag")
DtuplePTag.addTool(TupleToolTrackEff, name="TupleToolTrackEff")
DtuplePTag.TupleToolTrackEff.TTAssocFraction=0.6
DtuplePTag.TupleToolTrackEff.MuonAssocFraction=0.4
DtuplePTag.Inputs = [ "/Event/AllStreams/Phys/TrackEffMuonTT_ZLine2/Particles"]
DtuplePTag.Decay = "Z0 -> ^mu+ ^mu-"
DtuplePTag.Branches = {
  "tag" : "Z0 -> ^mu+ mu-",
  "probe" : "Z0 -> mu+ ^mu-",
  "boson"   : "^(Z0 -> mu+ mu-)"}
seqplustag.Members      +=[DtuplePTag]

DtupleMTag = DecayTreeTuple("MinusTag")
DtupleMTag.addTool(TupleToolTrackEff, name="TupleToolTrackEff")
DtupleMTag.TupleToolTrackEff.TTAssocFraction=0.6
DtupleMTag.TupleToolTrackEff.MuonAssocFraction=0.4
DtupleMTag.addTool( TupleToolTISTOS, name = "TISTOS" )
DtupleMTag.Inputs = [ "/Event/AllStreams/Phys/TrackEffMuonTT_ZLine1/Particles" ]
DtupleMTag.Decay = "Z0 -> ^mu+ ^mu-"
DtupleMTag.Branches = {
  "tag" : "Z0 -> mu+ ^mu-",
  "probe" : "Z0 -> ^mu+ mu-",
  "boson"   : "^(Z0 -> mu+ mu-)"}
seqminustag.Members      +=[DtupleMTag]

fillTuple(DtuplePTag)
fillTuple(DtupleMTag)

# ########################################################################################
# Make a muonTT track out of hits in the muon station and TT, and give it some options to configure
# #######################################################################################
from Configurables import MuonTTTrack, MuonNNetRec, PatAddTTCoord, TrackMasterFitter, MuonCombRec, TrackMasterExtrapolator,MuonHitDecode
XTolParam = 25.0
MaxChi2TolParam = 7.0
MinAxProjParam = 5.5
MajAxProjParam = 25.0
MakeMuonTT = MuonTTTrack("MakeMuonTT")
MakeMuonTT.ToolName = "MuonCombRec"
MakeMuonTT.addTool( MuonCombRec )
MakeMuonTT.MuonCombRec.MeasureTime = True
MakeMuonTT.MuonCombRec.CloneKiller = False
MakeMuonTT.MuonCombRec.SkipStation = -1 # -1=no skip, 0=M1, 1=M2, 2=M3, 3=M4, 4=M5
MakeMuonTT.MuonCombRec.DecodingTool = "MuonHitDecode"
MakeMuonTT.MuonCombRec.PadRecTool = "MuonPadRec"
MakeMuonTT.MuonCombRec.ClusterTool = "MuonFakeClustering" # to enable: "MuonClusterRec"
#MakeMuonTT.MuonCombRec.ClusterTool = "MuonClusterRec" # to enable: "MuonClusterRec"
MakeMuonTT.MuonCombRec.PhysicsTiming = True
MakeMuonTT.MuonCombRec.AssumeCosmics = False
MakeMuonTT.MuonCombRec.AssumePhysics = True
MakeMuonTT.MuonCombRec.StrongCloneKiller = False
MakeMuonTT.MuonCombRec.SeedStation = 2 # default seed station is 2 - M3, better is 4 - M5
MakeMuonTT.MuonCombRec.addTool( MuonHitDecode, ("MuonHitDecode") )
MakeMuonTT.MuonCombRec.MuonHitDecode.SkipHWNumber = True
# #############################################################
MakeMuonTT.addTool( PatAddTTCoord )
MakeMuonTT.PatAddTTCoord.YTolSlope = 400000.0
MakeMuonTT.PatAddTTCoord.XTol = XTolParam
MakeMuonTT.PatAddTTCoord.XTolSlope = 400000.0
MakeMuonTT.PatAddTTCoord.MinAxProj = MinAxProjParam
MakeMuonTT.PatAddTTCoord.MajAxProj = MajAxProjParam
MakeMuonTT.PatAddTTCoord.MaxChi2Tol = MaxChi2TolParam
# ################################################################
MakeMuonTT.addTool( TrackMasterFitter)
MakeMuonTT.TrackMasterFitter.MaterialLocator = "SimplifiedMaterialLocator"
MakeMuonTT.addTool( TrackMasterExtrapolator )
MakeMuonTT.TrackMasterExtrapolator.MaterialLocator = "SimplifiedMaterialLocator"
# ########################################################################################
MakeMuonTT.AddTTHits = True
MakeMuonTT.MC = False
MakeMuonTT.OutputLevel = 4
MakeMuonTT.Output = "Rec/Track/MuonTT"
seq.Members     += [MakeMuonTT]
# ########################################################################################
# Make the protoparticles
# ########################################################################################
from PhysSelPython.Wrappers import Selection, DataOnDemand, ChargedProtoParticleSelection
from Configurables import ChargedProtoParticleMaker
MakeProtoParticles = ChargedProtoParticleMaker()
MakeProtoParticles.Inputs = ["Rec/Track/MuonTT"]
MakeProtoParticles.Output = "Rec/MuonTTPParts/ProtoParticles"
seq.Members      += [MakeProtoParticles]
# ########################################################################################
# Make Particles out of the muonTT ProtoParticles
# ########################################################################################
from Configurables import NoPIDsParticleMaker, TrackSelector
MuonTTParts = NoPIDsParticleMaker("MuonTTParts")
MuonTTParts.Particle = 'muon'
MuonTTParts.addTool( TrackSelector )
MuonTTParts.TrackSelector.TrackTypes = [ "Long" ]
MuonTTParts.Input =  "Rec/MuonTTPParts/ProtoParticles"
MuonTTParts.OutputLevel = 4
seq.Members  += [ MuonTTParts]
# ################################################################
# mct
# ################################################################

from Configurables import MCDecayTreeTuple
simulation = True
DtupleMCTZ = MCDecayTreeTuple("MCTZ")
DtupleMCTZ.Decay = 'Z0 => ^mu+ ^mu- {e+} {e-} {e+} {e-}'
DtupleMCTZ.Branches = {
  "Z"  : "^(Z0 =>  mu+   mu- {e+} {e-} {e+} {e-})",
  "mup":  "(Z0 => ^mu+   mu- {e+} {e-} {e+} {e-})",
  "mum":  "(Z0 =>  mu+  ^mu- {e+} {e-} {e+} {e-})"}

mctl=[
   'MCTupleToolAngles',
   'MCTupleToolHierarchy',
   'MCTupleToolKinematic',
   'MCTupleToolPrimaries',
   'MCTupleToolReconstructed',
   'MCTupleToolInteractions',
   'TupleToolEventInfo',
   'MCTupleToolHierarchy',
   "MCTupleToolKinematic"
]

DtupleMCTZ.ToolList=mctl
from Configurables import MyTupleTollPID
DtupleMCTZ.addTool(MyTupleTollPID, name="MyTupleTollPID")
DtupleMCTZ.MyTupleTollPID.StubLocation = "Phys/MuonTTParts/Particles"
DtupleMCTZ.ToolList += ["MyTupleTollPID"]

from Configurables import LoKi__Hybrid__MCTupleTool
LoKi_All=LoKi__Hybrid__MCTupleTool("LoKi_All")
DtupleMCTZ.addTool(LoKi_All)
DtupleMCTZ.LoKi_All.Variables =  {
  'TRUEID' : 'MCID',
  'ETA' : 'MCETA',
  'PHI' : 'MCPHI',
  'PT'  : 'MCPT'
}

DtupleMCTZ.LoKi_All.Preambulo = [
      "from LoKiCore.math import log"
      ]
DtupleMCTZ.LoKi_All.Variables.update({"M" : "MCM"})
DtupleMCTZ.LoKi_All.Variables.update({"Y" : "0.5*log((MCE+MCPZ)/(MCE-MCPZ))"})
DtupleMCTZ.ToolList+=["LoKi::Hybrid::MCTupleTool/LoKi_All"]

# ################################################################
# LongLong
# ################################################################
from Configurables import TupleToolTrackIsolation, TupleToolTISTOS, TupleToolMuonTT,TupleToolConeIsolation,L0TriggerTisTos
SeqZMuMu = GaudiSequencer("SeqZMuMu")
DtupleTrkLongLong = DecayTreeTuple("LongLongTuple")
DtupleTrkLongLong.Decay = "Z0 -> ^mu+ ^mu-"
DtupleTrkLongLong.Inputs = ["/Event/AllStreams/Phys/Z02MuMuLine/Particles"]
DtupleTrkLongLong.Branches = {
   "mu-" : "Z0 -> mu+ ^mu-"
  ,"mu+" : "Z0 -> ^mu+ mu-"
  ,"Z0"   : "^(Z0 -> mu+ mu-)"}
DtupleTrkLongLong.ToolList = [
         "TupleToolPrimaries"
         ,"TupleToolKinematic"
#         ,"TupleToolTrackInfo"
         ,"TupleToolRecoStats"
         ,"TupleToolPid"
         ,"TupleToolANNPID"
         ,"TupleToolEventInfo"
         ,"TupleToolConeIsolation"
         ,"TupleToolAngles"
         ,"TupleToolGeometry"
         ,"TupleToolTrackEff"
         ,"TupleToolMCTruth"
         ,"TupleToolMuonTT"
    ]
from Configurables import TupleToolMCTruth
MCTruth=TupleToolMCTruth()
MCTruth.ToolList = [ "MCTupleToolKinematic", "MCTupleToolHierarchy" ]
DtupleTrkLongLong.addTool(MCTruth)
from Configurables import TupleToolRecoStats
DtupleTrkLongLong.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
DtupleTrkLongLong.TupleToolRecoStats.Verbose=True
from Configurables import TupleToolMuonTT
DtupleTrkLongLong.addTool(TupleToolMuonTT, name="TupleToolMuonTT")
DtupleTrkLongLong.TupleToolMuonTT.StubLocation = "Phys/MuonTTParts/Particles"

DtupleTrkLongLong.addTool(TupleToolConeIsolation,name="TupleToolConeIsolation")
DtupleTrkLongLong.ToolList +=["TupleToolConeIsolation"]
DtupleTrkLongLong.TupleToolConeIsolation.MinConeSize = 0.4
DtupleTrkLongLong.TupleToolConeIsolation.MaxConeSize = 0.6
DtupleTrkLongLong.TupleToolConeIsolation.FillMaxPt = False
DtupleTrkLongLong.addTool( TupleToolTISTOS, name = "TISTOS" )

DtupleTrkLongLong.ToolList += [ "TupleToolTISTOS/TISTOS" ]
DtupleTrkLongLong.TISTOS.TriggerList = triggers
DtupleTrkLongLong.TISTOS.VerboseL0     = True
DtupleTrkLongLong.TISTOS.VerboseHlt1     = True
DtupleTrkLongLong.TISTOS.VerboseHlt2     = True
#Loki
from Configurables import LoKi__Hybrid__TupleTool
LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
LoKi_All.Variables = {
  "PT" : "PT",
  "ETA" : "ETA",
  "PHI" : "PHI",
  "PERR2":"PERR2"
  }
DtupleTrkLongLong.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
DtupleTrkLongLong.addTool(LoKi_All)
########################################
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().EvtMax = -1                     # Number of events
DaVinci().DataType = "2017"                    # Default is "DC06"
DaVinci().Simulation   = True                # It's MC
DaVinci().TupleFile='TRK_MC_2017_D.root'
DaVinci().Lumi= False
DaVinci().PrintFreq=2000
DaVinci().CondDBtag     = "sim-20190430-vc-md100"
DaVinci().DDDBtag       = "dddb-20170721-3"
DaVinci().UserAlgorithms = [seq,DtupleMCTZ]
DaVinci().UserAlgorithms += [seqplustag]
DaVinci().UserAlgorithms += [seqminustag]
DaVinci().UserAlgorithms += [ seq,DtupleTrkLongLong]
DaVinci().EventPreFilters = sf.filters('Filters')

from DecayTreeTuple.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, MergedSelection
from Configurables import DaVinci, FilterDesktop, TupleToolTISTOS, DeterministicPrescaler, L0TriggerTisTos,TriggerTisTos, CombineParticles, LoKi__ODINFilter as ODINFilt
from StandardParticles import StdAllNoPIDsMuons
import GaudiKernel.SystemOfUnits as Units
from Configurables import TupleToolMCTruth
from PhysSelPython.Wrappers import Selection, DataOnDemand
from Configurables import NoPIDsParticleMaker, TrackSelector

triggers = [
    "L0MuonDecision",
    "L0MuonEWDecision",
    "Hlt1SingleMuonHighPTDecision",
    "Hlt2SingleMuonDecision",
    "Hlt2SingleMuonLowPTDecision",
    "Hlt2SingleMuonHighPTDecision",
    "Hlt2SingleMuonVHighPtDecision",
    "Hlt2EWSingleMuonHighPtDecision",
    "Hlt2EWSingleMuonVHighPtDecision"
    ]

def fillTuple(tuple):

  tuple.ToolList = [
        "TupleToolKinematic"
       ,"TupleToolTrackInfo"
       ,"TupleToolRecoStats"
       ,"TupleToolPid"
       ,"TupleToolTrackIsolation"
       ,"TupleToolAngles"
       ,"TupleToolGeometry"
    ]

  from Configurables import TupleToolRecoStats
  tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
  tuple.TupleToolRecoStats.Verbose=True

  from Configurables import TupleToolConeIsolation
#TupleToolConeIsolation
  tuple.addTool(TupleToolConeIsolation,name="TupleToolConeIsolation")
  tuple.ToolList +=["TupleToolConeIsolation"]
  tuple.TupleToolConeIsolation.MinConeSize = 0.4
  tuple.TupleToolConeIsolation.MaxConeSize = 0.6
  tuple.TupleToolConeIsolation.FillMaxPt = False
#TupleToolTISTOS
  from Configurables import TupleToolConeIsolation
  tuple.addTool( TupleToolTISTOS, name = "TISTOS" )
  tuple.ToolList += [ "TupleToolTISTOS/TISTOS" ]
  tuple.TISTOS.TriggerList = triggers
  tuple.TISTOS.addTool( TriggerTisTos, name="TriggerTisTos")
  tuple.TISTOS.TriggerTisTos.TOSFracMuon = 0.
  tuple.TISTOS.TriggerTisTos.TOSFracEcal = 0.
  tuple.TISTOS.TriggerTisTos.TOSFracHcal = 0.
  tuple.TISTOS.Verbose     = True
  tuple.TISTOS.VerboseL0     = True
  tuple.TISTOS.VerboseHlt1     = True
  tuple.TISTOS.VerboseHlt2     = True
  tuple.TISTOS.addTool(L0TriggerTisTos)
  tuple.TISTOS.addTool(TriggerTisTos)
#Loki
  from Configurables import LoKi__Hybrid__TupleTool
  LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
  LoKi_All.Variables = {
    "PT" : "PT",
    "ETA" : "ETA",
    "PHI" : "PHI",
    "PERR2":"PERR2"
    }
  tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
  tuple.addTool(LoKi_All)
#################################################################
#FilterDesktop
#################################################################
from Configurables import GaudiSequencer
SeqPlusTag = GaudiSequencer("SeqPlusTag")
SeqMinusTag = GaudiSequencer("SeqMinusTag")

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
from PhysConf.Filters import LoKi_Filters
sf = LoKi_Filters ( STRIP_Code = "HLT_PASS_RE( 'StrippingWMuLineDecision' )")

from Configurables import FilterDesktop
FltMuons                = FilterDesktop( "FltMuons" )
FltMuons.Inputs = ["/Event/EW/Phys/WMuLine/Particles"]
FltMuons.Preambulo     += [ "from LoKiTracks.decorators import *" ]
FltMuons.Code           = "HASTRACK & (TRPCHI2>0.001) & (ETA>=1.8) & (ETA<=5.0) & (PT>10*GeV )"
SeqPlusTag.Members     += [ FltMuons ]
SeqMinusTag.Members    += [ FltMuons ]

from Configurables import FilterDesktop
FltNoPIDsMuons          = FilterDesktop( "FltNoPIDsMuons" )
FltNoPIDsMuons.Inputs = [ "Phys/StdAllNoPIDsPions/Particles" ]
FltNoPIDsMuons.Preambulo += [ "from LoKiTracks.decorators import *" ]
FltNoPIDsMuons.Code       = "HASTRACK & (TRPCHI2>0.001) & (PT>10*GeV) & (ETA>=1.8) & (ETA<=5.0)"
FltNoPIDsMuons.Code       = "(PT>10*GeV) & (ETA>=1.8) & (ETA<=5.0)"
SeqPlusTag.Members       += [ FltNoPIDsMuons ]
SeqMinusTag.Members      += [ FltNoPIDsMuons ]

FltPlusMuons                = FilterDesktop( "FltPlusMuons" )
FltPlusMuons.Inputs = ["Phys/FltMuons/Particles"]
FltPlusMuons.Code           = "Q > 0"
SeqPlusTag.Members += [ FltPlusMuons ]

FltMinusMuons                = FilterDesktop( "FltMinusMuons" )
FltMinusMuons.Inputs = ["Phys/FltMuons/Particles"]
FltMinusMuons.Code           = "Q < 0"
SeqMinusTag.Members       += [ FltMinusMuons ]

FltNoPIDMinusMuons                = FilterDesktop( "FltNoPIDMinusMuons" )
FltNoPIDMinusMuons.Inputs = ["Phys/FltNoPIDsMuons/Particles"]
FltNoPIDMinusMuons.Code           = "Q < 0"
SeqPlusTag.Members       += [ FltNoPIDMinusMuons ]

FltNoPIDPlusMuons                = FilterDesktop( "FltNoPIDPlusMuons" )
FltNoPIDPlusMuons.Inputs = ["Phys/FltNoPIDsMuons/Particles"]
FltNoPIDPlusMuons.Code           = "Q > 0"
SeqMinusTag.Members        += [ FltNoPIDPlusMuons ]

#################################################################
# CombineParticles
#################################################################
Idptag = CombineParticles("IdPlusTag")
Idptag.Inputs = [ "Phys/FltPlusMuons/Particles","Phys/FltNoPIDMinusMuons/Particles" ]
Idptag.DecayDescriptors = ["Z0 -> mu+ pi-"]
Idptag.DaughtersCuts = {"mu+" : "(PT > 10*GeV) & (MIPCHI2DV(PRIMARY) < 9)"}
Idptag.CombinationCut = "(AM > 40*GeV)"
Idptag.MotherCut = "M > 40*GeV"
SeqPlusTag.Members      += [Idptag]

Idmtag = CombineParticles("IdMinusTag")
Idmtag.Inputs = [ "Phys/FltMinusMuons/Particles","Phys/FltNoPIDPlusMuons/Particles" ]
Idmtag.DecayDescriptors = ["Z0 -> pi+ mu-"]
Idmtag.DaughtersCuts = {"mu-" : "(PT > 10*GeV) & (MIPCHI2DV(PRIMARY) < 9)"}
Idmtag.CombinationCut = "(AM > 40*GeV)"
Idmtag.MotherCut = "M > 40*GeV"
SeqMinusTag.Members      += [Idmtag]

#################################################################
#DecayTreeTuple
# ################################################################
from Configurables import  DecayTreeTuple
from DecayTreeTuple.Configuration import *
DtupleIdPlusTag = DecayTreeTuple("IDPlusTag")
DtupleIdPlusTag.Decay = "Z0 -> ^mu+ ^pi-"
DtupleIdPlusTag.Inputs = ["Phys/IdPlusTag"]
DtupleIdPlusTag.addBranches({
   "tag" : "Z0 -> ^mu+ pi-"
  ,"probe" : "Z0 -> mu+ ^pi-"
  ,"boson" : "^(Z0 -> mu+ pi-)"})
#
DtupleIdMinusTag = DecayTreeTuple("IDMinusTag")
DtupleIdMinusTag.Decay = "Z0 -> ^pi+ ^mu-"
DtupleIdMinusTag.Inputs = ["Phys/IdMinusTag"]
DtupleIdMinusTag.addBranches ({
   "tag" : "Z0 -> pi+ ^mu-"
  ,"probe" : "Z0 -> ^pi+ mu-"
  ,"boson"   : "^(Z0 -> pi+ mu-)"})
#

fillTuple(DtupleIdPlusTag)
fillTuple(DtupleIdMinusTag)

SeqPlusTag.Members += [ DtupleIdPlusTag ]
SeqMinusTag.Members += [ DtupleIdMinusTag ]

from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().EvtMax = -1                   # Number of events
DaVinci().DataType = "2018"                    # Default is "DC06"
DaVinci().Simulation   = False                 # It's  Data
DaVinci().TupleFile='ID_Data_2018.root'
DaVinci().Lumi = True
DaVinci().PrintFreq=2000
DaVinci().UserAlgorithms += [SeqPlusTag]
DaVinci().UserAlgorithms += [SeqMinusTag]
DaVinci().EventPreFilters = sf.filters('Filters')
